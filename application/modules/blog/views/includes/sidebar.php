<?php
$recent_query = $this->blog_model->get_recent_posts();
$recent_posts ='';
if($recent_query->num_rows() > 0)
{
	$row = $recent_query->row();
	
	$post_id = $row->post_id;
	$post_title = $row->post_title;
	$web_name = $this->site_model->create_web_name($post_title);
	$image = base_url().'assets/images/posts/thumbnail_'.$row->post_image;
	$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
	$description = $row->post_content;
	$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
	
	$recent_posts = '
					<div class="gc_footer_ln_main_wrapper">
	                    <div class="gc_footer_ln_img_wrapper">
	                        <img src="'.$image.'" class="img-responsive" alt="ln_img" />
	                    </div>
	                    <div class="gc_footer_ln_cont_wrapper">
	                        <h4><a href="'.site_url().'blog/view-single/'.$web_name.'">'.$post_title.'</a></h4>
	                        <p>28/oct/2018 </p>
	                    </div>
	                </div>';

}

else
{
	$recent_posts = 'No posts yet';
}

$categories_query = $this->blog_model->get_all_active_category_parents();
if($categories_query->num_rows() > 0)
{
	$categories = '';
	foreach($categories_query->result() as $res)
	{
		$category_id = $res->blog_category_id;
		$category_name = $res->blog_category_name;
		$web_name = $this->site_model->create_web_name($category_name);
		
		$children_query = $this->blog_model->get_all_active_category_children($category_id);
		
		//if there are children
		$categories .= '<li><i class="fa fa-caret-right" aria-hidden="true"></i><a href="'.site_url().'blog/category/'.$web_name.'">'.$category_name.'</a></li>';
	}
}

else
{
	$categories = 'No Categories';
}
$popular_query = $this->blog_model->get_popular_posts();

$popular_posts = '';
if($popular_query->num_rows() > 0)
{
	
	
	foreach ($popular_query->result() as $row)
	{
		$post_id = $row->post_id;
		$post_title = $row->post_title;
		$web_name = $this->site_model->create_web_name($post_title);
		$image = base_url().'assets/images/posts/thumbnail_'.$row->post_image;
		$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
		$description = $row->post_content;
		$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 10));
		$created = date('jS M Y',strtotime($row->created));
		
		$popular_posts .= '
			<li>
				<div class="pm-recent-blog-post-thumb" style="background-image:url('.$image.');"></div>
				<div class="pm-recent-blog-post-details">
					<a href="'.site_url().'blog/view-single/'.$web_name.'">'.$mini_desc.'</a>
					<p class="pm-date">'.$created.'</p>
				</div>
			</li>
		';
	}
}

else
{
	$popular_posts = 'No posts views yet';
}
?>
<div class="row">
    <!-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="contect_form_blog">
            <input type="text" placeholder="search here"><a href="blog_category.html"><i class="fa fa-search" aria-hidden="true"></i></a>
        </div>
    </div> -->
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="right_blog_category_wrapper">
            <h4 class="med_bottompadder10">Categories</h4>
            <img src="<?php echo base_url().'assets/themes/deepmind/'?>images/line.png" alt="img" class="img-responsive">
            <div class="right_blog_category_list_wrapper">
                <ul>
                 	<?php echo $categories;?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="right_blog_category_wrapper right_blog_1">
            <h4 class="med_bottompadder10">recent post</h4>
            <img src="<?php echo base_url().'assets/themes/deepmind/'?>images/line.png" alt="img" class="img-responsive">
            <div class="right_post_category_list_wrapper">
                <?php echo $recent_posts?>
            </div>
        </div>
    </div>
</div>