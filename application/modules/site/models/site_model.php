<?php

class Site_model extends CI_Model 
{
	public function get_slides()
	{
  		$table = "slideshow";
		$where = "slideshow_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_active_adverts($ad_position_id = 1)
	{
  		$table = "advert";
		$where = "advert_status = 1 AND ad_position_id = ".$ad_position_id;
		
		$this->db->where($where);
		$this->db->order_by('created', 'DESC');
		$query = $this->db->get($table);
		
		return $query;
	}

	public function get_partners()
	{
  		$table = "partners";
		$where = "partners_status = 1";
		
		$this->db->where($where);
		$this->db->order_by('partner_order', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_corporates()
	{
  		$table = "corporates";
		$where = "corporates_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	
	public function get_all_services()
	{
		$category_name = 'services';
		$where = 'post.blog_category_id = blog_category.blog_category_id AND blog_category.blog_category_name = \''.$category_name.'\''; 
		$table = 'post, blog_category';
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;

	}

	public function get_parent_categories($post_id)
	{
		$where = 'post.post_id = '.$post_id.' AND post.blog_category_id = blog_category.blog_category_id AND blog_category.blog_category_parent <> 0'; 
		$table = 'post, blog_category';
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_blog_category_parents($blog_category_id)
	{
		$where = 'blog_category.blog_category_id = '.$blog_category_id.' AND blog_category.blog_category_parent <> 0'; 
		$table = 'blog_category';
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_resource($limit = NULL)
	{
		$table = "resource";
		$where = "resource_status = 1";
		if($limit !=NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->where($where);
		$this->db->order_by('resource_id', 'DESC');
		$query = $this->db->get($table);
		
		return $query;
	}
	public function display_page_title()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		$last = $total - 1;
		$name = $this->site_model->decode_web_name($page[$last]);
		
		if(is_numeric($name))
		{
			$last = $last - 1;
			$name = $this->site_model->decode_web_name($page[$last]);
		}
		$page_url = ucwords(strtolower($name));
		
		return $page_url;
	}
	
	function generate_price_range()
	{
		$max_price = $this->products_model->get_max_product_price();
		//$min_price = $this->products_model->get_min_product_price();
		
		$interval = $max_price/5;
		
		$range = '';
		$start = 0;
		$end = 0;
		
		for($r = 0; $r < 5; $r++)
		{
			$end = $start + $interval;
			$value = 'KES '.number_format(($start+1), 0, '.', ',').' - KES '.number_format($end, 0, '.', ',');
			$range .= '
			<label class="radio-fancy">
				<input type="radio" name="agree" value="'.$start.'-'.$end.'">
				<span class="light-blue round-corners"><i class="dark-blue round-corners"></i></span>
				<b>'.$value.'</b>
			</label>';
			
			$start = $end;
		}
		
		return $range;
	}
	public function get_footer_navigation()
	{
		$page = explode("/",uri_string());
		$total = count($page);

            
		$navigation = 
		'
			<li><a href="'.base_url().'home">Home</a></li>
            <li><a href="'.base_url().'about">About Us</a></li>
            <li><a href="'.base_url().'faqs">FAQs</a></li>
            <li><a href="'.base_url().'utu-app">Utu App</a></li>
            <li><a href="'.base_url().'blog">Blog</a></li>
            <li><a href="'.base_url().'contact">Contact Us</a></li>
		';

		return $navigation;
	}
	
	public function get_account_navigation()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		
		$name = strtolower($page[1]);
		
		$resources = '';
		$events = '';
		$notifications = '';
		$offers = '';
		$invoices = '';
		$profile = '';
		
		if($name == 'resources')
		{
			$resources = 'current';
		}
		
		if($name == 'events')
		{
			$events = 'current';
		}
		if($name == 'notifications')
		{
			$notifications = 'current';
		}
		if($name == 'offers')
		{
			$offers = 'current';
		}
		if($name == 'invoices')
		{
			$invoices = 'current';
		}
		if($name == 'profile')
		{
			$profile = 'current';
		}
		
		$navigation = 
		'
			<li class="'.$resources.'">
				<a href="'.site_url().'member/resources">
					<i class="fa fa-book"></i>
					Resources
				</a>
			</li>
			<li class="'.$events.'">
				<a href="'.site_url().'member/events">
					<i class="fa fa-newspaper-o"></i>
					Events
				</a>
			</li>
			<li class="'.$notifications.'">
				<a href="'.site_url().'member/notifications">
					<i class="fa fa-bell"></i>
					Notifications
				</a>
			</li>
			<li class="'.$offers.'">
				<a href="'.site_url().'member/offers">
					<i class="fa fa-plus-square"></i>
					Offers
				</a>
			</li>
			<li class="'.$invoices.'">
				<a href="'.site_url().'member/invoices">
					<i class="fa fa-money"></i>
					Invoices
				</a>
			</li>
			<li class="'.$profile.'">
				<a href="'.site_url().'member/profile">
					<i class="fa fa-user"></i>
					Profile
				</a>
			</li>
			
			';
		
		return $navigation;
	}

	public function get_navigation()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		
		$name = strtolower($page[0]);
		
		$home = '';
		$about = '';
		$services = '';
		$membership = '';
		$initiatives = '';
		$opportunities = '';
		$events = '';
		$contact = '';
		$blog = '';
		$blog = $registration = '';
		$resources='';
		
		if($name == 'home')
		{
			$home = 'active';
		}
		
		if($name == 'about-us')
		{
			$about = 'active';
		}
		
		if($name == 'services')
		{
			$services = 'active';
		}
		if($name == 'blog')
		{
			$blog = 'active';
		}
		
		if($name == 'contact-us')
		{
			$contact = 'active';
		}
		


		$rs_categories = $this->site_model->get_active_child_items(6);
		$services_sub_menu_services = '';
		if($rs_categories->num_rows() > 0)
		{
			foreach ($rs_categories->result() as $key => $value) {
				# code...

				$blog_category_name = $value->blog_category_name;
				$blog_category_id = $value->blog_category_id;


				$blog_web_name = $this->create_web_name($blog_category_name);

				$category_items = $this->get_active_post_content_by_category($blog_category_id);
				$services_sub_menu_services .= ' <li class="parent"><a href="'.site_url().'view-service-category/'.$blog_web_name.'">'.$blog_category_name.'</a>';
				// var_dump($category_items);die();
				if($category_items->num_rows() > 0)
				{
					
	                $services_sub_menu_services .= '<ul>';
					foreach ($category_items->result() as $key => $value) {
						# code...

						$post_title = $value->post_title;
						$web_name = $this->create_web_name($post_title);

						$services_sub_menu_services .= ' <li class="parent"><a href="'.site_url().'view-service/'.$web_name.'">'.$post_title.'</a>
	                        </li>';
					}
					$services_sub_menu_services .= '</ul>
					';
					// var_dump($services_sub_menu_services);die();
				}
				else
				{
					// put that category here
				}

				$services_sub_menu_services .= '</li>';

			}
		}
		// var_dump($contact);die();
		// $services_query = $this->get_active_items('Company Services');
		// $services_sub_menu_services = '';
		// if($services_query->num_rows() > 0)
		// {
		// 	foreach($services_query->result() as $res)
		// 	{
		// 		$parent_title = $res->post_title;
		// 		$web_name = $this->create_web_name($post_title);


		// 		$services_query = $this->get_active_items($parent_title);
		// 		$services_sub_menu_services .= ' <li class="parent"><a href="services.html">'.$parent_title.'</a>';
		// 		if($services_query->num_rows() > 0)
		// 		{
		// 			foreach($services_query->result() as $res)
		// 			{
		// 				$post_title = $res->post_title;
		// 				$web_name = $this->create_web_name($post_title);
		// 				$services_sub_menu_services .= '
		// 												 <li class="parent"><a href="services.html">'.$post_title.'</a>
		// 								                	<ul>
		// 								                        <li class="parent"><a href="doctor.html">doctor single</a>
		// 								                        </li>
		// 								                        <li class="parent"><a href="our_doctors.html">our doctors</a>
		// 								                        </li>

		// 								                    </ul>
		// 								                </li>

		// 												<li><a href="'.site_url().'our-services/'.$web_name.'" title="'.$post_title.'"> '.$post_title.'</a></li>';
		// 			}
		// 		}

		// 	}
		// }
			// <li class="nav-item dropdown">
   //              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		 //          ABOUT US
		 //        </a>
		 //        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		 //        	<a class="dropdown-item" href="'.site_url().'about-us" title="">About Us</a>
			// 		<a class="dropdown-item" href="'.site_url().'about-us/our-history" title="">Our History</a>
			// 		<a class="dropdown-item" href="'.site_url().'about-us/policy-and-governance" title="">Policy and Governance</a>
			//     </div>
              
   //          </li>


		$navigation = 
		'
            <li ><a href="'.site_url().'home">Home</a></li>
            <li ><a href="'.site_url().'about-us">About Us</a></li>
            <li ><a href="'.site_url().'our-courses">Courses</a></li>
            <li ><a href="'.site_url().'contact-us">Contact Us</a></li>
            
         
			
			';
		
		return $navigation;
	}

	public function get_side_bar()
	{

		//get departments]
		$projects_query = $this->get_active_items('Company Programmes');
		$projects_sub_menu_projects = '';
		if($projects_query->num_rows() > 0)
		{
			foreach($projects_query->result() as $res)
			{
				$post_title = $res->post_title;
				$post_id = $res->post_id;
				$web_name = $this->create_web_name($post_title);
				$projects_sub_menu_projects .= '<li><a href="'.site_url().'programmes/'.$web_name.'/'.$post_id.'">'.$post_title.'</a></li>';
			}
		}


		$projects_query = $this->get_active_items('Company Opportunities');
		$company_sub_menu_opportunities = '';
		if($projects_query->num_rows() > 0)
		{
			foreach($projects_query->result() as $res)
			{
				$post_title = $res->post_title;
				$web_name = $this->create_web_name($post_title);
				$company_sub_menu_opportunities .= '<li><a href="'.site_url().'opportunities/'.$web_name.'/'.$post_id.'">'.$post_title.'</a></li>';
			}
		}

		$navigation = 
		'
		  	<li ><a href="'.site_url().'home">Home</a></li>
            <li ><a href="'.site_url().'about-us">About Us</a></li>
            <li ><a href="#" class="tree-toggler nav-header">Programmes</a>
	            <ul  class="nav nav-list tree">
	              '.$projects_sub_menu_projects.'
	            </ul>
	        </li>
			<li ><a href="'.site_url().'membership">Membership</a></li>
			<li ><a href="'.site_url().'initiatives">Initiatives</a></li>
             
            <li ><a href="#" class="tree-toggler nav-header">Opportunities</a>
                <ul  class="nav nav-list tree">
                	'.$company_sub_menu_opportunities.'

                </ul>
            </li>
            <li ><a href="'.site_url().'events">Events</a></li>
            <li ><a href="'.site_url().'blog">Blog</a></li>
            <li ><a href="'.site_url().'contact-us">Contact Us</a></li>

			
			';
		
		return $navigation;
	}
	
	
	public function get_header_item()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		$last_uri = $this->get_last_uri();
		
		$name = strtolower($page[0]);
		$name2 = $name3 = '';
		$title = $name;
		if(isset($page[1]))
		{
			$name2 = $page[1];
			$title = $name2;
		}
		if(isset($page[2]))
		{
			$name3 = $page[2];
			$title = $name3;
		}
		if(isset($page[3]))
		{
			$name4 = $page[3];
			$title = $name4;
		}
		$item = '';
		$title = ucwords($this->decode_web_name($title));
		
		if($name == 'blog')
		{
			$item = '<div class="cp_inner-banner">
					    <img src="'.base_url().'assets/themes/taxigo/images/banner/inner-banner-img-08.jpg" alt="">
					    <!--Inner Banner Holder Start-->
					    <div class="cp-inner-banner-holder">
					        <div class="container">
					        <h2>Blog</h2>
					          <!--Breadcrumb Start-->
					           <ul class="breadcrumb">
					             <li><a href="'.site_url().'">Home</a></li>
					             <li class="active">Blog</li>
					           </ul><!--Breadcrumb End-->
					        </div>
					  
					    </div><!--Inner Banner Holder End-->
					    <div class="animate-bus">
					        <img src="'.base_url().'assets/themes/taxigo/images/animate-bus2.png" alt="">
					    </div>
					</div>
					';
		}
		
		if($name == 'about-us')
		{
			//Title
			$title = $this->decode_web_name($name2);
			
			$category_id = $this->get_category_id('About Us');
			// service number two
			$about_query = $this->get_active_child_items($category_id);
			$about_sub_menu_services = '';
			if($about_query->num_rows() > 0)
			{
				foreach($about_query->result() as $res)
				{
					$blog_category_name = $res->blog_category_name;
					$web_name = $this->create_web_name($blog_category_name);
					$about_sub_menu_services .= '<li><a href="'.site_url().'about-us/'.$web_name.'">'.$blog_category_name.'</a></li>';
				}
			}
			$image = 'about-bio.jpg';		
			$header_image = $this->get_header_image($last_uri);
			
			if($header_image != NULL)
			{
				$image = base_url()."assets/images/posts/".$header_image;
			}
			
			else
			{
				$image = base_url()."assets/images/".$image;
			}
			/*if($name2 == 'Company-Bio')
			{
				$image = 'about-bio.jpg';
			}
			else if($name2 == 'Code-Of-Ethics')
			{
				$image = 'about-news2.jpg';
			}
			else if($name2 == 'News-&-Upcoming-Events')
			{
				$image = 'about-news2.jpg';
			}
			else if($name2 == 'Client-Reviews')
			{
				$image = 'about-news2.jpg';
			}
			else if($name2 == 'Professional-Affiliations')
			{
				$image = 'about-news2.jpg';
			}
			else if($name2 == 'Mission')
			{
				$image = 'about-mission3.jpg';
			}*/
			
			//$image = base_url()."assets/images/".$image;
			$item = '<div class="inner-breadcrumb">
					    <div class="container">
					      <ul>
					        '.$about_sub_menu_services.'
					      </ul>
					    </div>
					  </div>
					  <div class="inner-banner" style="background-image: url('.$image.');">
					    <div class="container">
					      <h2>'.$title.'</h2>
					    </div>
					  </div>
					';
		}
		
		if($name == "our-services")
		{
			$page = explode("/",uri_string());
			$total = count($page);
			$last = $total - 1;
			$name = strtolower($page[$last]);
			
			$title = $this->decode_web_name($name);
			$title_item = ucwords($this->decode_web_name($page[$last]));
			$title_item = str_replace("And", 'and', $title_item);
			// var_dump($title_item); die();
			$image = 'our-services.jpg';
			
			$header_image = $this->get_header_image($last_uri);
			
			if($header_image != NULL)
			{
				$image = base_url()."assets/images/posts/".$header_image;
			}
			
			else
			{
				$image = base_url()."assets/images/".$image;
			}
			
			$item = '<div class="inner-breadcrumb">
					    
					  </div>
					  <div class="inner-banner" style="background-image: url('.$image.');">
					    <div class="container">
					      <h2>'.$title_item.'</h2>
					    </div>
					  </div>
					';
		}
		
		if($name == "our-services")
		{
			$category_search = 'Services List';
			$services_query = $this->get_active_items($category_search);
			$services_sub_menu_services = '';
			if($services_query->num_rows() > 0)
			{
				foreach($services_query->result() as $res)
				{
					$post_title = $res->post_title;
					$web_name = $this->create_web_name($post_title);
					$services_sub_menu_services .= '<li><a href="'.site_url().'our-services/'.$web_name.'">'.$post_title.'</a></li>';
				}
			}
			$image = 'about-services.jpg';
			
			if($name2 == 'Geo-Technical-Engineering')
			{
				if(!empty($name4))
				{
					$category_search = $this->decode_web_name($name4);
				}
				else if(!empty($name3))
				{
					$category_search = $this->decode_web_name($name3);
				}
				else
				{
					$category_search = $this->decode_web_name($name2);
				}
				
				$services_query = $this->get_active_items($category_search);
				$services_sub_menu_services = '';
				if($services_query->num_rows() > 0)
				{
					foreach($services_query->result() as $res)
					{
						$post_title = $res->post_title;
						$web_name = $this->create_web_name($post_title);
						
						if(!empty($name4))
						{
							$services_sub_menu_services .= '<li><a href="'.site_url().'our-services/'.$name2.'/'.$name3.'/'.$name4.'/'.$web_name.'">'.$post_title.'</a></li>';
						}
						else if(!empty($name3))
						{
							$services_sub_menu_services .= '<li><a href="'.site_url().'our-services/'.$name2.'/'.$name3.'/'.$web_name.'">'.$post_title.'</a></li>';
						}
						else
						{
							$services_sub_menu_services .= '<li><a href="'.site_url().'our-services/'.$name2.'/'.$web_name.'">'.$post_title.'</a></li>';
						}
					}
				}
			}
			else if($name2 == 'Construction-Sites-Material-Testing')
			{
				$image = 'services-head-2.jpg';
			}
			else if($name2 == 'ICC-Special-Materials-Testing')
			{
				$image = 'services-head-3.jpg';
			}
			else if($name2 == 'News-And-Upcoming-Events')
			{
				$image = 'services-head-4.jpg';
			}
			
			$last_uri = $this->get_last_uri();			
			$header_image = $this->get_header_image($last_uri);
			
			if($header_image != NULL)
			{
				$image = base_url()."assets/images/posts/".$header_image;
			}
			
			else
			{
				$image = base_url()."assets/images/".$image;
			}
			
			$item = '
						<div class="inner-breadcrumb">
						    <div class="container">
						      <ul>
						        '.$services_sub_menu_services.'
						      </ul>
						    </div>
						  </div>
						  <div class="inner-banner" style="background-image: url('.$image.');">
						    <div class="container">
						      <h2>'.$title_item.'</h2>
						    </div>
						  </div>
					';
		}

		if($name == "our-people")
		{
			$image = 'people-head-1.jpg';
			
			$last_uri = $this->get_last_uri();			
			$header_image = $this->get_header_image($last_uri);
			
			if($header_image != NULL)
			{
				$image = base_url()."assets/images/posts/".$header_image;
			}
			
			else
			{
				$image = base_url()."assets/images/".$image;
			}
			
			$item = '
						<div class="inner-breadcrumb">
						    <div class="container">
						      <ul>
						        <li><a href="'.site_url().'">Home</a></li>
						        <li>'.$title.'</li>
						      </ul>
						    </div>
						  </div>
						  <div class="inner-banner" style="background-image: url('.$image.');">
						    <div class="container">
						      <h2>Our People</h2>
						    </div>
						  </div>
					';
		}
		if($name == "our-projects")
		{
			$title = $this->decode_web_name($name2);
			
			$category_id = $this->get_category_id('Company Projects');
			// service number two
			$about_query = $this->get_active_child_items($category_id);
			$company_sub_menu_services = '';
			if($about_query->num_rows() > 0)
			{
				foreach($about_query->result() as $res)
				{
					$blog_category_name = $res->blog_category_name;
					$web_name = $this->create_web_name($blog_category_name);
					$company_sub_menu_services .= '<li><a href="'.site_url().'our-projects/'.$web_name.'">'.$blog_category_name.'</a></li>';
				}
			}
			$item = '
						<div class="inner-breadcrumb">
						    <div class="container">
						      <ul>
						        <li><a href="'.site_url().'">Home</a></li>
						        '.$company_sub_menu_services.'
						      </ul>
						    </div>
						  </div>
					';
		}

		if($name == "contact")
		{
			$item = '
					  <div class="inner-banner" style="background-image: url('.base_url()."assets/images/contact-us.jpg".'); margin-top:98px">
					    <div class="container">
					      <h2>'.$title.'</h2>
					    </div>
					  </div>
					';
					
			/*$item = '
					<div id="googleMap">
					    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1927.0899234726962!2d-149.87292188413718!3d61.11835338232018!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x56c899bd8f88dd8f%3A0x1207445908c8b769!2s11301+Olive+Ln%2C+Anchorage%2C+AK+99515%2C+USA!5e0!3m2!1sen!2ske!4v1490640452349"frameborder="0" style="border:0" allowfullscreen></iframe>
					  </div>
					';*/
		}

		if($name == "useful-info")
		{
			$item = '
					<div class="inner-breadcrumb">
					    <div class="container">
					      <ul>
					        <li><a href="'.site_url().'">Home</a></li>
					        <li>'.$title.'</li>
					      </ul>
					    </div>
					  </div>
					';
		}
		if($name == "search-result")
		{
			$image = 'services-head-2.jpg';
			$item = '
						<div class="inner-breadcrumb">
						    
						  </div>
						  <div class="inner-banner">
						    <div class="container">
						      <h2>'.$title.'</h2>
						    </div>
						  </div>
					';
		}

		if($name == "home")
		{
			
			
			$time = $seconds = '';
			for($r = 0; $r <= 24; $r++)
			{
				$time .= '<option value="'.sprintf("%02d", $r).'">'.sprintf("%02d", $r).'</option>';
			}
			for($r = 0; $r <= 60; $r++)
			{
				$seconds .= '<option value="'.sprintf("%02d", $r).'">'.sprintf("%02d", $r).'</option>';
			}

			$error = $this->session->userdata('booking_error_message');
			$success = $this->session->userdata('booking_success_message');
			$item_check ='';
			
			if(!empty($success))
			{
				$item_check =  '
					<div class="alert alert-success">'.$success.'</div>
				';
				$this->session->unset_userdata('booking_success_message');
			}
			
			if(!empty($error))
			{
				$item_check = '
					<div class="alert alert-danger">'.$error.'</div>
				';
				$this->session->unset_userdata('booking_error_message');
			}

			//  end of getting slide shows from the database
			$item = '

					';
		}
		return $item;

	}
	public function get_active_departments($service_name)
	{
  		$table = "service, department";
		$where = "service.service_status = 1 AND department.department_status = 1 AND service.department_id = department.department_id AND department.department_name = '".$service_name."'";
		
		$this->db->select('service.*');
		$this->db->where($where);
		$this->db->group_by('service_name');
		$this->db->order_by('service_name', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}

	public function get_active_services()
	{
  		$table = "service";
		$where = "service.service_status = 1 AND department_id = 2";
		
		$this->db->select('service.*');
		$this->db->where($where);
		$this->db->group_by('service_name');
		$this->db->order_by('service_name', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_active_service_gallery($service_id)
	{
		$table = "service_gallery";
		$where = "service_id = ".$service_id;
		
		$this->db->select('service_gallery.*');
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_active_gallery()
	{
		$table = "gallery";
		$where = "gallery_status > 0";
		
		$this->db->select('gallery.*');
		$this->db->where($where);
		
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_active_service_gallery_names()
	{
		$table = "gallery";
		$where = "gallery_status > 0";
		
		$this->db->select('gallery.*');
		$this->db->where($where);
		$this->db->group_by('gallery_name');
		$query = $this->db->get($table);
		
		return $query;
	}
	public function create_web_name($field_name)
	{
		$web_name = str_replace(" ", "-", $field_name);
		
		return $web_name;
	}
	public function get_posts($table, $where, $limit = NULL)
	{
		$this->db->where($where);
		$this->db->select('post.*');
		$this->db->order_by('post_title', 'ASC');
		$query = $this->db->get($table);
		
		
		return $query;
	}

	public function get_post_title($post_id)
	{
		$this->db->where('post_id',$post_id);
		$this->db->select('post.*');
		$this->db->order_by('post_title', 'ASC');
		$query = $this->db->get('post');
		
		
		return $query;
	}

	public function get_post_by_name($post_name)
	{
		$this->db->where('post_title = "'.$post_name.'" AND blog_category.blog_category_id = post.blog_category_id');
		$this->db->select('post.*,blog_category.*');
		$this->db->order_by('post_title', 'ASC');
		$query = $this->db->get('post,blog_category');
		
		
		return $query;
	}
	public function get_services($table, $where, $limit = NULL)
	{
		$this->db->where($where);
		$this->db->select('service.*');
		$this->db->order_by('service_name', 'ASC');
		$query = $this->db->get($table);
		
		
		return $query;
	}
	
	public function decode_web_name($web_name)
	{
		$field_name = str_replace("-", " ", $web_name);
		
		return $field_name;
	}
	
	public function image_display($base_path, $location, $image_name = NULL)
	{
		$default_image = 'http://placehold.it/300x300&text=IOD';
		$file_path = $base_path.'/'.$image_name;
		//echo $file_path.'<br/>';
		
		//Check if image was passed
		if($image_name != NULL)
		{
			if(!empty($image_name))
			{
				if((file_exists($file_path)) && ($file_path != $base_path.'\\'))
				{
					return $location.$image_name;
				}
				
				else
				{
					return $default_image;
				}
			}
			
			else
			{
				return $default_image;
			}
		}
		
		else
		{
			return $default_image;
		}
	}
	
	public function get_contacts()
	{
  		$table = "contacts";
		
		$query = $this->db->get($table);
		$contacts = array();
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$contacts['email'] = $row->email;
			$contacts['phone'] = $row->phone;
			$contacts['facebook'] = $row->facebook;
			$contacts['twitter'] = $row->twitter;
			$contacts['youtube'] = $row->youtube;
			$contacts['instagram'] = $row->instagram;
			//$contacts['linked_in'] = $row->linked_in;
			$contacts['oemail'] = $row->email;
			$contacts['company_name'] = $row->company_name;
			$contacts['logo'] = $row->logo;
			$contacts['address'] = $row->address;
			$contacts['city'] = $row->city;
			$contacts['post_code'] = $row->post_code;
			$contacts['building'] = $row->building;
			$contacts['floor'] = $row->floor;
			$contacts['location'] = $row->location;
			$contacts['working_weekend'] = $row->working_weekend;
			$contacts['working_weekday'] = $row->working_weekday;
			$contacts['mission'] = $row->mission;
			$contacts['vision'] = $row->vision;
			$contacts['motto'] = $row->motto;
			$contacts['about'] = $row->about;
			$contacts['objectives'] = $row->objectives;
			$contacts['core_values'] = $row->core_values;
			$contacts['corporate_values'] = '';
		}
		
		// about
		$services_query = $this->get_active_departments('About');
		$director_development_services = '';
		if($services_query->num_rows() > 0)
		{
			foreach($services_query->result() as $res)
			{
				$service_name = $res->service_name;
				$service_description = $res->service_description;
				
				if($service_name == 'Corporate Values & Principles')
				{
					$contacts['corporate_values'] = $service_description;
				}
			}
		}
		return $contacts;
	}

	public function get_all_branches()
	{
		$table = "branch";
		
		$query = $this->db->get($table);

		return $query;
	}
	
	public function get_breadcrumbs()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		$last = $total - 1;
		$crumbs = '<li style="font-size: 8px;"><a href="'.site_url().'home">HOME </a></li>';
		
		for($r = 0; $r < $total; $r++)
		{
			$name = $this->decode_web_name($page[$r]);
			if($r == $last)
			{
				$crumbs .= '<li class="active" style="font-size: 8px;">'.strtoupper($name).'</li>';
			}
			else
			{
				$link = site_url();
				for($s = 0; $s <= $r; $s++)
				{
					$link_name = $this->create_web_name($page[$s]);
					$link .= $link_name.'/';
				}
				$crumbs .= '<li style="font-size: 8px;"><a href="'.$link.'">'.strtoupper($name).'</a></li>';
			}
		}
		
		return $crumbs;
	}
	public function contact() 
	{
		$this->load->model('site/email_model');
		
		//Notify admin
		$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$message['subject'] =  $this->input->post('sender_name')." needs some help";
		$message['text'] = '<p>A help message was sent on '.$date.' saying:</p> 
				<p>'.$this->input->post('message').'</p>
				<p>Their contact details are:</p>
				<p>
					Name: '.$this->input->post('sender_name').'<br/>
					Email: '.$this->input->post('sender_email').'<br/>
					Phone: '.$this->input->post('sender_phone').'
				</p>';
		$message['text'] = $this->load->view('compose_mail', $message, TRUE);
		
		$sender['email'] = $branch_email;
		$sender['name'] = $branch_name;
		$receiver['email'] = $personnel_email;
		$receiver['name'] = $personnel_fname.' '.$personnel_onames;
	
		$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
		
		$this->load->library('Mandrill', $this->config->item('mandrill_key'));
		
		$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
		$subject = $this->input->post('sender_name')." needs some help";
		$message = '
				<p>A help message was sent on '.$date.' saying:</p> 
				<p>'.$this->input->post('message').'</p>
				<p>Their contact details are:</p>
				<p>
					Name: '.$this->input->post('sender_name').'<br/>
					Email: '.$this->input->post('sender_email').'<br/>
					Phone: '.$this->input->post('sender_phone').'
				</p>
				';
		$sender_email = $this->input->post('sender_email');
		$shopping = "";
		$from = $this->input->post('sender_name');
		
		$button = '';
		$response = $this->email_model->send_mandrill_mail('info@instorelook.com.au', "Hi", $subject, $message, $sender_email, $shopping, $from, $button, $cc = NULL);
		
		//echo var_dump($response);
		
		return $response;
	}
	
	public function get_neighbourhoods()
	{
		$this->db->order_by('neighbourhood_name');
		return $this->db->get('neighbourhood');
	}
	public function get_testimonials()
	{
		$this->db->where('post.blog_category_id = blog_category.blog_category_id AND (blog_category.blog_category_name LIKE "%testimonials%") AND post.post_status = 1');
		$this->db->order_by('post.created','ASC');
		return $this->db->get('post,blog_category');
	}
	public function get_faqs()
	{
		$this->db->where('post.blog_category_id = blog_category.blog_category_id AND (blog_category.blog_category_name LIKE "%faqs%") AND post.post_status = 1');
		$this->db->order_by('post.created','ASC');
		return $this->db->get('post,blog_category');
	}
	public function get_front_end_items()
	{
		$this->db->where('post.blog_category_id = blog_category.blog_category_id AND (blog_category.blog_category_name LIKE "%front%") AND post.post_status = 1');
		$this->db->order_by('post.created','ASC');
		$this->db->limit(1);
		return $this->db->get('post,blog_category');
	}
	
	public function valid_url($url)
	{
		$pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
		//$pattern = "/^((ht|f)tp(s?)\:\/\/|~/|/)?([w]{2}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?/";
        if (!preg_match($pattern, $url))
		{
            return FALSE;
        }
 
        return TRUE;
	}
	
	public function get_days($date)
	{
		$now = time(); // or your date as well
		$your_date = strtotime($date);
		$datediff = $now - $your_date;
		return floor($datediff/(60*60*24));
	}
	
	public function limit_text($text, $limit) 
	{
		$pieces = explode(" ", strip_tags($text));
		$total_words = count($pieces);
		
		if ($total_words > $limit) 
		{
			$return = "";
			$count = 0;
			for($r = 0; $r < $total_words; $r++)
			{
				$count++;
				if(($count%$limit) == 0)
				{
					$return .= $pieces[$r]."<br/>";
				}
				else{
					$return .= $pieces[$r]." ";
				}
			}
		}
		
		else{
			$return = "<i>".$text;
		}
		return $return.'</i><br/>';
    }

    public function get_all_resources($table, $where, $per_page, $page)
	{
		//retrieve all trainings
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('resource_category.resource_category_id', 'DESC');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	public function view_single_post($post_title)
	{
		$this->db->where('post_title = $post_title');
		$this->db->select('*');
		return $query = $this->db->get('post');
	}
	public function get_event($training_id)
	{
		$this->db->where('training_id  = '.$training_id);
		$this->db->select('*');
		return $query = $this->db->get('training');
	}

	public function get_event_id($training_name)
	{
		//retrieve all users
		$this->db->from('training');
		$this->db->select('training_id');
		$this->db->where('training_name', $training_name);
		$query = $this->db->get();
		$training_id = FALSE;
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$training_id = $row->training_id;
		}
		
		return $training_id;
	}

	public function get_crumbs()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		
		$crumb[0]['name'] = ucwords(strtolower($page[0]));
		$crumb[0]['link'] = $page[0];
		
		if($total > 1)
		{
			$sub_page = explode("-",$page[1]);
			$total_sub = count($sub_page);
			$page_name = '';
			
			for($r = 0; $r < $total_sub; $r++)
			{
				$page_name .= ' '.$sub_page[$r];
			}
			$crumb[1]['name'] = ucwords(strtolower($page_name));
			
			if($page[1] == 'category')
			{
				$category_id = $page[2];
				$category_details = $this->categories_model->get_category($category_id);
				
				if($category_details->num_rows() > 0)
				{
					$category = $category_details->row();
					$category_name = $category->category_name;
				}
				
				else
				{
					$category_name = 'No Category';
				}
				
				$crumb[1]['link'] = 'products/all-products/';
				$crumb[2]['name'] = ucwords(strtolower($category_name));
				$crumb[2]['link'] = 'products/category/'.$category_id;
			}
			
			else if($page[1] == 'brand')
			{
				$brand_id = $page[2];
				$brand_details = $this->brands_model->get_brand($brand_id);
				
				if($brand_details->num_rows() > 0)
				{
					$brand = $brand_details->row();
					$brand_name = $brand->brand_name;
				}
				
				else
				{
					$brand_name = 'No Brand';
				}
				
				$crumb[1]['link'] = 'products/all-products/';
				$crumb[2]['name'] = ucwords(strtolower($brand_name));
				$crumb[2]['link'] = 'products/brand/'.$brand_id;
			}
			
			else if($page[1] == 'view-product')
			{
				$product_id = $page[2];
				$product_details = $this->products_model->get_product($product_id);
				
				if($product_details->num_rows() > 0)
				{
					$product = $product_details->row();
					$product_name = $product->product_name;
				}
				
				else
				{
					$product_name = 'No Product';
				}
				
				$crumb[1]['link'] = 'products/all-products/';
				$crumb[2]['name'] = ucwords(strtolower($product_name));
				$crumb[2]['link'] = 'products/view-product/'.$product_id;
			}
			
			else
			{
				$crumb[1]['link'] = '#';
			}
		}
		
		return $crumb;
	}
	
	public function get_resource_category_id($resource_category_name)
	{
		//retrieve all users
		$this->db->from('resource_category');
		$this->db->where('resource_category_name', $resource_category_name);
		$query = $this->db->get();
		$return['resource_category_id'] = FALSE;
		$return['member_only'] = FALSE;
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$return['resource_category_id'] = $row->resource_category_id;
			$return['member_only'] = $row->member_only;
		}
		
		return $return;
	}
	public function get_service_id($about_name)
	{
		$this->db->from('service');
		$this->db->select('service_id');
		$this->db->where('service_name', $about_name);
		$query = $this->db->get();
		$service_id = FALSE;
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$service_id = $row->service_id;
		}
		
		return $service_id;
	}
	
	public function get_event_type_id($event_type_name)
	{
		//retrieve all users
		$this->db->from('event_type');
		$this->db->select('event_type_id');
		$this->db->where('event_type_name', $event_type_name);
		$query = $this->db->get();
		$event_type_id = FALSE;
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$event_type_id = $row->event_type_id;
		}
		
		return $event_type_id;
	}

	public function get_event_item($event_type_id)
	{
		$this->db->where('event_type.event_type_id = event.event_type_id AND event_start_time >= CURDATE() AND event_type.event_type_id = '.$event_type_id);
		$this->db->select('*');
		return $query = $this->db->get('event, event_type');
	}

	public function get_resource_item($resource_category_id)
	{
		$this->db->where('resource_category_id  = '.$resource_category_id);
		$this->db->select('*');
		return $query = $this->db->get('resource');
	}
	public function get_resource_category($resource_category_id)
	{

		$this->db->where('resource_category_id  = '.$resource_category_id);
		$this->db->select('*');
		return $query = $this->db->get('resource_category');

	}
	public function get_about_item($service_id)
	{
		$this->db->where('service_id  = '.$service_id);
		$this->db->select('*');
		return $query = $this->db->get('service');
	}
	public function get_directors()
	{
		$table = "directors";
		$where = "directors_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_member_details($directors_name)
	{
  		$table = "directors";
		$where = "directors_name = '".$directors_name."'";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_facilitators()
	{
		$table = "facilitators";
		$where = "facilitators_status = 1";
		
		$this->db->where($where);
		$query = $this->db->get($table);
		
		return $query;
	}
	
	public function get_tweets_old()
	{
		$this->load->library('twitterfetcher');
	
		$tweets = $this->twitterfetcher->getTweets(array(
			'consumerKey'       => 'fZvEA9Mw24i2jT3VIn1sIz92y',
			'consumerSecret'    => 'NW3rzs0jEv39JdSmNeurZvKL577vxPVLuV95vedROczQtQIbDp',
			'accessToken'       => '588425913-dHNleDnlFPdfHGYjZpUnph7MEhKTXqULJ6OaP6IP',
			'accessTokenSecret' => 'iMmuv0bADX4CbG3i0T1vDvGo1uRYlAWfb5khB9Qfm7v3m',
			'usecache'          => true,
			'count'             => 5,  //this how many tweets to fectch
			'numdays'           => 3000
		));
		
		return $tweets;
	}
	
	public function get_tweets()
	{
		$this->load->library('twitteroauth');
		$consumer = 'fZvEA9Mw24i2jT3VIn1sIz92y';
		$consumer_secret = 'NW3rzs0jEv39JdSmNeurZvKL577vxPVLuV95vedROczQtQIbDp';
		$access_token = '588425913-dHNleDnlFPdfHGYjZpUnph7MEhKTXqULJ6OaP6IP';
		$access_token_secret = 'iMmuv0bADX4CbG3i0T1vDvGo1uRYlAWfb5khB9Qfm7v3m';
		
		//Create an instance
		$connection = $this->twitteroauth->create($consumer, $consumer_secret, $access_token, $access_token_secret);
	
		//Verify your authentication details
		$content = $connection->get('account/verify_credentials');
	}
	
	public function get_facilitator($facilitators_name)
	{
		//retrieve all users
		$this->db->from('facilitators');
		$this->db->where('facilitators_name', $facilitators_name);
		$query = $this->db->get();
		
		return $query;
	}
	
	
	public function import_csv_payments($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		//var_dump($response); die();
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_payments_data($array);
			
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_payments_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);//var_dump($array);die();
		
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 2))
		{
			$items['modified_by'] = $this->session->userdata('personnel_id');
			$response = '
				<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Member Number</th>
						  <th>Member Name</th>
						  <th>Amount</th>
						  <th>Comment</th>
						</tr>
					  </thead>
					  <tbody>
			';
			//var_dump($array);die();
			//retrieve the data from array
			$this->load->model('member/member_model');
			$this->load->model('member/invoices_model');
			
			for($r = 1; $r < $total_rows; $r++)
			{
				$member_number_csv = $array[$r][0];
				$member_name = $array[$r][1];
				
				//get member id
				$this->db->where('member.member_number = \''.$member_number_csv.'\'');
				$query_member = $this->db->get('member');
				
				//if member exists
				if($query_member->num_rows() > 0)
				{
					$member_row = $query_member->row();
					$member_id = $member_row->member_id;
					//get invoice_id
					$comment = 'Payment not added';
					$class = 'danger';
			
					//get member invoice
					$this->db->where('invoice.member_id = '.$member_id);
					$query = $this->db->get('invoice');
					
					if($query->num_rows() > 0)
					{
						$res = $query->row();
						$invoice_id = $res->invoice_id;
					}
					
					else
					{
						$invoice_id = $this->invoices_model->create_member_invoice($member_id);
					}
					
					//check if invoice has been paid already
					$this->db->where(array('member_id' => $member_id, 'invoice_id' => $invoice_id));
					$invoice_query = $this->db->get('payment');
					
					if($invoice_query->num_rows() == 0)
					{
						$items = array(
						   'invoice_id'				=> $invoice_id,
						   'member_id'				=> $member_id,
						   'payment_date'     		=> date('Y-m-d H:i:s'),
						   'payment_amount'     	=> 12000,
						   'reciept_number'			=> $this->member_model->generate_receipt_number()
						);
						if($this->db->insert('payment', $items))
						{
							$comment = '<br/>Payment successfully added to the database';
							$class = 'success';
							
							//update invoice status
							$this->db->where('invoice_id', $invoice_id);
							if($this->db->update('invoice', array('invoice_status'=>8)))
							{
							}
						}
						
						else
						{
							$comment = '<br/>Internal error. Could not add payment to the database. Please contact the site administrator';
							$class = 'danger';
						}
					}
						
					else
					{
						$comment = '<br/>Payment not added. It already exists';
						$class = 'danger';
					}
				}
				
				else
				{
					$comment = '<br/>Cannot add payment. Member does not exist. Please add them first';
					$class = 'danger';
				}
				
				$response .= '
						<tr class="'.$class.'">
							<td>'.$r.'</td>
							<td>'.$member_number_csv.'</td>
							<td>'.$member_name.'</td>
							<td>12,000</td>
							<td>'.$comment.'</td>
						</tr> 
				';
			}
			
			$response .= '</table>';
			
			$return['response'] = $response;
			$return['check'] = TRUE;
		}
		
		//if no products exist
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}
		
		return $return;
	}
	
	public function check_current_number_exisits($member_number)
	{
		$this->db->where('member_number', $member_number);
		
		$query = $this->db->get('member');
		
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	public function get_active_items($category_name, $limit = NULL, $order_by = 'post_id', $order_method = 'ASC')
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}

		if($category_name == 'Company Projects')
		{
			$add = ' AND featured_status = 1';
		}
		else
		{
			$add = '';
		}
  		$table = "blog_category, post";
		$where = "post.blog_category_id = blog_category.blog_category_id AND blog_category.blog_category_name = '".$category_name."' AND post.post_status = 1 ".$add;
		
		$this->db->select('post.*,blog_category.*, post.created AS post_date');
		$this->db->where($where);
		//$this->db->group_by('post_title');
		$this->db->order_by($order_by, $order_method);
		$query = $this->db->get($table);
		
		return $query;
	}
	
	public function get_category_id($category_name,$limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
  		$table = "blog_category";
		$where = "blog_category.blog_category_name = '".$category_name."'";
		
		$this->db->select('blog_category.*');
		$this->db->where($where);
		$this->db->group_by('blog_category_id');
		$this->db->order_by('blog_category_id', 'ASC');
		$query = $this->db->get($table);
		$category_id = 0;
		foreach ($query->result() as $key) {
			# code...
			$category_id = $key->blog_category_id;
		}
		
		return $category_id;
	}

	public function get_active_child_items($category_id,$limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
  		$table = "blog_category";
		$where = "blog_category.blog_category_parent = ".$category_id."";
		
		$this->db->select('blog_category.*');
		$this->db->where($where);
		$this->db->group_by('blog_category_name');
		$this->db->order_by('blog_category_id', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}

	public function get_active_content_items($post_title, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
  		$table = "post,blog_category";
		$where = "post.post_status = 1 AND blog_category.blog_category_id = post.blog_category_id AND post.post_title = '".$post_title."'";
		
		$this->db->select('post.*,blog_category.*');
		$this->db->where($where);
		$this->db->group_by('post_id');
		$this->db->order_by('post_title', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}
	

	public function get_active_post_content($post_id, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
  		$table = "post,blog_category";
		$where = "post.post_status = 1 AND blog_category.blog_category_id = post.blog_category_id AND post.post_id = '".$post_id."'";
		
		$this->db->select('post.*,blog_category.*');
		$this->db->where($where);
		$this->db->group_by('post_id');
		$this->db->order_by('post_title', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}

	public function get_active_post_content_by_category($blog_category_id, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		// var_dump($blog_category_id);die();
  		$table = "post,blog_category";
		$where = "post.post_status = 1 AND blog_category.blog_category_id = post.blog_category_id AND blog_category.blog_category_id = '".$blog_category_id."'";
		
		$this->db->select('post.*,blog_category.*');
		$this->db->where($where);
		$this->db->group_by('post_id');
		$this->db->order_by('post_title', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}	

	public function get_active_post_content_by_category_parent($blog_category_id, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
			// var_dump($blog_category_id);die();
  		$table = "post,blog_category";
		$where = "post.post_status = 1 AND blog_category.blog_category_id = post.blog_category_id AND blog_category.blog_category_parent = '".$blog_category_id."'";
		
		$this->db->select('post.*,blog_category.*');
		$this->db->where($where);
		$this->db->group_by('post_id');
		$this->db->order_by('post_title', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}	
	public function get_post_gallery($post_id)
	{
		$this->db->where('post_id', $post_id);
		$query = $this->db->get('post_gallery');
		
		return $query;
	}
	
	public function get_last_uri()
	{
		$page = explode("/",uri_string());
		$total = count($page);
		$last = $total - 1;
		$name = strtolower($page[$last]);
		
		$title = ucwords($this->decode_web_name($name));
		
		return $title;
	}
	
	public function get_header_image($post_title)
	{
		$this->db->where('post_title', $post_title);
		$query = $this->db->get('post');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$header_image = $row->post_header;
			
			return $header_image;
		}
		
		else
		{
			return NULL;
		}
	}
	
	public function check_parent($post_title)
	{
		$this->db->where(array('blog_category_name' => $post_title, 'blog_category_status' => 1));
		$query = $this->db->get('blog_category');
		
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	public function get_customer_review_header($post_id)
	{
		$this->db->select('*');
		$this->db->where('post_id = '.$post_id);
		$query =$this->db->get('post');
		
		return $query;
	}
	
	public function search_posts($search)
	{
		$this->db->where('blog_category.blog_category_id = post.blog_category_id AND (post.post_title LIKE \'%'.$search.'%\' OR post.post_content LIKE \'%'.$search.'%\' OR blog_category.blog_category_name LIKE \'%'.$search.'%\')');
		$query = $this->db->get('post, blog_category');
		
		return $query;
	}
	
	public function order_posts($posts_query, $search)
	{
		$this->db->where('blog_category.blog_category_id = post.blog_category_id AND (post.post_title LIKE \'%'.$search.'%\' OR post.post_content LIKE \'%'.$search.'%\' OR blog_category.blog_category_name LIKE \'%'.$search.'%\')');
		$posts = $this->db->get('post, blog_category');
		$all_posts = array();
		foreach($posts_query->result() as $row)
		{
			$hit_ratio = 0;
			$post_id = $row->post_id;
			$post_title = $row->post_title;
			$post_content = $row->post_content;
			$blog_category_name = $row->blog_category_name;
			$blog_category_id = $row->blog_category_id;
			$post_image = $row->post_image;
			
			//calculate hit ratio
			$hit_ratio += substr_count($post_title, $search);
			$hit_ratio += substr_count($post_content, $search);
			$hit_ratio += substr_count($blog_category_name, $search);
			
			$post_data = array
			(
				"hit_ratio" => $hit_ratio,
				"post_id" => $post_id,
				"post_title" => $post_title,
				"post_content" => $post_content,
				"blog_category_name" => $blog_category_name,
				"blog_category_id" => $blog_category_id,
				"post_image" => $post_image,
			);
			
			array_push($all_posts, $post_data);
		}
		
		$all_posts = $this->arrange_posts($all_posts);
		
		return $all_posts;
	}
	
	public function arrange_posts($all_posts)
	{
		$total_posts = count($all_posts);
		$ordered_posts = array();
		
		for($t = 0; $t < $total_posts; $t++)
		{
			for($r = 0; $r < $total_posts; $r++)
			{
				$hit_ratio = $all_posts[$r]['hit_ratio'];
				
				//check next post
				$s = $r+1;
				if($s < $total_posts)
				{
					$next_hit_ratio = $all_posts[$s]['hit_ratio'];
					
					//swap
					if($next_hit_ratio > $hit_ratio)
					{
						$temp = $all_posts[$r];
						$all_posts[$r] = $all_posts[$s];
						$all_posts[$s] = $temp;
					}
				}
			}
		}
		
		return $all_posts;
	}
	
	public function highlight_text($text, $replace)
	{
		//As sent
		$text = str_replace($replace, "<mark>".$replace."</mark>", $text);
		
		//Capitalized
		$replace = ucwords($replace);
		$text = str_replace($replace, "<mark>".$replace."</mark>", $text);
		
		//Lower case
		$replace = strtolower($replace);
		$text = str_replace($replace, "<mark>".$replace."</mark>", $text);
		
		//Upper case
		$replace = strtoupper($replace);
		$text = str_replace($replace, "<mark>".$replace."</mark>", $text);
		return $text;
	}


	public function applicant_registration($post_id,$attachment_name)
	{
		//customer details
		$applicant_name = $this->input->post('applicant_name');
		$applicant_gender = $this->input->post('applicant_gender');
		$applicant_email = $this->input->post('applicant_email');
		$applicant_message = $this->input->post('applicant_message');
		
		
		$data = array(
			'created' => date('Y-m-d H:i:s'),
			'applicant_name' => $this->input->post('applicant_name'),
			'applicant_gender' => $this->input->post('applicant_gender'),
			'applicant_email' => $this->input->post('applicant_email'),
			'applicant_message' => $this->input->post('applicant_message'),
			'application_type'=>$post_id,
			'attachment_name'=>$attachment_name
		);
		
		//check if customer exists
		$this->db->where('applicant_email', $applicant_email);
		$query = $this->db->get('applications');
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$applicant_id = $row->applicant_id;
			//Update last login
			return FALSE;
		}
		
		else
		{
			$this->db->insert('applications',$data);
			return TRUE;
		}
		
	}
	public function get_newsletter()
	{
		$this->db->where(array('newsletter_status' => 1));
		$query = $this->db->get('newsletter_content');
		
		return $query;
	}
	public function get_project_contributions($project_id)
	{
		$this->db->select('SUM(customer_donations.donated_amount) AS total_credit');
		$this->db->where('customer_donation_status = 1 AND project_id = '.$project_id);
		$this->db->from('customer_donations');
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->total_credit;
	}
	public function get_project_contributors($project_id)
	{
		$this->db->select('COUNT(customer_donations.customer_donation_id) AS total_credit');
		$this->db->where('customer_donation_status = 1 AND project_id = '.$project_id);
		$this->db->from('customer_donations');
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->total_credit;
	}
	public function get_applicants($application_type)
	{
		$this->db->select('COUNT(applications.applicant_id) AS total_credit');
		$this->db->where('application_type = '.$application_type);
		$this->db->from('applications');
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->total_credit;
	}

	public function calculate_credit_total($client_id)
	{
		//select the user by email from the database
		$this->db->select('SUM(customer_donations.donated_amount) AS total_credit');
		$this->db->where('customer_donation_status = 1 AND customer_id = '.$client_id);
		$this->db->from('customer_donations');
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->total_credit;
	}
	public function calculate_member_credit_total($client_id)
	{
		//select the user by email from the database
		$this->db->select('SUM(customer_donations.donated_amount) AS total_credit');
		$this->db->where('customer_donation_status = 1 AND member_id = '.$client_id);
		$this->db->from('customer_donations');
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->total_credit;
	}


	public function calculate_credit_total_donations($client_id)
	{
		//select the user by email from the database
		$this->db->select('SUM(customer_donations.donated_amount) AS total_credit');
		$this->db->where('post.post_id = customer_donations.project_id AND customer_donations.customer_donation_status = 1 AND customer.customer_id = customer_donations.customer_id AND post.blog_category_id = blog_category.blog_category_id AND post.applicant_id = '.$client_id);
		$this->db->from('customer_donations,post,blog_category,customer');
		$query = $this->db->get();
		
		$result = $query->row();
		
		return $result->total_credit;
	}

	public function calculate_credit_total_donors($client_id)
	{
		//select the user by email from the database
		$this->db->select('COUNT(customer.customer_id) AS total_credit');
		$this->db->where('post.post_id = customer_donations.project_id AND customer_donations.customer_donation_status = 1 AND customer.customer_id = customer_donations.customer_id AND post.blog_category_id = blog_category.blog_category_id AND post.applicant_id = '.$client_id);
		$this->db->from('customer_donations,post,blog_category,customer');
		$this->db->group_by('customer.customer_id');
		$query = $this->db->get();
		
		$result = $query->row();

		if($query->num_rows() > 0)
		{

			return $result->total_credit;
		}
		else
		{
			return 0;
		}
		
	}

	public function get_related_posts($blog_category_id)
	{
		$table = "post";
		$where = "post_status = 1 AND blog_category_id = ".$blog_category_id; 
		$items = "*";
		$order = "post_id";
			//echo $sql;
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}

	function chop_string($str, $len) {
	    if (strlen($str) < $len)
	        return $str;

	    $str = substr($str,0,$len);
	    if ($spc_pos = strrpos($str," "))
	            $str = substr($str,0,$spc_pos);

	    return $str;
	} 
	public function get_total_numbers($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}
	public function get_departments()
	{
  		$table = "department";
		$where = "department_status = 1";
		
		$this->db->where($where);
		$this->db->order_by('department_name');
		$query = $this->db->get($table);
		
		return $query;
	}

	public function get_services1()
	{
  		$table = "service";
		$where = "service_status = 1";
		
		$this->db->where($where);
		$this->db->order_by('service_name');
		$query = $this->db->get($table);
		
		return $query;
	}
	public function get_gallery_departments()
	{
  		$table = "department, gallery";
		$where = "department.department_status = 1 AND gallery.department_id = department.department_id";
		
		$this->db->where($where);
		$this->db->group_by('department_name');
		$this->db->order_by('department_name');
		$query = $this->db->get($table);
		
		return $query;
	}
	
	public function get_all_gallerys($table, $where)
	{
		//retrieve all gallerys
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('department.department_name');
		$query = $this->db->get();
		
		return $query;
	}

	public function get_blog_category_id($blog_category_name)
	{
		$where = 'blog_category.blog_category_name = "'.$blog_category_name.'" '; 
		$table = 'blog_category';
		
		$this->db->where($where);
		$this->db->limit(1);
		$query = $this->db->get($table);
		$result = $query->row();

		if($query->num_rows() > 0)
		{

			return $result->blog_category_id;
		}
		else
		{
			return 0;
		}
		return $query;
	}
}

?>