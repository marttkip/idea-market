<?php
        $specialist_query = $this->site_model->get_active_items('Our Partners');
          $partners_item = '';

          // var_dump($specialist_query->num_rows());die();
          if($specialist_query->num_rows() > 0)
          {
            $x=0;
            foreach($specialist_query->result() as $row)
            {
              $about_title = $row->post_title;
              $post_id = $row->post_id;
              $blog_category_name = $row->blog_category_name;
              $blog_category_id = $row->blog_category_id;
              $post_title = $row->post_title;
              $web_name = $this->site_model->create_web_name($post_title);
              $post_status = $row->post_status;
              $post_views = $row->post_views;
              $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
              $created_by = $row->created_by;
              $modified_by = $row->modified_by;
              $post_target = $row->post_target;
              $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
              $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
              $description = $row->post_content;
              $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
              $created = $row->created;
              $day = date('j',strtotime($created));
              $month = date('M',strtotime($created));
              $year = date('Y',strtotime($created));
              $created_on = date('jS M Y',strtotime($row->created));
              $x++;
              if($x < 9)
              {
                $x = '0'.$x;
              }
              $partners_item .= '<div class="item">
                            <img src="'.$image_specialist.'" class="img-responsive" alt="story_img" />
                        </div>';
        }
       }

       // var_dump($partners_item);die();
     ?>


<div class="partner_wrapper med_bottompadder80 med_toppadder80">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                    <div class="partner_slider_img">
                        <div class="owl-carousel owl-theme">
                          <?php echo $partners_item?>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
