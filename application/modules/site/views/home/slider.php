
		
<div class="banner-area">
        <div id="bootcarousel" class="carousel text-light top-pad text-dark slide animate_text" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner carousel-zoom">
                <?php
                    if($slides->num_rows() > 0)
                    {
                      $slides_no = $slides->num_rows();
                      // var_dump($slides_no);die();
                      foreach($slides->result() as $cat => $value)
                      {   

                        $slideshow_id = $value->slideshow_id;
                        $slideshow_status = $value->slideshow_status;
                        $slideshow_name = $value->slideshow_name;
                        $slideshow_description = $value->slideshow_description;
                        $slideshow_image_name = $value->slideshow_image_name;
                        $slideshow_link = $value->slideshow_link;
                        $slideshow_button_text = $value->slideshow_button_text;
                        $subtitle = $value->subtitle;
                        $slideshow_thumb_name = 'thumbnail_'.$value->slideshow_image_name;
                     

                        // $med = explode(' ', $slideshow_name);

                        // $first_name = $med[0];
                        // $second_name = $med[1];


                        if(!empty($slideshow_link))
                        {
                            $buttons = ' <ul>
                                            <li data-animation-in="bounceInLeft" data-animation-out="animate-out bounceOutLeft"><a href="'.$slideshow_button_link.'">'.$slideshow_button_text.'</a></li>
                                        </ul>';
                        }
                        else
                        {
                            $buttons = '';
                        }


                        ?>
                            
                            <div class="item active bg-cover" style="background:url('<?php echo $slideshow_location.$slideshow_image_name;?>">
                                <div class="box-table">
                                    <div class="box-cell shadow dark">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="content">
                                                        <h2 data-animation="animated slideInDown"><?php echo $slideshow_name?></h2>
                                                        <p data-animation="animated slideInLeft">
                                                           <?php echo $slideshow_description;?>
                                                        </p>
                                                        <a data-animation="animated slideInUp" class="btn btn-theme effect btn-md" href="#">View Courses</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                    <?php
                        }
                    }
                    ?>
            </div>
            <!-- End Wrapper for slides -->

            <!-- Left and right controls -->
            <a class="left carousel-control shadow" href="#bootcarousel" data-slide="prev">
                <i class="fa fa-angle-left"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control shadow" href="#bootcarousel" data-slide="next">
                <i class="fa fa-angle-right"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>