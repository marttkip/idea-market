<?php
    $gallery = $this->site_model->get_active_service_gallery_names();

  
    $gallery_items_names = ' <li><a class="selected" href="#filter=*">All Departments</a></li>';
    if($gallery->num_rows() > 0)
    {   
        foreach($gallery->result() as $res_gallery)
        {
            $gallery_name = $res_gallery->gallery_name;
                    

                   
            $gallery_items_names .='<li><a href="#filter=.'.$gallery_name.'" title="'.$gallery_name.'">'.$gallery_name.'</a></li>';
        }
    }

    $gallery_div = $this->site_model->get_active_gallery();
    $gallery_items = '';
    if($gallery_div->num_rows() > 0)
    {   
        foreach($gallery_div->result() as $res_gallery_div)
        {
            $gallery_name = $res_gallery_div->gallery_name;
            $gallery_image_name = $res_gallery_div->gallery_image_name;
            $gallery_image_thumb = $res_gallery_div->gallery_image_thumb;

             $gallery_items .=
                                    '
                                    <div class="col-xs-6 col-md-3">
                                      <div class="box-gallery">
                                        <a href="'.$gallery_location.''.$gallery_image_name.'" title="Gallery #2">
                                          <img src="'.$gallery_location.''.$gallery_image_name.'" alt="" class="img-fluid">
                                          <div class="project-info">
                                            <div class="project-icon">
                                              <span class="fa fa-search"></span>
                                            </div>
                                          </div>
                                        </a>
                                      </div>
                                    </div>
                                    
                                   
                                    ';      
        }
    }
?>




<!-- BANNER -->
  <div class="section banner-page" data-background="<?php echo base_url().'assets/themes/ngoo/'?>images/banner-single.jpg">
    <div class="content-wrap pos-relative">
      <div class="d-flex justify-content-center bd-highlight mb-3">
        <div class="title-page">Gallery</div>
      </div>
      <div class="d-flex justify-content-center bd-highlight mb-3">
          <nav aria-label="breadcrumb">
          <ol class="breadcrumb ">
            <li class="breadcrumb-item"><a href="<?php echo site_url().'home'?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Gallery</li>
          </ol>
        </nav>
        </div>
    </div>
  </div>
  

  <!-- OUR GALLERY -->
  <div class="section">
    <div class="content-wrap">
      <div class="container">

        <div class="row">
          <div class="col-sm-12 col-md-12">
            <div class="row popup-gallery gutter-5">
                <?php echo $gallery_items?>              
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>