<?php
$company_details = $company_details;



?>

<div class="page relative">
    <div class="page_layout page_margin_top clearfix">
      <div class="page_header clearfix">
        <div class="page_header_left">
          <!-- <h1 class="page_title"><?php echo $title?></h1> -->
          <ul class="bread_crumb">
            <li>
              <a href="<?php echo site_url().'home'?>" title="Home">
                Home
              </a>
            </li>
             <li class="separator icon_small_arrow right_gray">
              &nbsp;
            </li>
            <li>
              <a href="<?php echo site_url().'about-us'?>"  title="About Us">
                About Us
              </a>
            </li>
            <li class="separator icon_small_arrow right_gray">
              &nbsp;
            </li>
            <li>
              <?php echo $title?>
            </li>
          </ul>
        </div>
        <div class="page_header_right">
          
        </div>
      </div>
      <div class="clearfix">

        <?php
        $about_query = $this->site_model->get_active_content_items('Policy and Governance');
         $about_company = '';
        if($about_query->num_rows() == 1)
        {
          $x=0;
          foreach($about_query->result() as $row)
          {
            $about_title = $row->post_title;
            $post_id = $row->post_id;
            $blog_category_name = $row->blog_category_name;
            $blog_category_id = $row->blog_category_id;
            $post_title = $row->post_title;
            $web_name = $this->site_model->create_web_name($post_title);
            $post_status = $row->post_status;
            $post_views = $row->post_views;
            $image_about = base_url().'assets/images/posts/'.$row->post_image;
            $created_by = $row->created_by;
            $modified_by = $row->modified_by;
            $post_target = $row->post_target;
            $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
            $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
            $description = $row->post_content;
            $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
            $created = $row->created;
            $day = date('j',strtotime($created));
            $month = date('M',strtotime($created));
            $year = date('Y',strtotime($created));
            $created_on = date('jS M Y',strtotime($row->created));
            $x++;
            if($x < 9)
            {
              $x = '0'.$x;
            }
            $about_company = '<div class="row">
                                <div class="col-md-12">
                                   <h2 class="heading">Welcome to '.$post_title.'</h2>
                                   <hr class="heading_space">
                                </div>
                                <div class="col-md-7 col-sm-6">
                                  <p class="half_space">'.$description.'.</p>
                                 
                                </div>
                                <div class="col-md-5 col-sm-6">
                                 <img class="img-responsive" src="'.$image_about.'" alt="welcome '.$post_title.'">
                                </div>
                              </div>';
          }
        }
        ?>
        <div class="gallery_item_details_list clearfix page_margin_top">
          <div class="gallery_item_details clearfix">
            <div class="columns no_width">
              <div class="column_left" style="width:50%">
              	<div id="image_box" style="margin-top: 40px;">
              		<img src="<?php echo $image_about?>" width="100%">
              	</div>
                	
              </div>
              <div class="column_right">
                <div class="details_box">
                  <h2>
                    <?php echo $post_title?>
                  </h2>
                  
                  <span style="text-align: justify;"><?php echo $description?></span>
                 
                  
                  
                </div>
              </div>
            </div>
          </div>
        </div>
       
      </div>
    </div>
    <?php

  echo $this->load->view("site/testimonials", '');
  ?>
  </div>
