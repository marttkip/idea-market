<?php
$company_details = $company_details;

$mission = $company_details['mission'];
$vision = $company_details['vision'];
$core_values = $company_details['core_values'];



?>

<?php
  $about_query = $this->site_model->get_active_content_items('Our Company',1);
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $company_details_description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $company_details_description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $gallery_rs = $this->site_model->get_post_gallery($post_id);

      $gallery = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
          $post_gallery_image_thumb = $value->post_gallery_image_thumb;

          $gallery .= ' <li>
                              <div class="portfolio-thumb">
                                  <div class="gc_filter_cont_overlay_wrapper port_uper_div">
                                      <img src="'.$post_gallery_image_name.'" class="zoom image img-responsive" alt="service_img" />
                                      <div class="gc_filter_cont_overlay zoom_popup">
                                          <div class="gc_filter_text"><a href="'.$post_gallery_image_name.'"><i class="fa fa-plus"></i></a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </li>';
        }
      }

    }
  }
  ?>


  <?php
  $testimonials_query = $this->site_model->get_active_items('Company Testimonials');
   $testimonials = '';
  if($testimonials_query->num_rows() == 1)
  {
    $x=0;
    foreach($testimonials_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $testimonials_image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;

      $testimonials .= '<div class="col-sm-6 col-md-6">
                          <div class="testimonial-1">
                                    <div class="media">
                                      <img src="'.$testimonials_image_about.'" alt="" class="img-fluid">
                                    </div>
                                    <div class="body">
                                      <p>'.$description.'. </p>
                                      <div class="title">'.$post_title.'</div>
                                      <div class="company">'.$post_target.'</div>
                                    </div>
                                </div>
                        </div>';
    }
  }
  ?>
 
  
     <!-- Start Breadcrumb 
    ============================================= -->
    <div class="breadcrumb-area shadow dark bg-fixed text-center text-light" style="background-image: url(<?php echo base_url().'assets/themes/educom/'?>assets/img/banner/7.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>About Us</h1>
                    <ul class="breadcrumb">
                        <li><a href="<?php echo site_url().'home'?>"><i class="fa fa-home"></i> Home</a></li>
                        <!-- <li><a href="#">Pages</a></li> -->
                        <li class="active">About</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Start About 
    ============================================= -->
    <div class="about-area default-padding">
        <div class="container">
            <div class="row">
                <div class="about-items">
                    <div class="col-md-6 about-info">
                        <h2>Welcome ! <span>We're are <?php echo $company_details['company_name']?></span></h2>
                        <blockquote>
                            <?php echo $company_details_description;?>
                        </blockquote>
                        <div class="semester-apply">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 item">
                                    <h4>Mission</h4>
                                    <p>
                                      <?php echo $mission?>
                                    </p>
                                    <!-- <a class="btn btn-dark effect btn-sm" href="#"><i class="fas fa-angle-right"></i> Apply Now</a> -->
                                </div>
                                <div class="col-md-6 col-sm-6 item">
                                    <h4>Vision</h4>
                                    <p>
                                        <?php echo $vision?>
                                    </p>
                                    <!-- <a class="btn btn-dark effect btn-sm" href="#"><i class="fas fa-angle-right"></i> Apply Now</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 features text-light">
                        <div class="row">
                            <div class="col-md-12">
                              <img src="<?php echo $image_about?>">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End About -->

    <!-- Start Campus Story 
    ============================================= -->
  
    <!-- End Campus Story -->

    <!-- Start Advisor 
    ============================================= -->
     <?php echo $this->load->view("site/team_list", '');?> 
    <!-- End Advisor -->

    <!-- Start Portfolio
    ============================================= -->
  
    <!-- End Portfolio -->