<?php
$company_details = $company_details;



?>

<?php
  $about_query = $this->site_model->get_active_content_items('Our Company',1);
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }
    }
  }
  ?>
<!-- BANNER -->
  <div class="section banner-page" data-background="<?php echo base_url().'assets/themes/ngoo/'?>images/banner-single.jpg">
    <div class="content-wrap pos-relative">
      <div class="d-flex justify-content-center bd-highlight mb-3">
        <div class="title-page">About Us</div>
      </div>
      <div class="d-flex justify-content-center bd-highlight mb-3">
          <nav aria-label="breadcrumb">
          <ol class="breadcrumb ">
            <li class="breadcrumb-item"><a href="<?php echo site_url().'home'?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">About Us</li>
          </ol>
        </nav>
        </div>
    </div>
  </div>

  <!-- CONTENT -->
  <div class="section">
    <div class="content-wrap">
      <div class="container">
        <div class="row">

          <div class="col-sm-7 col-md-7">
            <h6 class="section-heading">
              Welcome <span>To</span> <?php echo ucwords(strtolower($post_title));?>
            </h6>



            <p><?php echo $description;?>.</p>
            <!-- <div class="spacer-30"></div> -->
            <!-- <a href="about.html#" class="btn btn-primary">MEET OUR TEAMS</a> -->
            <!-- <div class="spacer-30"></div> -->

          </div>

          <div class="col-sm-5 col-md-5">
            
            <img src="<?php echo $image_about?>" alt="" class="img-fluid img-border">

          </div>
          

        </div>

        <div class="spacer-70"></div>

        <div class="row">

          <div class="col-sm-3 col-md-3">
            <div class="rs-icon-funfact style-2">
              <div class="icon">
                <i class="fa fa-file-text-o"></i>
              </div>
              <div class="body-content">
                <h2>12,280</h2>
                <p class="uk18">Complete</p>
              </div>
            </div>
          </div>

          <div class="col-sm-3 col-md-3">
            <div class="rs-icon-funfact style-2">
              <div class="icon">
                <i class="fa fa-users"></i>
              </div>
              <div class="body-content">
                <h2>1,825</h2>
                <p class="uk18">Our Team</p>
              </div>
            </div>
          </div>

          <div class="col-sm-3 col-md-3">
            <div class="rs-icon-funfact style-2">
              <div class="icon">
                <i class="fa fa-trophy"></i>
              </div>
              <div class="body-content">
                <h2>37</h2>
                <p class="uk18">Awards</p>
              </div>
            </div>
          </div>

          <div class="col-sm-3 col-md-3">
            <div class="rs-icon-funfact style-2">
              <div class="icon">
                <i class="fa fa-male"></i>
              </div>
              <div class="body-content">
                <h2>256,861</h2>
                <p class="uk18">Volunteer</p>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
   

    <?php
  $testimonials_query = $this->site_model->get_active_items('Company Testimonials');
   $testimonials = '';
  if($testimonials_query->num_rows() == 1)
  {
    $x=0;
    foreach($testimonials_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;

      $testimonials .= '<div class="col-sm-6 col-md-6">
                          <div class="testimonial-1">
                                    <div class="media">
                                      <img src="'.$image_about.'" alt="" class="img-fluid">
                                    </div>
                                    <div class="body">
                                      <p>'.$description.'. </p>
                                      <div class="title">'.$post_title.'</div>
                                      <div class="company">'.$post_target.'</div>
                                    </div>
                                </div>
                        </div>';
    }
  }
  ?>
 
  
  <!-- OUR VOLUUNTER SAYS -->
  <div class="section" data-background="<?php echo base_url().'assets/themes/ngoo/'?>images/bg-cause.png">
    <div class="content-wrap">
      <div class="container">
        <div class="row">

          <div class="col-sm-12 col-md-12">
            <h2 class="section-heading center">
              <span>Testimonials</span>
            </h2>
            <p class="subheading text-center">We are encouraged and always honoured to be part of a team that brings change to the lives of people.</p>
          </div>
            <?php echo $testimonials;?>
        </div>
      </div>
    </div>
  </div>
  <div class="section">
    <div class="content-wrap">
      <div class="container">
        <div class="row">

          <div class="col-sm-12 col-md-12">
            <h2 class="section-heading" id="team">
              Meet <span>Our</span> Team
            </h2>
          </div>


          <?php echo $this->load->view("site/our_team", '');?>
          

          

          <!-- <div class="col-sm-3 col-md-3">
            <div class="promo-ads" data-background="images/banner-ads.jpg">
              <div class="content font__color-white">
                <i class="fa fa-bullhorn"></i>
                <h4 class="title">Become a Volunteer</h4>
                <p class="uk16">We need you now for world</p>
                <p class="font__color-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur pellentesque neque eget diam.</p>
                <div class="spacer-30"></div>
                <a href="index-2.html#" class="btn btn-secondary">JOIN US NOW</a>
              </div>
            </div>
          </div> -->


        </div>
      </div>
    </div>
  </div>


  <!-- OUR PARTNERS -->
  <div class="section">
    <div class="content-wrap">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12">
            <h2 class="section-heading center">
              Our <span>Partners</span>
            </h2>
            <!-- <p class="subheading text-center">Lorem ipsum dolor sit amet, onsectetur adipiscing cons ectetur nulla. Sed at ullamcorper risus.</p> -->
          </div>
          
        </div>
        <div class="row gutter-2">
          <?php echo $this->load->view("site/our_partners", '');?>  
        </div>
      </div>
    </div>
  </div>


  <?php echo $this->load->view("site/bottom_flier", '');?>  

