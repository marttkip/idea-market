<?php
  $company_details = $this->site_model->get_contacts();
  
  if(count($company_details) > 0)
  {
    $email = $company_details['email'];
    $email2 = $company_details['email'];
    $facebook = $company_details['facebook'];
    $twitter = $company_details['twitter'];
    $linkedin = '';// $company_details['linkedin'];
    $logo = $company_details['logo'];
    $company_name = $company_details['company_name'];
    $phone = $company_details['phone']; 
    $mission = $company_details['mission'];   
  
  }
  else
  {
    $email = '';
    $facebook = '';
    $twitter = '';
    $linkedin = '';
    $logo = '';
    $company_name = '';
    $google = '';
    $mission = '';
  }


?>



<!-- BANNER -->
  <div id="oc-fullslider" class="banner owl-carousel">
       <?php
    if($slides->num_rows() > 0)
    {
      $slides_no = $slides->num_rows();
      // var_dump($slides_no);die();
      foreach($slides->result() as $cat => $value)
      {   

        $slideshow_id = $value->slideshow_id;
        $slideshow_status = $value->slideshow_status;
        $slideshow_name = $value->slideshow_name;
        $slideshow_description = $value->slideshow_description;
        $slideshow_image_name = $value->slideshow_image_name;
        $slideshow_thumb_name = 'thumbnail_'.$value->slideshow_image_name;
      ?>
      <div class="owl-slide">
            <div class="item">
                <img src="<?php echo $slideshow_location.$slideshow_image_name;?>" alt="Slider">
                <div class="slider-pos">
                  <div class="container">
                    <div class="wrap-caption center">
                        <h1 class="caption-heading">#<?php echo $slideshow_name?></h1>
                        <p><?php echo $slideshow_description?></p>
                        <a href="#" class="btn btn-light">LEARN MORE</a>
                        <a href="#" class="btn btn-primary">DONATE NOW</a>
                    </div>  
                  </div>  
                </div>  
              </div>  
          </div>
      <!-- <li style="background-image: url('<?php echo $slideshow_location.$slideshow_image_name;?>');">
        &nbsp;
      </li> -->
      <?php
      }
    }
    ?>
    </div>

  <div class="clearfix"></div>
  <?php
      $note_query = $this->site_model->get_active_content_items('Welcome Note');
    $welcome_note = '';
    if($note_query->num_rows() > 0)
    {
      foreach($note_query->result() as $row)
      {                
        $welcome_note = $row->post_content;
      }
    }
  ?>
   
  <!-- WELCOME TO NGOO-->
  <div class="section services">
    <div class="content-wrap">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12">
            <h2 class="section-heading center">
              Welcome <span>To</span> <?php echo ucwords(strtolower($company_name));?>
            </h2>
            <p class="subheading text-center"><?php echo strip_tags($welcome_note);?></p>
          </div>
          <div class="clearfix"></div>

          <?php
            $service_query = $this->site_model->get_active_items('Key Pillars',3);

            // var_dump($service_query);die();
              $service_item = '';
              if($service_query->num_rows() > 0)
              {
                $x=0;
                    foreach($service_query->result() as $row)
                    {
                      $about_title = $row->post_title;
                      $post_id = $row->post_id;
                      $blog_category_name = $row->blog_category_name;
                      $blog_category_id = $row->blog_category_id;
                      $post_title = $row->post_title;
                      $post_target = $row->post_target;
                      $web_name = $this->site_model->create_web_name($post_title);
                      $post_status = $row->post_status;
                      $post_views = $row->post_views;
                      $image_service = base_url().'assets/images/posts/'.$row->post_image;
                      $created_by = $row->created_by;
                      $modified_by = $row->modified_by;
                      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
                      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
                      $description = $row->post_content;
                      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
                      $created = $row->created;
                      $day = date('j',strtotime($created));
                      $month = date('M',strtotime($created));
                      $year = date('Y',strtotime($created));
                      $created_on = date('jS M Y',strtotime($row->created));
            
                        $x++;

                        if($x<10)
                        {
                          $number = '0'.$x;
                        }
                        else
                        {
                          $number = $x;
                        }
                        ?>
                        <div class="col-sm-4 col-md-4">
                        <div class="icon-with-text">
                            <div class="icon">
                              <i class="fa fa-male"></i>
                            </div>
                            <div class="no"><?php echo $number;?></div>
                            <p class="subtitle"><?php echo $post_target?></p>
                            <h3 class="title"><?php echo $post_title?></h3>
                            <div class="text"><?php echo $mini_desc?></div>
                        </div>
                    </div>

                       

                        
                    <?php
                  }
                }
              ?>
          

          <!-- <div class="col-sm-12 col-md-12">
            <div class="spacer-50"></div>
            <div class="text-center">
              <a href="<?php echo site_url().'causes'?>" class="btn btn-primary">SEE ALL CAUSE</a>
            </div>

          </div>
 -->
        </div>
      </div>
    </div>
  </div>


   <?php
  $be_part_query = $this->site_model->get_active_content_items('Cause Statement',1);
   $cause_statement = '';
  if($be_part_query->num_rows() == 1)
  {
    $x=0;
    foreach($be_part_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $cause_statement = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
    }
  }
  ?>
  <div class="section cta" data-background="<?php echo base_url().'assets/themes/ngoo/'?>images/bg-map-dot.jpg">
    <div class="content-wrap">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12">
            <div class="cta-info">
            <h1 class="section-heading light no-after">
              <span>Be part of us </span>
            </h1>
            <h3 class="color-primary">Crowdsourced Scholarships / Grants</h3>

            <div class="spacer-10"></div>
            <p><?php echo $cause_statement;?></p>

            <a href="#" class="btn btn-light margin-btn">LEARN MORE</a> <a href="#" class="btn btn-primary margin-btn">CONTRIBUTE NOW</a>  
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>

  <?php
      $statement_query = $this->site_model->get_active_content_items('Cause Statement',1);

      // var_dump($statement_query);die();
        $cause_statement = '[cause statement]';
        $post_video = 'http://www.youtube.com/embed/vNDrLjOmUY4?autoplay=0&loop=0&showinfo=0&theme=dark&color=red&controls=1&modestbranding=0&start=0&fs=1&iv_load_policy=1';
        if($statement_query->num_rows() > 0)
        {
        $x=0;
            foreach($statement_query->result() as $row)
            {
              $about_title = $row->post_title;
              $post_id = $row->post_id;
              $blog_category_name = $row->blog_category_name;
              $blog_category_id = $row->blog_category_id;
              $post_title = $row->post_title;
              $post_target = $row->post_target;
              $post_video = $row->post_video;
              $web_name = $this->site_model->create_web_name($post_title);
              $post_status = $row->post_status;
              $post_views = $row->post_views;
              $image_service = base_url().'assets/images/posts/'.$row->post_image;
              $created_by = $row->created_by;
              $modified_by = $row->modified_by;
              $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
              $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
              $cause_statement = $row->post_content;
              $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
              $created = $row->created;
              $day = date('j',strtotime($created));
              $month = date('M',strtotime($created));
              $year = date('Y',strtotime($created));
              $created_on = date('jS M Y',strtotime($row->created));
    
                $x++;

                if($x<10)
                {
                  $number = '0'.$x;
                }
                else
                {
                  $number = $x;
                }
              
        }
      }
    ?>

  <!-- WHY CHOOSE US -->
  <div class="section" data-background="<?php echo base_url().'assets/themes/ngoo/'?>images/bg-cause.png">
    <div class="content-wrap">
      <div class="container">
        <div class="row">

          <div class="col-sm-12 col-md-12">
            <h2 class="section-heading">
              Our <span>cause</span>
            </h2>
          </div>

          <div class="col-sm-6 col-md-6">
            <p><?php echo $cause_statement;?> .</p>
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="<?php echo $post_video?>"></iframe>
              
            </div>
            <div class="spacer-30"></div>

          </div>

          <div class="col-sm-6 col-md-6">
            
            <div class="accordion rs-accordion" id="accordionExample">

              <?php
                $statement_query = $this->site_model->get_active_items('Cause Objectives',3);

                // var_dump($statement_query);die();
                  $cause_statement = '';
                  if($statement_query->num_rows() > 0)
                  {
                  $x=0;
                      foreach($statement_query->result() as $row)
                      {
                        $about_title = $row->post_title;
                        $post_id = $row->post_id;
                        $blog_category_name = $row->blog_category_name;
                        $blog_category_id = $row->blog_category_id;
                        $post_title = $row->post_title;
                        $post_target = $row->post_target;
                        $web_name = $this->site_model->create_web_name($post_title);
                        $post_status = $row->post_status;
                        $post_views = $row->post_views;
                        $image_service = base_url().'assets/images/posts/'.$row->post_image;
                        $created_by = $row->created_by;
                        $modified_by = $row->modified_by;
                        $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
                        $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
                        $cause_statement = $row->post_content;
                        $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
                        $created = $row->created;
                        $day = date('j',strtotime($created));
                        $month = date('M',strtotime($created));
                        $year = date('Y',strtotime($created));
                        $created_on = date('jS M Y',strtotime($row->created));
              
                          $x++;

                          if($x== 1)
                          {
                            $expanded = 'true';
                          }
                          else
                          {
                            $expanded = 'false';
                          }

                          
                          ?>
                          <div class="card mb-2">
                      <div class="card-header" id="headingOne">
                        <h4 class="title">
                          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $x;?>" aria-expanded="<?php echo $expanded;?>" aria-controls="collapse<?php echo $x;?>">
                            <?php echo $post_title;?>
                          </button>
                        </h4>
                      </div>
                      <div id="collapse<?php echo $x;?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                         <?php echo $description;?>
                        </div>
                      </div>
                    </div>
                          <?php
                        
                  }
                }
              ?>
              


            </div>


          </div>

        </div>
      </div>
    </div>
  </div>

  <!-- FUN FACT -->
  <div class="section cta" data-background="<?php echo base_url().'assets/themes/ngoo/'?>images/bg-map-dot.jpg">
    <div class="content-wrap content-wrap-2x">
      <div class="container">
        <div class="row">

          <div class="col-sm-3 col-md-3">
            <div class="rs-icon-funfact">
              <div class="icon">
                <i class="fa fa-file-text-o"></i>
              </div>
              <div class="body-content">
                <h2>12,280</h2>
                <p class="uk18">Complete</p>
              </div>
            </div>
          </div>

          <div class="col-sm-3 col-md-3">
            <div class="rs-icon-funfact">
              <div class="icon">
                <i class="fa fa-users"></i>
              </div>
              <div class="body-content">
                <h2>1,825</h2>
                <p class="uk18">Our Team</p>
              </div>
            </div>
          </div>

          <div class="col-sm-3 col-md-3">
            <div class="rs-icon-funfact">
              <div class="icon">
                <i class="fa fa-trophy"></i>
              </div>
              <div class="body-content">
                <h2>37</h2>
                <p class="uk18">Awards</p>
              </div>
            </div>
          </div>

          <div class="col-sm-3 col-md-3">
            <div class="rs-icon-funfact">
              <div class="icon">
                <i class="fa fa-male"></i>
              </div>
              <div class="body-content">
                <h2>256,861</h2>
                <p class="uk18">Volunteer</p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <?php
      $joining_query = $this->site_model->get_active_items('Joining Us',3);

      // var_dump($joining_query);die();
      $joining_image = ''.base_url().'assets/themes/ngoo/images/help-people.jpg';
        $joining_us = '';
        if($joining_query->num_rows() > 0)
        {
          $x=0;
              foreach($joining_query->result() as $row)
              {
                $about_title = $row->post_title;
                $post_id = $row->post_id;
                $blog_category_name = $row->blog_category_name;
                $blog_category_id = $row->blog_category_id;
                $post_title = $row->post_title;
                $post_target = $row->post_target;
                $web_name = $this->site_model->create_web_name($post_title);
                $post_status = $row->post_status;
                $post_views = $row->post_views;
                $joining_image = base_url().'assets/images/posts/'.$row->post_image;
                $created_by = $row->created_by;
                $modified_by = $row->modified_by;
                $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
                $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
                $description = $row->post_content;
                $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
                $created = $row->created;
                $day = date('j',strtotime($created));
                $month = date('M',strtotime($created));
                $year = date('Y',strtotime($created));
                $created_on = date('jS M Y',strtotime($row->created));
      
                  $x++;

                  if($x<10)
                  {
                    $number = '0'.$x;
                  }
                  else
                  {
                    $number = $x;
                  }
                  $joining_us .='<dt><span class="fa fa-gift"></span></dt>
              <dd><div class="no">'.$number.'</div><h3>'.$post_title.'</h3>'.$description.'.</dd>';
            }
          }
        ?>
  <div class="content-wrap-top">
      <div class="container">
        <div class="row align-items-end">
          <div class="col-sm-6 col-md-6">
            <img src="<?php echo $joining_image?>" alt="" class="img-fluid">
          </div>

          <div class="col-sm-6 col-md-6">
            <h2 class="section-heading">
              How To <span>Join</span> Us
            </h2>
            <div class="section-subheading">To join us and be part of our .</div> 
            <div class="margin-bottom-10"></div>
            <dl class="hiw">
              
              <?php echo $joining_us;?>
              
            </dl>
            <div class="spacer-70"></div>
          </div>

        </div>
      </div>
    </div>

  <!-- MEET UR VOLUUNTER -->
  <div class="section">
    <div class="content-wrap">
      <div class="container">
        <div class="row">

          <div class="col-sm-12 col-md-12">
            <h2 class="section-heading">
              Meet <span>Our</span> Team
            </h2>
          </div>


          <?php echo $this->load->view("site/our_team", '');?>


        </div>
      </div>
    </div>
  </div>


  <!-- OUR PARTNERS -->
  <div class="section">
    <div class="content-wrap">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12">
            <h2 class="section-heading center">
              Our <span>Partners</span>
            </h2>
            <!-- <p class="subheading text-center">Lorem ipsum dolor sit amet, onsectetur adipiscing cons ectetur nulla. Sed at ullamcorper risus.</p> -->
          </div>
          
        </div>
        <div class="row gutter-2">
          <?php echo $this->load->view("site/our_partners", '');?>  
        </div>
      </div>
    </div>
  </div>


  <?php echo $this->load->view("site/bottom_flier", '');?>  