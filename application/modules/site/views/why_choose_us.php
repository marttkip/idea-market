<?php
        $specialist_query = $this->site_model->get_active_items('Why Choose Us');
          $main_services = '';
          if($specialist_query->num_rows() > 0)
          {
            $x=0;
            foreach($specialist_query->result() as $row)
            {
              $about_title = $row->post_title;
              $post_id = $row->post_id;
              $blog_category_name = $row->blog_category_name;
              $blog_category_id = $row->blog_category_id;
              $post_title = $row->post_title;
              $web_name = $this->site_model->create_web_name($post_title);
              $post_status = $row->post_status;
              $post_views = $row->post_views;
              $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
              $created_by = $row->created_by;
              $modified_by = $row->modified_by;
              $post_target = $row->post_target;
              $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
              $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
              $description = $row->post_content;
              $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
              $created = $row->created;
              $day = date('j',strtotime($created));
              $month = date('M',strtotime($created));
              $year = date('Y',strtotime($created));
              $created_on = date('jS M Y',strtotime($row->created));
              $x++;
              if($x < 9)
              {
                $x = '0'.$x;
              }

              if($x== 1)
              {
              	$expandable="true";
                $show = 'show';
              }
              else
              {
              	$expandable = 'false';
                 $show = '';
              }
              $main_services .= '
              						<div class="panel panel-default sidebar_pannel">
	                                    <div class="panel-heading desktop">
	                                        <h4 class="panel-title">
	                                            <a data-toggle="collapse" data-parent="#accordionFifteenLeft" href="#collapseFifteenLeftone'.$post_id.'" aria-expanded="false">'.$about_title.'</a>
	                                        </h4>
	                                    </div>
	                                    <div id="collapseFifteenLeftone'.$post_id.'" class="panel-collapse collapse '.$show.'" aria-expanded="'.$expandable.'" role="tabpanel">
	                                        <div class="panel-body">
	                                            <div class="panel_cont">
	                                                <p>'.$mini_desc.'</p>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>';
        }
       }
        // $main_services = '</ul>';
     ?>
<?php echo $main_services?>