  <?php
$about_query = $this->site_model->get_active_items('Company Courses',10);
 $company_services = '';

 // var_dump($about_query);die();
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $x++;
    if($x < 9)
    {
      $x = '0'.$x;
    }
    $company_services .= '
                          <div class="col-md-4 col-sm-6 equal-height">
                            <div class="item">
                                <div class="thumb">
                                    <img src="'.$image_about.'" alt="Thumb">
                                    <div class="overlay">
                                        <a href="#">
                                            <img src="'.$image_about.'" alt="Thumb">
                                        </a>
                                    </div>
                                </div>
                                <div class="info">
                                    <div class="meta">
                                        <ul>
                                            <li>
                                                <a href="#">'.$blog_category_name.'</a>
                                            </li>
                                            
                                        </ul>
                                    </div>
                                    <h4>
                                        <a href="#">'.$post_title.'</a>
                                    </h4>
                                    <p>
                                       '.$mini_desc.'
                                    </p>
                                    <div class="footer-meta">
                                        <a class="btn btn-theme effect btn-sm" href="href="'.site_url().'our-courses/'.$web_name.'"">View More</a>
                                        <h4>'.$post_target.'</h4>
                                    </div>
                                </div>
                            </div>
                        </div>';
  }
}
?>
 
  <div class="popular-courses-area weekly-top-items bg-gray default-padding bottom-less">
        <div class="container">
            <div class="row">
                <div class="site-heading text-center">
                    <div class="col-md-8 col-md-offset-2">
                        <h2>Popular Courses</h2>
                     <!--    <p>
                            Discourse assurance estimable applauded to so. Him everything melancholy uncommonly but solicitude inhabiting projection off. Connection stimulated estimating excellence an to impression. 
                        </p> -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="top-course-items">
                      <?php echo $company_services?>
                </div>
            </div>
        </div>
    </div>