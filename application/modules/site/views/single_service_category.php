<?php
  $about_query = $this->site_model->get_active_content_items($title,1);
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      // $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $gallery_rs = $this->site_model->get_post_gallery($post_id);

      $gallery = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
          $post_gallery_image_thumb = $value->post_gallery_image_thumb;

          $gallery .= ' <li>
                              <div class="portfolio-thumb">
                                  <div class="gc_filter_cont_overlay_wrapper port_uper_div">
                                      <img src="'.$post_gallery_image_name.'" class="zoom image img-responsive" alt="service_img" />
                                      <div class="gc_filter_cont_overlay zoom_popup">
                                          <div class="gc_filter_text"><a href="'.$post_gallery_image_name.'"><i class="fa fa-plus"></i></a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </li>';
        }
      }

    }
  }

// var_dump($blog_category_id);die();

  // blog services 




  ?>
  <!--med_tittle_section-->
    <div class="med_tittle_section">
        <div class="med_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="med_tittle_cont_wrapper">
                        <div class="med_tittle_cont">
                            <h1>Our Services</h1>
                            <ol class="breadcrumb">
                                <li><a href="<?php echo site_url().'home'?>">Home</a></li>
                                <li>Services</li>
                                <li><?php echo $title?></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- med_tittle_section End -->
   
    <!--ser_abt section start-->
   
    <!--ser_abt section end-->
     <div class="about_us_section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 about_indx_txt">
                    <div class="ser_abt_img_resp">
                        <img src="<?php echo $image_about?>" alt="img" class="img-responsive" width="100%" height="250">
                    </div>

                </div>
                <div class="col-md-8">
                    <div class="abt_heading_wrapper abt_2_heading">
                        <h1 class="med_bottompadder20"><?php echo $post_title;?></h1>
                        <img src="<?php echo base_url().'assets/themes/deepmind/'?>images/line.png" alt="title" class="med_bottompadder20">
                    </div>
                    <div class="abt_txt">
                        <!-- <h3>Specialty Medicine with Compassion and Car Proin gravida nibh vel nd Car Proin velit.</h3> -->
                       <p class="med_toppadder0"><?php echo $description;?>.</p>
                    </div>
                    
                   
                </div>

            </div>
        </div>
    </div>

    <?php
// var_dump($blog_category_id);die();
  $blog_services_rs = $this->site_model->get_active_post_content_by_category($blog_category_id);

  $services_result = '';
  if($blog_services_rs->num_rows() > 0)
  {
    foreach ($blog_services_rs->result() as $key => $value) {
      # code...

       $about_title = $value->post_title;
      $post_id = $value->post_id;
      $blog_category_name = $value->blog_category_name;
      $blog_category_id = $value->blog_category_id;
      $post_title = $value->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $value->post_status;
      $post_views = $value->post_views;
      $image_about = base_url().'assets/images/posts/'.$value->post_image;
      $created_by = $value->created_by;
      $modified_by = $value->modified_by;
      $post_target = $value->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = strip_tags($value->post_content);
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $value->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($value->created));

      $services_result .= '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                            <div class="blog_about blog_resp wow SlideInLeft" data-wow-delay="0.4s">
                                <div class="blog_img blog_img_resp">
                                    <figure>
                                        <img src="'.$image_about.'" alt="img" class="img-responsive" style="max-height:250px !important;">
                                    </figure>
                                </div>
                                <div class="blog_txt">
                                    <h1><a href="'.site_url().'view-service/'.$web_name.'">'.$post_title.'</a></h1>
                                    <p>'.$mini_desc.'</p>
                                    <a href="'.site_url().'view-service/'.$web_name.'"> Read More <i class="fa fa-long-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>';
    }
  }

    ?>
    <!--ser_blog section start-->
    <div class="blog_wrapper med_toppadder10 med_bottompadder10">
      <div class="top_serv_overlay">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 col-lg-offset-2">
                    <div class="team_heading_wrapper med_bottompadder50 wow fadeInDown" data-wow-delay="0.3s">
                        <h1 class="med_bottompadder20">service procedures</h1>
                        <img src="<?php echo base_url().'assets/themes/deepmind/'?>images/Icon_team.png" alt="line" class="med_bottompadder20">
                    <!--     <p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, <br>nec sagittis sem nibh id elit. Duis sed odio sit sagittis sem nibh id elit.</p> -->
                    </div>
                </div>
                <?php echo $services_result;?>
            </div>
        </div>
      </div>
    </div>
    <!-- ser_blog section end-->
    <!--news wrapper start-->
    <div class="newsletter_wrapper med_toppadder80 med_bottompadder70">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                    <div class="newsletter_text wow fadeOut" data-wow-delay="0.5s">
                        <h3>Your First Step Towards Oral Health For Life Starts Here :</h3>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-12 col-sm-6 col-6">
                    <div class="contect_btn_news">
                        <ul>
                            <li><a href="services.html#">Enquiry</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--news wrapper end-->