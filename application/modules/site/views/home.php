<?php
	$company_details = $this->site_model->get_contacts();
	
	if(count($company_details) > 0)
	{
		$email = $company_details['email'];
		$email2 = $company_details['email'];
		$facebook = $company_details['facebook'];
		$twitter = $company_details['twitter'];
		$linkedin = '';// $company_details['linkedin'];
		$logo = $company_details['logo'];
		$company_name = $company_details['company_name'];
		$phone = $company_details['phone'];	
		$mission = $company_details['mission'];	
        $about = $company_details['about']; 	
	
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
		$mission = '';
	}


?>

        
    <!-- Start Banner 
    ============================================= -->
     <?php echo $this->load->view("site/home/slider", '');?>     
    <
    <!-- End Banner -->

 <!-- Start Features 
    ============================================= -->
    <!-- <div class="features-area default-padding bottom-less">
        <div class="container">
            <div class="row">
                <div class="features">
                    <div class="equal-height col-md-3 col-sm-6">
                        <div class="item mariner">
                            <a href="#">
                                <div class="icon">
                                    <i class="fa fa-panel"></i>
                                </div>
                                <div class="info">
                                    <h4>Expert faculty</h4>
                                    <p>
                                        attention say frankness intention out dashwoods now curiosity. Stronger ecstatic as no judgment daughter.
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="equal-height col-md-3 col-sm-6">
                        <div class="item brilliantrose">
                            <a href="#">
                                <div class="icon">
                                    <i class="fa fa-ruler-pencil"></i>
                                </div>
                                <div class="info">
                                    <h4>Best Teachers</h4>
                                    <p>
                                        attention say frankness intention out dashwoods now curiosity. Stronger ecstatic as no judgment daughter.
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="equal-height col-md-3 col-sm-6">
                        <div class="item casablanca">
                            <a href="#">
                                <div class="icon">
                                    <i class="fa fa-server"></i>
                                </div>
                                <div class="info">
                                    <h4>Online Courses</h4>
                                    <p>
                                        attention say frankness intention out dashwoods now curiosity. Stronger ecstatic as no judgment daughter.
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="equal-height col-md-3 col-sm-6">
                        <div class="item malachite">
                            <a href="#">
                                <div class="icon">
                                    <i class="fa fa-desktop"></i>
                                </div>
                                <div class="info">
                                    <h4>Scholarship</h4>
                                    <p>
                                        attention say frankness intention out dashwoods now curiosity. Stronger ecstatic as no judgment daughter.
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- End Features -->

    <!-- Start About 
    ============================================= -->
    <div class="about-area default-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="about-items">
                    <div class="col-md-6 about-info">
                        <h2>Welcome ! <span>We're <?php echo $company_name;?></span></h2>
                      <!--   <blockquote>
                            A man who has never gone to school may steal from a freight car; but if he has a university education, he may steal the whole railroad.
                        </blockquote> -->
                        <p>
                           <?php echo $about?>
                        </p>
                        <a class="btn circle btn-theme effect btn-md" href="<?php echo site_url().'about-us'?>">Know More</a>
                    </div>
                    <div class="col-md-6 thumb">
                        <div class="thumb">
                            <img src="assets/img/about/2.jpg" alt="Thumb">
                            <a href="https://www.youtube.com/watch?v=DKz_EEoJRs4" class="popup-youtube light video-play-button">
                                <i class="fa fa-play"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End About -->

    <!-- Start Popular Courses
    ============================================= -->
   <?php echo $this->load->view("site/main_services", '');?> 
    <!-- End Popular Courses -->

    <!-- Start Advisor 
    ============================================= -->
    <?php echo $this->load->view("site/team_list", '');?> 
    <!-- End Advisor -->

    <!-- Start Testimonials 
    ============================================= -->
     <?php echo $this->load->view("site/testimonials", '');?> 
    <!-- End Testimonials -->

    <!-- Start Event 
    ============================================= -->
 
    <!-- End Event -->


    <!-- Start Blog 
    ============================================= -->
    <?php echo $this->load->view("site/recent_blogs", '');?> 
    <!-- End Blog -->