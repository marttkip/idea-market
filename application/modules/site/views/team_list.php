
  <div class="advisor-area default-padding bottom-less bg-cover">
        <div class="container">
            <div class="row">
                <div class="site-heading text-center">
                    <div class="col-md-8 col-md-offset-2">
                        <h2>Out Team</h2>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="advisor-items col-3 text-light text-center">
                    <!-- Single item -->

                     <?php
                                $specialist_query = $this->site_model->get_active_items('Our Team');
                                  $specialist_item = '';
                                  if($specialist_query->num_rows() > 0)
                                  {
                                    $x=0;
                                    foreach($specialist_query->result() as $row)
                                    {
                                      $about_title = $row->post_title;
                                      $post_id = $row->post_id;
                                      $blog_category_name = $row->blog_category_name;
                                      $blog_category_id = $row->blog_category_id;
                                      $post_title = $row->post_title;
                                      $web_name = $this->site_model->create_web_name($post_title);
                                      $post_status = $row->post_status;
                                      $post_views = $row->post_views;
                                      $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
                                      $created_by = $row->created_by;
                                      $modified_by = $row->modified_by;
                                      $post_target = $row->post_target;
                                      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
                                      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
                                      $description = strtoupper($row->post_content);
                                      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
                                      $created = $row->created;
                                      $day = date('j',strtotime($created));
                                      $month = date('M',strtotime($created));
                                      $year = date('Y',strtotime($created));
                                      $created_on = date('jS M Y',strtotime($row->created));
                                      $x++;
                                      if($x < 9)
                                      {
                                        $x = '0'.$x;
                                      }
                            
                                      
                                      ?>

                                      <div class="col-md-3 col-sm-6 single-item">
                                        <div class="item">
                                            <div class="thumb">
                                                <img src="<?php echo $image_specialist;?>" alt="Thumb">
                                                <ul>
                                                    <li class="facebook">
                                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                                    </li>
                                                    <li class="twitter">
                                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                                    </li>
                                                    <li class="dribbble">
                                                        <a href="#"><i class="fab fa-dribbble"></i></a>
                                                    </li>
                                                    <li class="youtube">
                                                        <a href="#"><i class="fab fa-youtube"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="info">
                                                <span><?php echo $post_target;?></span>
                                                <h4><?php echo $post_title;?></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }

                            }
                                    ?>
                    
                </div>
            </div>
        </div>
    </div>