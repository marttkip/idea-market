<?php

		$result = '';
		
		//if users exist display them
	
		if ($query->num_rows() > 0)
		{	
			//get all administrators
			$administrators = $this->users_model->get_all_administrators();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$post_id = $row->post_id;
				$blog_category_name = $row->blog_category_name;
				$blog_category_id = $row->blog_category_id;
				$post_title = $row->post_title;
				$web_name = $this->site_model->create_web_name($post_title);
				$post_status = $row->post_status;
				$post_views = $row->post_views;
				$image = base_url().'assets/images/posts/'.$row->post_image;
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
				$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
				$description = $row->post_content;
				$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
				$created = $row->created;
				$day = date('j',strtotime($created));
				$month = date('M Y',strtotime($created));
				$created_on = date('jS M Y H:i a',strtotime($row->created));
				
				$categories = '';
				$count = 0;
				//get all administrators
					// $administrators = $this->users_model->get_all_administrators();
					// if ($administrators->num_rows() > 0)
					// {
					// 	$admins = $administrators->result();
						
					// 	if($admins != NULL)
					// 	{
					// 		foreach($admins as $adm)
					// 		{
					// 			$user_id = $adm->user_id;
								
					// 			if($user_id == $created_by)
					// 			{
					// 				$created_by = $adm->first_name;
					// 			}
					// 		}
					// 	}
					// }
					
					// else
					// {
					// 	$admins = NULL;
					// }
				
					foreach($categories_query->result() as $res)
					{
						$count++;
						$category_name = $res->blog_category_name;
						$category_id = $res->blog_category_id;
						
						if($count == $categories_query->num_rows())
						{
							$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>';
						}
						
						else
						{
							$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>, ';
						}
					}
					$comments_query = $this->blog_model->get_post_comments($post_id);
					//comments
					$comments = 'No Comments';
					$total_comments = $comments_query->num_rows();
					if($total_comments == 1)
					{
						$title = 'comment';
					}
					else
					{
						$title = 'comments';
					}
					
					if($comments_query->num_rows() > 0)
					{
						$comments = '';
						foreach ($comments_query->result() as $row)
						{
							$post_comment_user = $row->post_comment_user;
							$post_comment_description = $row->post_comment_description;
							$date = date('jS M Y H:i a',strtotime($row->comment_created));
							
							$comments .= 
							'
								<div class="user_comment">
									<h5>'.$post_comment_user.' - '.$date.'</h5>
									<p>'.$post_comment_description.'</p>
								</div>
							';
						}
					}
				$result .= '

							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 med_toppadder50">
                                <div class="owl_box">
                                    <div class="post_blog_slider">
                                        <div class="owl-carousel owl-theme">
                                            <div class="item">
                                                <img src="'.$image.'" class="img-responsive" alt="story_img" width="849" height="450" />
                                            </div>
                                            <div class="item">
                                                <img src="'.$image.'" class="img-responsive" alt="story_img" width="849" height="450"/>
                                            </div>
                                            <div class="item">
                                                <img src="'.$image.'" class="img-responsive" alt="story_img" width="849" height="450"/>
                                            </div>
                                        </div>

                                        <div class="blog_comment">
                                            <ul>
                                                <li><a href="'.site_url().'blog/view-single/'.$web_name.'"><i class="fa fa-comment" aria-hidden="true"></i>50</a></li>
                                                <li><a href="'.site_url().'blog/view-single/'.$web_name.'""><i class="fa fa-thumbs-up" aria-hidden="true"></i>98</a></li>
                                            </ul>
                                        </div>
                                        <div class="blog_txt">
                                            <h1><a href="'.site_url().'blog/view-single/'.$web_name.'">'.$post_title.'</a></h1>
                                            <div class="blog_txt_info">
                                                <ul>
                                                    <li>BY ADMIN</li>
                                                    <li>'.$created_on.'</li>
                                                </ul>
                                            </div>
                                            <p>'.$mini_desc.'</p>
                                            <a href="'.site_url().'blog/view-single/'.$web_name.'">Read More <i class="fa fa-long-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
		         
		            ';
		        }
			}
			else
			{
				$result = "";
			}
           
          ?>     
<div class="med_tittle_section">
        <div class="med_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="med_tittle_cont_wrapper">
                        <div class="med_tittle_cont">
                            <h1>Blog</h1>
                            <ol class="breadcrumb">
                                <li><a href="<?php echo site_url().'home'?>">Home</a>
                                </li>
                                <li>Blog</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 <div class="blog_section med_toppadder10 med_bottompadder10">
        <div class="container">

            <div class="blog_category_main_wrapper">
                <div class="row">
                    <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <div class="row">
                            
                           <?php echo $result;?>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="page_post med_toppadder20">
                                    <p>Viewing <strong><?php echo $last;?></strong> of <strong><?php echo $total;?></strong> posts</p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                   		<?php echo $this->load->view('blog/includes/sidebar', '', TRUE);?>
                    </div>
                </div>
            </div>
        </div>
    </div>     

        <!-- <p>Viewing <strong><?php //echo $last;?></strong> of <strong><?php //echo $total;?></strong> posts</p> -->
                
        <?php //echo $this->load->view('blog/includes/sidebar', '', TRUE);?>
 	<div class="newsletter_wrapper med_toppadder80 med_bottompadder70">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                <div class="newsletter_text wow fadeOut" data-wow-delay="0.5s">
                    <h3>Your First Step Towards Oral Health For Life Starts Here :</h3>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-12 col-sm-6 col-6">
                <div class="contect_btn_news">
                    <ul>
                        <li><a href="blog_single.html#">Enquiry</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
