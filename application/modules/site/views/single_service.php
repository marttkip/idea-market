<?php
    $company_details = $this->site_model->get_contacts();
    
    if(count($company_details) > 0)
    {
        $email = $company_details['email'];
        $email2 = $company_details['email'];
        $facebook = $company_details['facebook'];
        $twitter = $company_details['twitter'];
        $linkedin = '';// $company_details['linkedin'];
        $logo = $company_details['logo'];
        $company_name = $company_details['company_name'];
        $phone = $company_details['phone']; 
        $mission = $company_details['mission'];     
    
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
        $mission = '';
    }


?>
<?php
  $about_query = $this->site_model->get_active_content_items($title,1);
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
        $blog_web_name = $this->site_model->create_web_name($blog_category_name);
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $gallery_rs = $this->site_model->get_post_gallery($post_id);

      $gallery = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
          $post_gallery_image_thumb = $value->post_gallery_image_thumb;

          $gallery .= ' <li>
                              <div class="portfolio-thumb">
                                  <div class="gc_filter_cont_overlay_wrapper port_uper_div">
                                      <img src="'.$post_gallery_image_name.'" class="zoom image img-responsive" alt="service_img" />
                                      <div class="gc_filter_cont_overlay zoom_popup">
                                          <div class="gc_filter_text"><a href="'.$post_gallery_image_name.'"><i class="fa fa-plus"></i></a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </li>';
        }
      }

    }
  }


  // blog services 




  ?>
   <!-- Start Breadcrumb 
    ============================================= -->
    <div class="breadcrumb-area shadow dark bg-fixed text-center text-light" style="background-image: url(<?php echo base_url().'assets/themes/educom/'?>assets/img/banner/19.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>Course Details</h1>
                    <ul class="breadcrumb">
                        <li><a href="<?php echo site_url().'home'?>"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="<?php echo site_url().'our-courses'?>">Courses</a></li>
                        <li class="active"><?php echo $post_title;?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Start Course Details 
    ============================================= -->
    <div class="course-details-area default-padding">
        <div class="container">
            <div class="row">
                <!-- Start Course Info -->
                <div class="col-md-9">
                    <div class="courses-info">
                        <h2>
                            <?php echo $post_title;?>
                        </h2>
                        <div class="course-meta">
                            <div class="item author">
                                <div class="thumb">
                                    <a href="#"><img alt="Thumb" src="assets/img/team/6.jpg"></a>
                                </div>
                                <div class="desc">
                                    <h4>Author</h4>
                                    <a href="#">Admin</a>
                                </div>
                            </div>
                            <div class="item category">
                                <h4>Category</h4>
                                <a href="#"><?php echo $blog_category_name?></a>
                            </div>
                          
                            <div class="item price">
                                <h4>Price</h4>
                                <span><?php echo $post_target?></span>
                            </div>
                            <div class="align-right">
                               <!--  <a class="btn btn-theme effect btn-sm" href="#">
                                    <i class="fas fa-chart-bar"></i> Enroll
                                </a> -->
                            </div>
                        </div>
                        <img src="<?php echo $image_about?>" alt="Thumb">
                        <!-- Star Tab Info -->
                        <div class="tab-info">
                            <!-- Tab Nav -->
                            <ul class="nav nav-pills">
                                <li class="active">
                                    <a data-toggle="tab" href="#tab1" aria-expanded="true">
                                        Overview
                                    </a>
                                </li>
                               <!--  <li>
                                    <a data-toggle="tab" href="#tab2" aria-expanded="false">
                                        Curriculum
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab3" aria-expanded="false">
                                        Instructors
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tab4" aria-expanded="false">
                                        Reviews
                                    </a>
                                </li> -->
                            </ul>
                            <!-- End Tab Nav -->
                            <!-- Start Tab Content -->
                            <div class="tab-content tab-content-info">
                                <!-- Single Tab -->
                                <div id="tab1" class="tab-pane fade active in">
                                    <div class="info title">
                                        <h4>Course Desscription</h4>
                                        <?php echo $description;?>
                                    </div>
                                </div>
                                <!-- End Single Tab -->
                            </div>
                            <!-- End Tab Content -->
                        </div>
                        <!-- End Tab Info -->
                    </div>
                </div>
                <!-- End Course Info -->

                <!-- Start Course Sidebar -->
                <div class="col-md-3">
                      <?php echo $this->load->view('site/sidebar', '', TRUE);?>
                </div>
                <!-- End Course Sidebar -->
            </div>
        </div>
    </div>
    <!-- End Course Details -->