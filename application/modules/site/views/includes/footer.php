<?php

$contacts = $this->site_model->get_contacts();

  if(count($contacts) > 0)
  {
    $email = $contacts['email'];
    $phone = $contacts['phone'];
    $facebook = $contacts['facebook'];
    $twitter = $contacts['twitter'];
    $linkedin = '';//$contacts['linkedin'];
    $logo = $contacts['logo'];
    $company_name = $contacts['company_name'];
    $address = $contacts['address'];
    $city = $contacts['city'];
    $post_code = $contacts['post_code'];
    $building = $contacts['building'];
    $floor = $contacts['floor'];
    $location = $contacts['location'];
    $about = $contacts['about'];
    $working_weekend = $contacts['working_weekend'];
    $working_weekday = $contacts['working_weekday'];
    
    $location = $post_code.' '.$address.' '.$city.' '.$building.' '.$floor;
    if(!empty($email))
    {
      $mail = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
    }
    
    // if(!empty($facebook))
    // {
    //  $facebook = '<li><a class="social_icon facebook" href="'.$facebook.'" target="_blank"></a></li>';
    // }
    
    // if(!empty($twitter))
    // {
    //  $twitter = '<li><a class="social_icon twitter" href="'.$twitter.'" target="_blank"></li>';
    // }
    
    // if(!empty($linkedin))
    // {
    //  $linkedin = '<li><a class="social_icon googleplus" href="'.$linkedin.'" target="_blank"></li>';
    // }
  }
  else
  {
    $email = '';
    $facebook = '';
    $twitter = '';
    $linkedin = '';
    $logo = '';
    $company_name = '';
  }
$about_query = $this->site_model->get_active_items('Company Courses',5);
 $company_services = '';

 // var_dump($about_query);die();
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = $row->post_content;
    $mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $description), 0, 50)));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $x++;
    if($x < 9)
    {
      $x = '0'.$x;
    }
    $company_services .= '
                          <li>
                                <div class="thumb">
                                    <a href="#">
                                        <img src="'.$image_about.'" alt="Thumb" width="800">
                                    </a>
                                </div>
                                <div class="info">
                                    <a href="'.site_url().'our-courses/'.$web_name.'">'.$post_title.'</a>
                                    <ul class="meta">
                                       
                                        <li>'.$mini_desc.'</li>
                                    </ul>
                                </div>
                            </li>';
  }
}
?>

 <!-- Start Footer 
    ============================================= -->
    <footer class="bg-dark text-light">
        <div class="container">
            <div class="f-items default-padding">
                <div class="row">
                    <!-- Single item -->
                    <div class="col-md-4 col-sm-6 item equal-height">
                        <div class="f-item about">
                            <h4>About</h4>
                            <p>
                               <?php echo $about?>
                            </p>
                            <ul>
                                <li>
                                    <p>Email <span><a href="mailto:<?php echo $email?>"><?php echo $email?></a></span></p>
                                </li>
                                <li>
                                    <p>Office <span><?php echo $location?></span></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Single item -->

                    <!-- Single item -->
                    <div class="col-md-4 col-sm-6 item equal-height">
                        <div class="f-item link">
                            <h4>Quick Links</h4>
                            <ul>
                                <li>
                                    <a href="<?php echo site_url().'home'?>"><i class="ti-angle-right"></i> Home</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url().'about-us'?>"><i class="ti-angle-right"></i> About Us</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url().'our-courses'?>"><i class="ti-angle-right"></i> Courses</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url().'contact-us'?>"><i class="ti-angle-right"></i> Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Single item -->


                    <!-- Single item -->
                    <div class="col-md-4 col-sm-6 item equal-height">
                        <div class="f-item popular-courses">
                            <h4>Popular Courses</h4>
                            <ul>
                               <?php echo $company_services?>
                            </ul>
                        </div>
                    </div>
                    <!-- End Single item -->
                </div>
            </div>
        </div>
        <!-- Start Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>&copy; Copyright 2020. All Rights Reserved by <a href="#"> validtemplates</a></p>
                    </div>
                    <div class="col-md-6 text-right link">
                        <ul>
                            <li>
                                <a href="#">Terms of user</a>
                            </li>
                            <li>
                                <a href="#">License</a>
                            </li>
                            <li>
                                <a href="#">Support</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Footer Bottom -->
    </footer>
    <!-- End Footer -->