  	<head>      


        <title><?php echo $title;?></title>
        <!--meta-->
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta name="description" content="deepmind"/>
        <meta name="keywords" content="medical/deepmind/hospital" />
        <meta name="author" content="" />
        <meta name="MobileOptimized" content="320" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/";?>fontawesome/css/font-awesome.min.css" type="text/css">



          <!-- ========== Favicon Icon ========== -->
        <link rel="shortcut icon" href="<?php echo base_url().'assets/themes/educom/'?>assets/img/favicon.png" type="image/x-icon">

        <!-- ========== Start Stylesheet ========== -->
        <link href="<?php echo base_url().'assets/themes/educom/'?>assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo base_url().'assets/themes/educom/'?>assets/css/flaticon-set.css" rel="stylesheet" />
        <link href="<?php echo base_url().'assets/themes/educom/'?>assets/css/themify-icons.css" rel="stylesheet" />
        <link href="<?php echo base_url().'assets/themes/educom/'?>assets/css/magnific-popup.css" rel="stylesheet" />
        <link href="<?php echo base_url().'assets/themes/educom/'?>assets/css/owl.carousel.min.css" rel="stylesheet" />
        <link href="<?php echo base_url().'assets/themes/educom/'?>assets/css/owl.theme.default.min.css" rel="stylesheet" />
        <link href="<?php echo base_url().'assets/themes/educom/'?>assets/css/animate.css" rel="stylesheet" />
        <link href="<?php echo base_url().'assets/themes/educom/'?>assets/css/bootsnav.css" rel="stylesheet" />
        <link href="<?php echo base_url().'assets/themes/educom/'?>style.css" rel="stylesheet">
        <link href="<?php echo base_url().'assets/themes/educom/'?>assets/css/responsive.css" rel="stylesheet" />
        <!-- ========== End Stylesheet ========== -->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="assets/js/html5/html5shiv.min.js"></script>
          <script src="assets/js/html5/respond.min.js"></script>
        <![endif]-->

        <!-- ========== Google Fonts ========== -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">
    
  </head>
