<!DOCTYPE html>
<html lang="en">
	<?php echo $this->load->view('includes/header', '', TRUE);?>
	
    <body>
    	
        <?php echo $this->load->view('includes/navigation', '', TRUE);?>
        <?php echo $content; ?>
        <?php echo $this->load->view('includes/footer', '', TRUE);?>

         <!--footer wrapper end-->
	    <!--main js files-->
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/jquery-1.12.4.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/bootstrap.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/equal-height.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/jquery.appear.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/jquery.easing.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/jquery.magnific-popup.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/modernizr.custom.13711.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/owl.carousel.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/wow.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/progress-bar.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/isotope.pkgd.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/imagesloaded.pkgd.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/count-to.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/YTPlayer.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/loopcounter.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/jquery.nice-select.min.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/bootsnav.js"></script>
	    <script src="<?php echo base_url().'assets/themes/educom/'?>assets/js/main.js"></script>


	   


      
    
	</body>
</html>