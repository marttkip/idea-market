<?php
	$company_details = $this->site_model->get_contacts();
	
	if(count($company_details) > 0)
	{
		$email = $company_details['email'];
		$email2 = $company_details['email'];
		$facebook = $company_details['facebook'];
		$twitter = $company_details['twitter'];
		$linkedin = '';// $company_details['linkedin'];
		$logo = $company_details['logo'];
		$company_name = $company_details['company_name'];
		$phone = $company_details['phone'];
		
		// if(!empty($email))
		// {
		// 	$email = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$twitter = '<li class="pm_tip_static_bottom" title="Twitter"><a href="#" class="fa fa-twitter" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$linkedin = '<li class="pm_tip_static_bottom" title="Linkedin"><a href="#" class="fa fa-linkedin" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		// if(!empty($facebook))
		// {
		// 	$instagram = '<li class="pm_tip_static_bottom" title="Instagram"><a href="#" class="fa fa-instagram" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$google = '<li class="pm_tip_static_bottom" title="Google Plus"><a href="#" class="fa fa-google-plus" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$facebook = '<li class="pm_tip_static_bottom" title="Facebook"><a href="#" class="fa fa-facebook" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}


?>
<!--Loader-->

<!-- HEADER -->
<!-- <div class="header header-2">

	<div class="navbar-main">
		<div class="container">
		    <nav class="navbar navbar-expand-lg">
		        <a class="navbar-brand" href="<?php echo site_url().'home';?>">
					<img src="<?php echo site_url().'assets/logo/'.$logo?>" alt="" width="120px"/>
				</a>
		        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		            <span class="navbar-toggler-icon"></span>
		        </button>
		        <div class="collapse navbar-collapse" id="navbarNavDropdown">
		            <ul class="navbar-nav ml-auto">
		               <?php echo $this->site_model->get_navigation();?>
		            </ul>
		            <a href="#" class="btn btn-primary btn-nav ml-auto">DONATE NOW</a>
		        </div>
		    </nav>
		</div>
	</div>
</div> -->


<!-- Start Header Top 
    ============================================= -->
    <div class="top-bar-area bg-dark inc-border text-light">
        <div class="container">
            <div class="row">
                
                <div class="col-md-8 address-info text-left">
                    <div class="info">
                        <ul>
                            <li class="social">
                                <a href="<?php echo $facebook?>"><i class="fa fa-facebook-f"></i></a>
                                <a href="<?php echo $twitter?>"><i class="fa fa-twitter"></i></a>
                                <a href="<?php echo $linkedin?>"><i class="fa fa-youtube"></i></a>
                            </li>
                            <li>
                                <i class="fa fa-inbox"></i> Email: <strong><?php echo $email?></strong>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i> Help Line: <strong><?php echo $phone?></strong>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4 link text-right">
                    <ul>
                        <li>
                            <a href="#">Register</a>
                        </li>
                        <li>
                            <a href="#">Login</a>
                        </li>
                    </ul>
                </div>                
            </div>
        </div>
    </div>
    <!-- End Header Top -->

    <!-- Header 
    ============================================= -->
    <header id="home">

        <!-- Start Navigation -->
        <nav class="navbar top-pad navbar-default attr-border-none navbar-fixed navbar-transparent white bootsnav">

            <!-- Start Top Search -->
            <div class="container">
                <div class="row">
                    <div class="top-search">
                        <div class="input-group">
                            <form action="#">
                                <input type="text" name="text" class="form-control" placeholder="Search">
                                <button type="submit">
                                    <i class="fa fa-search"></i>
                                </button>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container">

                <!-- Start Atribute Navigation -->
                <div class="attr-nav">
                    <ul>
                        <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                    </ul>
                </div>        
                <!-- End Atribute Navigation -->

                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="<?php echo site_url().'home'?>">
                        <img src="<?php echo site_url().'assets/logo/'.$logo?>" class="logo logo-display" alt="Logo" width="100px">
                        <img src="<?php echo site_url().'assets/logo/'.$logo?>" class="logo logo-scrolled" alt="Logo"  width="100px">
                    </a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                       <?php echo $this->site_model->get_navigation();?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
        <!-- End Navigation -->

    </header>
    <!-- End Header -->
