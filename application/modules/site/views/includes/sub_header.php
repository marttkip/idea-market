<!-- Sub-header area -->

<div class="pm-sub-header-container">

	<div class="pm-sub-header-info">
    	
        <div class="container">
        	<div class="row">
            	<div class="col-lg-12">
                	
                    <p class="pm-page-title"><?php echo $title;?></p>
                    
                </div>
            </div>
        </div>
        
    </div>
    
    <div class="pm-sub-header-breadcrumbs">
    	
        <div class="container">
        	<div class="row">
            	<div class="col-lg-12">
                	
                    <ul class="pm-breadcrumbs">
                    	<?php echo $this->site_model->get_breadcrumbs();?>
                    </ul>
                    
                </div>
            </div>
        </div>
        
    </div>

</div>

 		<!-- Sub-header area end -->



        <?php

$contacts = $this->site_model->get_contacts();

  if(count($contacts) > 0)
  {
    $email = $contacts['email'];
    $phone = $contacts['phone'];
    $facebook = $contacts['facebook'];
    $twitter = $contacts['twitter'];
    $linkedin = '';//$contacts['linkedin'];
    $logo = $contacts['logo'];
    $company_name = $contacts['company_name'];
    $address = $contacts['address'];
    $city = $contacts['city'];
    $post_code = $contacts['post_code'];
    $building = $contacts['building'];
    $floor = $contacts['floor'];
    $location = $contacts['location'];
    $working_weekend = $contacts['working_weekend'];
    $working_weekday = $contacts['working_weekday'];
    
    if(!empty($email))
    {
      $mail = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
    }
    
    // if(!empty($facebook))
    // {
    //  $facebook = '<li><a class="social_icon facebook" href="'.$facebook.'" target="_blank"></a></li>';
    // }
    
    // if(!empty($twitter))
    // {
    //  $twitter = '<li><a class="social_icon twitter" href="'.$twitter.'" target="_blank"></li>';
    // }
    
    // if(!empty($linkedin))
    // {
    //  $linkedin = '<li><a class="social_icon googleplus" href="'.$linkedin.'" target="_blank"></li>';
    // }
  }
  else
  {
    $email = '';
    $facebook = '';
    $twitter = '';
    $linkedin = '';
    $logo = '';
    $company_name = '';
  }
$popular_query = $this->blog_model->get_popular_posts();

if($popular_query->num_rows() > 0)
{
    $popular_posts = '';
    $count = 0;
    foreach ($popular_query->result() as $row)
    {
        $count++;
        
        if($count < 3)
        {
            $post_id = $row->post_id;
            $post_title = $row->post_title;
            $image = base_url().'assets/images/posts/thumbnail_'.$row->post_image;
            $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
            $description = $row->post_content;
            $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 10));
            $created = date('jS M Y',strtotime($row->created));
            
            $popular_posts .= '

                          <li class="icon_small_arrow right_white">
                            <a href="'.site_url().'blog/view-single/'.$post_id.'">
                             '.$mini_desc.'.
                            </a>
                            <abbr title="'.$created.'" class="timeago">'.$created.'</abbr>
                          </li>
                
            ';
        }
    }
}

else
{
    $popular_posts = 'There are no posts yet';
}
?>


<div class="footer_wrapper">
        <div class="container">
            <div class="box_1_wrapper">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="address_main">
                            <div class="footer_widget_add">
                                <a href="index.html"><img src="<?php echo site_url().'assets/logo/'.$logo;?>" class="img-responsive" alt="footer_logo" /></a>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor.</p>
                                <a href="index.html#">Read More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                            <div class="footer_box_add">
                                <ul>
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>Address : </span>-512/fonia,canada</li>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i><span>Call us : </span>+61 5001444-122</li>
                                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="index.html#"><span>Email :</span> dummy@example.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box_1_wrapper">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="address_main">
                            <div class="footer_widget_add">
                                <a href="index.html"><img src="<?php echo site_url().'assets/logo/'.$logo;?>" class="img-responsive" alt="footer_logo" /></a>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor.</p>
                                <a href="index.html#">Read More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                            <div class="footer_box_add">
                                <ul>
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>Address : </span>-512/fonia,canada</li>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i><span>Call us : </span>+61 5001444-122</li>
                                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="index.html#"><span>Email :</span> dummy@example.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
            <!-- <div class="container-fluid">
                <div class="up_wrapper">
                    <a href="javascript:" id="return-to-top"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></a>
                </div>
            </div> -->
        </div>
    </div>
    <!--footer wrapper end-->
