<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class Blog extends admin 
{
	var $posts_path;
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('users_model');
		$this->load->model('blog_model');
		$this->load->model('file_model');
		
		$this->load->library('image_lib');
		
		//path to image directory
		$this->posts_path = realpath(APPPATH . '../assets/images/posts');
	}
    
	/*
	*
	*	Default action is to show all the posts
	*
	*/
	public function index() 
	{
		$where = 'post.blog_category_id = blog_category.blog_category_id';
		$table = 'post, blog_category';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'all-posts';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 2;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->blog_model->get_all_posts($table, $where, $config["per_page"], $page);
		
		if ($query->num_rows() > 0)
		{
			$v_data['query'] = $query;
			$v_data['page'] = $page;
			$data['content'] = $this->load->view('blog/all_posts', $v_data, true);
		}
		
		else
		{
			$data['content'] = '<a href="'.site_url().'add-post" class="btn btn-success pull-right">Add post</a>There are no posts';
		}
		$data['title'] = 'All posts';
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function search_group_post()
	{
		$post_category = $this->input->post('post_category');
		$post_title = $this->input->post('post_title');
		$search = '';
		
		if(!empty($post_title))
		{
			$search .= ' AND post.post_title = \''.$post_title.'\'';
		}
		
		if(!empty($post_category))
		{
			$search .= ' AND post.blog_category_id = \''.$post_category.'\'';
		}
		
		if(!empty($search))
		{
			$this->session->set_userdata('group_post_search', $search);
		}
		
		redirect('company-services/services-list');
	}
	
	public function close_group_post_search()
	{
		$this->session->unset_userdata('group_post_search');
		redirect('company-services/services-list');
	}
    /*
	*
	*	Default action is to show all the posts
	*
	*/
	public function group_post($web_name) 
	{
		// get category id
		$category_name = $this->blog_model->decode_web_name($web_name);
		// echo $category_name;die();
		$category_id = $this->blog_model->get_category_id($category_name);//echo $category_name;die();
		// 
		$where = 'post.blog_category_id = blog_category.blog_category_id AND (post.blog_category_id ='.$category_id.' OR blog_category.blog_category_parent = '.$category_id.' OR blog_category.blog_category_parent IN (SELECT blog_category_id FROM blog_category WHERE blog_category_parent = '.$category_id.') OR blog_category.blog_category_parent IN (SELECT blog_category_id FROM blog_category WHERE blog_category_parent IN (SELECT blog_category_id FROM blog_category WHERE blog_category_parent = '.$category_id.')))'; 
		
		$search = $this->session->userdata('group_post_search');
		
		if(!empty($search))
		{
			$where .= ' '.$search;
		}
		 
		$table = 'post, blog_category';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'web-content/'.$web_name;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 3;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$order = 'blog_category_name';
		$order_method = 'ASC';
		$query = $this->blog_model->get_all_posts($table, $where, $config["per_page"], $page, $order, $order_method);

		$v_data['title'] = $category_name;
		$v_data['title_url'] = $web_name;
		
		$this->db->order_by('blog_category_name');
		$v_data['categories_query'] = $this->db->get('blog_category');
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('blog/all_posts', $v_data, true);
		
		$data['title'] = $category_name;
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Add a new post
	*
	*/
	public function add_post() 
	{
		//form validation rules
		$this->form_validation->set_rules('blog_category_id', 'Post Category', 'required|xss_clean');
		$this->form_validation->set_rules('created', 'Post Date', 'xss_clean');
		$this->form_validation->set_rules('post_title', 'Post Name', 'required|xss_clean');
		$this->form_validation->set_rules('post_status', 'Post Status', 'required|xss_clean');
		$this->form_validation->set_rules('post_content', 'Post Description', 'required|xss_clean');
		
		// var_dump($_POST); die();
		//if form has been submitted
		if ($this->form_validation->run())
		{

			//upload product's gallery images
			$resize['width'] = 600;
			$resize['height'] = 800;
			
			if(is_uploaded_file($_FILES['post_image']['tmp_name']))
			{
				$posts_path = $this->posts_path;
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_file($posts_path, 'post_image', $resize);
				if($response['check'])
				{
					$file_name = $response['file_name'];
					$thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					
					$data['title'] = 'Add New Post';
					$data['content'] = $this->load->view('posts/add_post', '', true);
					$this->load->view('admin/templates/general_page', $data);
					break;
				}
			}
			
			else{
				$file_name = '';
			}
			
			if($this->blog_model->add_post($file_name, $thumb_name))
			{
				$this->session->set_userdata('success_message', 'Post added successfully');
				redirect('all-posts');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add post. Please try again');
			}
		}
		
		//open the add new post
		$data['title'] = 'Add New post';
		$categories_query = $this->blog_model->get_all_active_categories();
		if($categories_query->num_rows > 0)
		{
			$categories = '<select class="form-control" name="blog_category_id">';
			
			foreach($categories_query->result() as $res)
			{
				$categories .= '<option value="'.$res->blog_category_id.'">'.$res->blog_category_name.'</option>';
			}
			$categories .= '</select>';
			
			$v_data['categories'] = $categories;
			$data['content'] = $this->load->view('blog/add_post', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Please add blog categories to continue. Add categories <a href="'.site_url().'add-blog-category">here</a>';
		}
		$this->load->view('admin/templates/general_page', $data);
	}




	/*
	*
	*	Add a new post
	*
	*/
	public function add_post_content($web_name) 
	{

		$category_name = $this->blog_model->decode_web_name($web_name);
		$category_id = $this->blog_model->get_category_id($category_name);

		//form validation rules
		$this->form_validation->set_rules('created', 'Post Date', 'required|xss_clean');
		$this->form_validation->set_rules('post_title', 'Post Name', 'required|xss_clean');
		$this->form_validation->set_rules('post_status', 'Post Status', 'required|xss_clean');
		$this->form_validation->set_rules('post_content', 'Post Description', 'xss_clean');

		if($web_name == 'about-us')
		{
			$url_changed = 'web-content/about-us';
		}
		else if($web_name == 'drivers')
		{
			$url_changed = 'web-content/drivers';
		}
		else if($web_name == 'mobile-app')
		{
			$url_changed = 'web-content/mobile-app';
		}
		else if($web_name == 'our-awards')
		{
			$url_changed = 'web-content/our-awards';
		}
		else if($web_name == 'student-affairs')
		{
			$url_changed = 'web-content/student-affairs';
		}
		else if($web_name == 'news')
		{
			$url_changed = 'news';
		}
		else if($web_name == 'services-list')
		{
			$url_changed = 'company-services/services-list';
		}
		else if($web_name == 'admin-posts')
		{
			$url_changed = 'blog/admin-posts';
		}
		else
		{
			if(empty($web_name))
			{

				$url_changed = 'all-posts';
			}
			else
			{
				$url_changed = 'web-content/'.$web_name;
			}
		}

				$posts_path = $this->posts_path;
				// var_dump($posts_path); die();

		//if form has been submitted
		if ($this->form_validation->run())
		{
			//upload product's gallery images
			$resize['width'] = 600;
			$resize['height'] = 800;
			
			if(is_uploaded_file($_FILES['post_image']['tmp_name']))
			{
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_file($posts_path, 'post_image', $resize);
				if($response['check'])
				{
					$file_name = $response['file_name'];
					$thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					
					$data['title'] = 'Add New Post';
					$data['content'] = $this->load->view('posts/add_post', '', true);
					$this->load->view('admin/templates/general_page', $data);
					break;
				}



				$response = $this->file_model->upload_any_file($posts_path, 'post_image');
				if($response['check'])
				{
					$file_name = $response['file_name'];
					$thumb_name = $response['file_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);

					$file_name = '';
					$thumb_name = '';
				}
			}
			
			else{
				$file_name = '';
			}
			
			//Upload header image
			if(is_uploaded_file($_FILES['header_image']['tmp_name']))
			{
				//echo 'uploaded'; die();
				$posts_path = $this->posts_path;
				
				//delete original image
				$this->file_model->delete_file($posts_path."\\".$this->input->post('current_header_image'), $posts_path);
				
				//delete original thumbnail
				$this->file_model->delete_file($posts_path."\\thumbnail_".$this->input->post('current_header_image'), $posts_path);
				/*
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$resize['width'] = 1920;
				$resize['height'] = 250;
				$response = $this->file_model->upload_file($posts_path, 'header_image', $resize);
				if($response['check'])
				{
					$header_image_file_name = $response['file_name'];
					$header_image_thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					$header_image_file_name = '';
				}
			}
			
			else{
				$header_image_file_name = '';
			}
			
			//upload attachment
			$attachment_name = '';
			if(is_uploaded_file($_FILES['attachment']['tmp_name']))
			{
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_any_file($posts_path, 'attachment');
				if($response['check'])
				{
					$attachment_name = $response['file_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
				}
			}
			
			$post_id = $this->blog_model->add_post_content($file_name, $thumb_name,$category_id, $header_image_file_name, $attachment_name);
			if($post_id > 0)
			{
				//upload gallery images
				$resize['width'] = 800;
				$resize['height'] = 600;
				$response = $this->file_model->upload_post_gallery($post_id, $this->posts_path, $resize);
				
				$this->session->set_userdata('success_message', 'Post added successfully');
				redirect($url_changed);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add post. Please try again');
			}
		}
		
		//open the add new post
		$data['title'] = 'Add New post';
		$v_data['title_url'] = $web_name;
		$categories_query = $this->blog_model->get_all_active_categories();
		if($categories_query->num_rows > 0)
		{
			$categories = '<select class="form-control" name="blog_category_id">';
			
			foreach($categories_query->result() as $res)
			{

				if($res->blog_category_id == $category_id)
				{
					$categories .= '<option value="'.$res->blog_category_id.'" selected>'.$res->blog_category_name.'</option>';
				}
				else
				{
					$categories .= '<option value="'.$res->blog_category_id.'">'.$res->blog_category_name.'</option>';
				}
				
			}
			$categories .= '</select>';
			
			$v_data['categories'] = $categories;
			$data['content'] = $this->load->view('blog/add_post', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Please add blog categories to continue. Add categories <a href="'.site_url().'add-blog-category">here</a>';
		}
		$this->load->view('admin/templates/general_page', $data);
	}
    

    public function add_campaign($web_name = "company-initiatives") 
	{

		$category_name = $this->blog_model->decode_web_name($web_name);
		$category_id = $this->blog_model->get_category_id($category_name);

		//form validation rules
		$this->form_validation->set_rules('post_title', 'Post Name', 'required|xss_clean');
		$this->form_validation->set_rules('post_content', 'Post Description', 'xss_clean');
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $data['contacts'] = $contacts;

		// var_dump($_POST); die();

		$url_changed = $this->input->post('uri_string');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			//upload product's gallery images
			$resize['width'] = 600;
			$resize['height'] = 800;
			
			
			$posts_path = $this->posts_path;
			
			//upload attachment
			$file_name = '';
			if(is_uploaded_file($_FILES['post_image']['tmp_name']))
			{
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_any_file($posts_path, 'post_image');
				if($response['check'])
				{
					$file_name = $response['file_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
				}
			}

			// var_dump($file_name); die();
			
			$post_id = $this->blog_model->add_post_content_post($file_name, $file_name,$category_id, "", "");
			if($post_id > 0)
			{
				//upload gallery images
				$resize['width'] = 800;
				$resize['height'] = 600;
				$response = $this->file_model->upload_post_gallery($post_id, $this->posts_path, $resize);
				
				$this->session->set_userdata('success_message', 'Post added successfully');
				// redirect($url_changed);
				redirect('conservancy/profile');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add post. Please try again');
			}
		}
		
		//open the add new post
		$data['title'] = 'Add New post';
		$v_data['title_url'] = $web_name;
		
		$data['content'] = $this->load->view('blog/add_post_site', $v_data, true);

		$this->load->view("site/templates/general_page", $data);
	}   

	 
	/*
	*
	*	Edit an existing post
	*	@param int $post_id
	*
	*/
	public function edit_post($post_id,$web_name) 
	{
		$category_name = $this->blog_model->decode_web_name($web_name);
		$category_id = $this->blog_model->get_category_id($category_name);

		if($web_name == 'about-us')
		{
			$url_changed = 'web-content/about-us';
		}
		else if($web_name == 'drivers')
		{
			$url_changed = 'web-content/drivers';
		}
		else if($web_name == 'mobile-app')
		{
			$url_changed = 'web-content/mobile-app';
		}
		else if($web_name == 'our-awards')
		{
			$url_changed = 'web-content/our-awards';
		}
		else if($web_name == 'student-affairs')
		{
			$url_changed = 'web-content/student-affairs';
		}
		else if($web_name == 'news')
		{
			$url_changed = 'news';
		}
		else if($web_name == 'services-list')
		{
			$url_changed = 'company-services/services-list';
		}
		else if($web_name == 'admin-posts')
		{
			$url_changed = 'blog/admin-posts';
		}
		else
		{
			if(empty($web_name))
			{

				$url_changed = 'all-posts';
			}
			else
			{
				$url_changed = 'web-content/'.$web_name;
			}
		}

		//form validation rules
		$this->form_validation->set_rules('blog_category_id', 'Post Category', 'required|xss_clean');
		$this->form_validation->set_rules('created', 'Post Date', 'xss_clean');
		$this->form_validation->set_rules('post_title', 'Post Name', 'required|xss_clean');
		$this->form_validation->set_rules('post_status', 'Post Status', 'required|xss_clean');
		$this->form_validation->set_rules('post_content', 'Content', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//upload product's gallery images
			$resize['width'] = 600;
			$resize['height'] = 800;
			//var_dump($_FILES['post_image']); die();
			if(is_uploaded_file($_FILES['post_image']['tmp_name']))
			{
				//echo 'uploaded'; die();
				$posts_path = $this->posts_path;
				
				//delete original image
				$this->file_model->delete_file($posts_path."\\".$this->input->post('current_image'), $posts_path);
				
				//delete original thumbnail
				$this->file_model->delete_file($posts_path."\\thumbnail_".$this->input->post('current_image'), $posts_path);
				/*
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_file($posts_path, 'post_image', $resize);
				if($response['check'])
				{
					$file_name = $response['file_name'];
					$thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					$file_name = $this->input->post('current_image');
					$thumb_name = 'thumbnail_'.$this->input->post('current_image');
				}


				// $response = $this->file_model->upload_any_slideshow($posts_path, 'post_image');
				// if($response['check'])
				// {
				// 	$file_name = $response['file_name'];
				// 	$thumb_name = "thumbnail_".$response['file_name'];
				// }
			
				// else
				// {
				// 	$this->session->set_userdata('error_message', $response['error']);

				// 	$file_name = '';
				// 	$thumb_name = '';
				// }
			}
			
			else{
				$file_name = $this->input->post('current_image');
				$thumb_name = 'thumbnail_'.$this->input->post('current_image');
			}
			
			//upload attachment
			$attachment_name = $this->input->post('current_attachment');
			if(is_uploaded_file($_FILES['attachment']['tmp_name']))
			{
				//echo 'uploaded'; die();
				$posts_path = $this->posts_path;
				
				//delete original image
				$this->file_model->delete_file($posts_path."\\".$this->input->post('current_attachment'), $posts_path);
				/*
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_any_file($posts_path, 'attachment');
				if($response['check'])
				{
					$attachment_name = $response['file_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
				}
			}
			
			//Upload header image
			if(is_uploaded_file($_FILES['header_image']['tmp_name']))
			{
				//echo 'uploaded'; die();
				$posts_path = $this->posts_path;
				
				//delete original image
				$this->file_model->delete_file($posts_path."\\".$this->input->post('current_header_image'), $posts_path);
				
				//delete original thumbnail
				$this->file_model->delete_file($posts_path."\\thumbnail_".$this->input->post('current_header_image'), $posts_path);
				/*
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$resize['width'] = 1920;
				$resize['height'] = 250;
				$response = $this->file_model->upload_file($posts_path, 'header_image', $resize);
				if($response['check'])
				{
					$header_image_file_name = $response['file_name'];
					$header_image_thumb_name = $response['thumb_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
				}
			}
			
			else{
				$header_image_file_name = $this->input->post('current_header_image');
				$header_image_thumb_name = 'thumbnail_'.$this->input->post('current_header_image');
			}
			
			//upload gallery images
			$resize['width'] = 800;
			$resize['height'] = 600;
			$response = $this->file_model->upload_post_gallery($post_id, $this->posts_path, $resize);
			
			//update post
			if($this->blog_model->update_post($file_name, $thumb_name, $post_id, $header_image_file_name, $header_image_thumb_name, $attachment_name))
			{
				$this->session->set_userdata('success_message', 'Post updated successfully');
				redirect($url_changed);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update post. Please try again');
			}
		}
		
		//open the add new post
		$data['title'] = 'Edit Post';
		$v_data['title'] = 'Edit Post '.$category_name;
		$v_data['title_url'] = $web_name;
		
		//select the post from the database
		$query = $this->blog_model->get_post($post_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['post'] = $query->result();
			
			$categories_query = $this->blog_model->get_all_active_categories();
			$v_data['post_gallery'] = $this->blog_model->get_all_post_images($post_id);
			if($categories_query->num_rows > 0)
			{
				$categories = '<select class="form-control" name="blog_category_id">';
				
				foreach($categories_query->result() as $res)
				{
					if($v_data['post'][0]->blog_category_id == $res->blog_category_id)
					{
						$categories .= '<option value="'.$res->blog_category_id.'" selected="selected">'.$res->blog_category_name.'</option>';
					}
					else
					{
						$categories .= '<option value="'.$res->blog_category_id.'">'.$res->blog_category_name.'</option>';
					}
				}
				$categories .= '</select>';
				
				$v_data['categories'] = $categories;
			
				$data['content'] = $this->load->view('blog/edit_post', $v_data, true);
			}
			
			else
			{
				$data['content'] = 'Please add blog categories to continue. Add categories <a href="'.site_url().'add-blog-category">here</a>';
			}
		}
		
		else
		{
			$data['content'] = 'post does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
    


    /*
	*
	*	Edit an existing post
	*	@param int $post_id
	*
	*/
	public function edit_campaign($web_name,$post_id) 
	{
		$category_name = $this->blog_model->decode_web_name($web_name);
		$category_id = $this->blog_model->get_category_id($category_name);

		

		//form validation rules
		$this->form_validation->set_rules('blog_category_id', 'Post Category', 'required|xss_clean');
		$this->form_validation->set_rules('post_title', 'Post Name', 'required|xss_clean');
		$this->form_validation->set_rules('post_content', 'Content', 'required|xss_clean');

		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $data['contacts'] = $contacts;
		

		$url_changed = $this->input->post('uri_string');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			//upload product's gallery images
			$resize['width'] = 600;
			$resize['height'] = 800;

			$posts_path = $this->posts_path;
			
			
			//upload attachment
			$attachment_name = '';
			if(is_uploaded_file($_FILES['post_image']['tmp_name']))
			{
				//echo 'uploaded'; die();
				$posts_path = $this->posts_path;
				
				//delete original image
				// $this->file_model->delete_file($posts_path."\\".$this->input->post('current_attachment'), $posts_path);
				/*
				/*
					-----------------------------------------------------------------------------------------
					Upload image
					-----------------------------------------------------------------------------------------
				*/
				$response = $this->file_model->upload_any_file($posts_path, 'post_image');
				if($response['check'])
				{
					$attachment_name = $response['file_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
				}
			}
			
			
			
			//update post
			if($this->blog_model->update_post_campaign($attachment_name, $attachment_name, $post_id, '', '', ''))
			{
				$this->session->set_userdata('success_message', 'Post updated successfully');
				// redirect($url_changed);
				redirect('conservancy/profile');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update post. Please try again');
			}
		}
		
		//open the add new post
		$data['title'] = 'Edit Post';
		$v_data['title'] = 'Edit Post '.$category_name;
		$v_data['title_url'] = $web_name;
		
		//select the post from the database
		$query = $this->blog_model->get_post($post_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['post'] = $query->result();
			$data['content'] = $this->load->view('blog/edit_post_campaign', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'post does not exist';
		}
		
		$this->load->view("site/templates/general_page", $data);

	}
	/*
	*
	*	Delete an existing post
	*	@param int $post_id
	*
	*/
	public function delete_post($post_id,$web_name)
	{//die();

		if($web_name == 'about-us')
		{
			$url_changed = 'web-content/about-us';
		}
		else if($web_name == 'drivers')
		{
			$url_changed = 'web-content/drivers';
		}
		else if($web_name == 'mobile-app')
		{
			$url_changed = 'web-content/mobile-app';
		}
		else if($web_name == 'our-awards')
		{
			$url_changed = 'web-content/our-awards';
		}
		else if($web_name == 'student-affairs')
		{
			$url_changed = 'web-content/student-affairs';
		}
		else if($web_name == 'news')
		{
			$url_changed = 'news';
		}
		else if($web_name == 'services-list')
		{
			$url_changed = 'company-services/services-list';
		}
		else if($web_name == 'admin-posts')
		{
			$url_changed = 'blog/admin-posts';
		}
		else
		{
			if(empty($web_name))
			{

				$url_changed = 'all-posts';
			}
			else
			{
				$url_changed = 'web-content/'.$web_name;
			}
		}
		$query = $this->blog_model->get_post($post_id);
		
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			$image = $result[0]->post_image;
			
			$this->load->model('file_model');
			//delete image
			$this->file_model->delete_file($this->posts_path."\\".$image, $this->posts_path);
			//delete thumbnail
			$this->file_model->delete_file($this->posts_path."\\thumbnail_".$image, $this->posts_path);
		}
		//delete posts of that category
		$this->blog_model->delete_post_comments($post_id);
		$this->blog_model->delete_post($post_id);
		$this->session->set_userdata('success_message', 'Post has been deleted');
		redirect($url_changed);
	}
    
	/*
	*
	*	Activate an existing post
	*	@param int $post_id
	*
	*/
	public function activate_post($post_id,$web_name)
	
	{
		if($web_name == 'about-us')
		{
			$url_changed = 'web-content/about-us';
		}
		else if($web_name == 'drivers')
		{
			$url_changed = 'web-content/drivers';
		}
		else if($web_name == 'mobile-app')
		{
			$url_changed = 'web-content/mobile-app';
		}
		else if($web_name == 'our-awards')
		{
			$url_changed = 'web-content/our-awards';
		}
		else if($web_name == 'student-affairs')
		{
			$url_changed = 'web-content/student-affairs';
		}
		else if($web_name == 'news')
		{
			$url_changed = 'news';
		}
		else if($web_name == 'services-list')
		{
			$url_changed = 'company-services/services-list';
		}
		else if($web_name == 'admin-posts')
		{
			$url_changed = 'blog/admin-posts';
		}
		else
		{
			if(empty($web_name))
			{

				$url_changed = 'all-posts';
			}
			else
			{
				$url_changed = 'web-content/'.$web_name;
			}
		}
		$this->blog_model->activate_post($post_id);
		$this->session->set_userdata('success_message', 'Post activated successfully');
		redirect($url_changed);
	}
    
	/*
	*
	*	Deactivate an existing post
	*	@param int $post_id
	*
	*/
	public function deactivate_post($post_id,$web_name)

   {  
   		if($web_name == 'about-us')
		{
			$url_changed = 'web-content/about-us';
		}
		else if($web_name == 'drivers')
		{
			$url_changed = 'web-content/drivers';
		}
		else if($web_name == 'mobile-app')
		{
			$url_changed = 'web-content/mobile-app';
		}
		else if($web_name == 'our-awards')
		{
			$url_changed = 'web-content/our-awards';
		}
		else if($web_name == 'student-affairs')
		{
			$url_changed = 'web-content/student-affairs';
		}
		else if($web_name == 'news')
		{
			$url_changed = 'news';
		}
		else if($web_name == 'services-list')
		{
			$url_changed = 'company-services/services-list';
		}
		else if($web_name == 'admin-posts')
		{
			$url_changed = 'blog/admin-posts';
		}
		else
		{
			if(empty($web_name))
			{

				$url_changed = 'all-posts';
			}
			else
			{
				$url_changed = 'web-content/'.$web_name;
			}
		}
        $this->blog_model->deactivate_post($post_id);
		$this->session->set_userdata('success_message', 'Post disabled successfully');
		redirect($url_changed);
	}
    
	/*
	*
	*	Post Comments
	*
	*/
	public function comments($post_id = NULL) 
	{
		$where = 'post.post_id = post_comment.post_id';
		if($post_id == NULL)
		{
			$segment = 2;
		}
		
		else
		{
			$where .= ' AND post_comment.post_id = '.$post_id;
			$segment = 3;
		}
		$table = 'post_comment, post';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'comments/'.$post_id;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->blog_model->get_comments($table, $where, $config["per_page"], $page);
		
		if ($query->num_rows() > 0)
		{
			$v_data['query'] = $query;
			$v_data['page'] = $page;
			$v_data['post_id'] = $post_id;
			$v_data['title'] = $this->blog_model->get_comment_title($post_id);
			$data['content'] = $this->load->view('blog/comments', $v_data, true);
		}
		
		else
		{
			if($post_id != NULL)
			{
				$data['content'] = '<a href="'.site_url().'add-comment/'.$post_id.'" class="btn btn-success pull-right">Add Comment</a>There are no comments';
			}
			
			else
			{
				$data['content'] = 'There are no comments';
			}
		}
		$data['title'] = 'Comments';
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Add a new comment
	*
	*/
	public function add_comment($post_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('post_comment_description', 'Description', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{	
			if($this->blog_model->add_comment_admin($post_id))
			{
				$this->session->set_userdata('success_message', 'Comment added successfully');
				redirect('comments/'.$post_id);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add comment. Please try again');
			}
		}
		
		//open the add new post
		$data['title'] = 'Add Comment';
		$v_data['post_id'] = $post_id;
		$v_data['title'] = $this->blog_model->get_comment_title($post_id);
		$data['content'] = $this->load->view('blog/add_comment', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing comment
	*	@param int $post_comment_id
	*	@param int $post_id
	*
	*/
	public function delete_comment($post_comment_id, $post_id = NULL)
	{
		$this->blog_model->delete_comment($post_comment_id);
		$this->session->set_userdata('success_message', 'Comment has been deleted');
		redirect('comments/'.$post_id);
	}
    
	/*
	*
	*	Activate an existing comment
	*	@param int $post_comment_id
	*	@param int $post_id
	*
	*/
	public function activate_comment($post_comment_id, $post_id = NULL)
	{
		$this->blog_model->activate_comment($post_comment_id);
		$this->session->set_userdata('success_message', 'Comment activated successfully');
		redirect('comments/'.$post_id);
	}
    
	/*
	*
	*	Deactivate an existing comment
	*	@param int $post_comment_id
	*	@param int $post_id
	*
	*/
	public function deactivate_comment($post_comment_id, $post_id = NULL)
	{
		$this->blog_model->deactivate_comment($post_comment_id);
		$this->session->set_userdata('success_message', 'Comment disabled successfully');
		redirect('comments/'.$post_id);
	}
    
	/*
	*
	*	Blog Categories
	*
	*/
	public function categories() 
	{
		$where = 'blog_category_id > 0';
		$table = 'blog_category';
		$search = $this->session->userdata('category_search');
		if(!empty($search))
		{
			$where .= $search;
		}
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'categories';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 2;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->blog_model->get_all_categories($table, $where, $config["per_page"], $page);
		$this->db->order_by('blog_category_name');
		$v_data['categories_query'] = $this->db->get('blog_category');
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['categories_query'] = $this->blog_model->get_all_active_categories();
		$data['content'] = $this->load->view('blog/all_categories', $v_data, true);
		$data['title'] = 'All Categories';
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function add_blog_category()
	{
		//form validation rules
		$this->form_validation->set_rules('blog_category_parent', 'Parent Category', 'required|xss_clean');
		$this->form_validation->set_rules('blog_category_name', 'Category Name', 'required|xss_clean');
		$this->form_validation->set_rules('blog_category_status', 'Category Status', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{	
			if($this->blog_model->add_blog_category())
			{
				$this->session->set_userdata('success_message', 'Category added successfully');
				redirect('categories');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add category. Please try again');
			}
		}
		
		//open the add new post
		$data['title'] = 'Add Category';
		$categories_query = $this->blog_model->get_all_active_categories();
		$categories = '<select class="form-control" name="blog_category_parent"><option value="0">No Parent</option>';
		if($categories_query->num_rows > 0)
		{
			
			foreach($categories_query->result() as $res)
			{
				$categories .= '<option value="'.$res->blog_category_id.'">'.$res->blog_category_name.'</option>';
			}
		}
		$categories .= '</select>';
		
		$v_data['categories'] = $categories;
		$data['content'] = $this->load->view('blog/add_category', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Edit an existing blog category
	*	@param int $blog_category_id
	*
	*/
	public function edit_blog_category($blog_category_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('blog_category_parent', 'Parent Category', 'required|xss_clean');
		$this->form_validation->set_rules('blog_category_name', 'Category Name', 'required|xss_clean');
		$this->form_validation->set_rules('blog_category_status', 'Category Status', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update post
			if($this->blog_model->update_blog_category($blog_category_id))
			{
				$this->session->set_userdata('success_message', 'Category updated successfully');
				redirect('categories');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update category. Please try again');
			}
		}
		
		//open the add new post
		$data['title'] = 'Edit Category';
		
		//select the post from the database
		$query = $this->blog_model->get_blog_category($blog_category_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['category'] = $query->result();
			$categories_query = $this->blog_model->get_all_active_categories();
			$categories = '<select class="form-control" name="blog_category_parent"><option value="0">No Parent</option>';
			if($categories_query->num_rows > 0)
			{
				
				foreach($categories_query->result() as $res)
				{
					if($v_data['category'][0]->blog_category_parent == $res->blog_category_id)
					{
						$categories .= '<option value="'.$res->blog_category_id.'" selected="selected">'.$res->blog_category_name.'</option>';
					}
					
					else
					{
						$categories .= '<option value="'.$res->blog_category_id.'">'.$res->blog_category_name.'</option>';
					}
				}
			}
			$categories .= '</select>';
			
			$v_data['categories'] = $categories;
			
			$data['content'] = $this->load->view('blog/edit_blog_category', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'post does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing category
	*	@param int $blog_category_id
	*
	*/
	public function delete_blog_category($blog_category_id)
	{
		//delete posts of that category
		$this->blog_model->delete_category_post_comments($blog_category_id);
		$this->blog_model->delete_category_posts($blog_category_id);
		$this->blog_model->delete_blog_category($blog_category_id);
		$this->session->set_userdata('success_message', 'Category has been deleted');
		redirect('categories');
	}
    
	/*
	*
	*	Activate an existing category
	*	@param int $blog_category_id
	*
	*/
	public function activate_blog_category($blog_category_id)
	{
		$this->blog_model->activate_blog_category($blog_category_id);
		$this->session->set_userdata('success_message', 'Category activated successfully');
		redirect('categories');
	}
    
	/*
	*
	*	Deactivate an existing category
	*	@param int $blog_category_id
	*
	*/
	public function deactivate_blog_category($blog_category_id)
	{
		$this->blog_model->deactivate_blog_category($blog_category_id);
		$this->session->set_userdata('success_message', 'Category disabled successfully');
		redirect('categories');
	}
	
	public function search_category()
	{
		$blog_category_name = $this->input->post('blog_category_name');
		$post_category = $this->input->post('post_category');
		$search = '';
		
		if(!empty($post_category))
		{
			$search .= ' AND blog_category.blog_category_parent = \''.$post_category.'\'';
		}
		
		if(!empty($blog_category_name))
		{
			$search .= ' AND blog_category.blog_category_name = \''.$blog_category_name.'\'';
		}
		
		if(!empty($search))
		{
			$this->session->set_userdata('category_search', $search);
		}
		
		redirect('categories');
	}
	
	public function close_category_search()
	{
		$this->session->unset_userdata('category_search');
		redirect('categories');
	}
	
	public function delete_post_gallery($post_gallery_id, $post_id)
	{
		$this->db->where('post_gallery_id', $post_gallery_id);
		if($this->db->delete('post_gallery'))
		{
			$this->session->set_userdata('success_message', 'Image deleted successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Could not delete image');
		}
		redirect('edit-post/'.$post_id.'/services-list');
	}
}
?>