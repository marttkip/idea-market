<?php
if($event_partners->num_rows() > 0)
{
	$row = $event_partners->row();
	$event_partners_type = $row->event_partners_type;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Sponsors & Partners</h2>
    </header>
    <div class="panel-body">
    	<?php
			$success = $this->session->userdata('success_message');
			if(!empty($success))
			{
				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
				$this->session->unset_userdata('success_message');
			}
			
			$error = $this->session->userdata('error_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				$this->session->unset_userdata('error_message');
			}
			$error2 = validation_errors(); 
			if(!empty($error2)){?>
				<div class="row">
					<div class="col-md-6 col-md-offset-2">
						<div class="alert alert-danger">
							<strong>Error!</strong> <?php echo validation_errors(); ?>
						</div>
					</div>
				</div>
			<?php } ?>
        <!-- Jasny -->
        <link href="<?php echo base_url();?>assets/jasny/jasny-bootstrap.css" rel="stylesheet">		
        <script type="text/javascript" src="<?php echo base_url();?>assets/jasny/jasny-bootstrap.js"></script> 
          <div class="padd">
				<?php
				$attributes = array('role' => 'form', 'class'=>'form-horizontal');
		
				echo form_open_multipart($this->uri->uri_string(), $attributes);
				
				?>
                <div class="row">
                	<div class="col-md-8">
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="gallery_name">Name</label>
                            <div class="col-lg-8">
                            	<input type="text" class="form-control" name="event_partners_name" placeholder="Name" value="<?php echo $row->event_partners_name;?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="gallery_name">Website URL</label>
                            <div class="col-lg-8">
                            	<input type="text" class="form-control" name="event_partners_location" placeholder="Website URL" value="<?php echo $row->event_partners_link;?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Type</label>
                            <div class="col-lg-4">
                                <div class="radio">
                                    <label>
                            			<?php if($event_partners_type == 1){?>
                                        <input id="optionsRadios1" type="radio" value="1" name="event_partners_type" checked>
										<?php }else{?>
                                        <input id="optionsRadios1" type="radio" value="1" name="event_partners_type">
                                        <?php }?>
                                        Partner
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                            			<?php if($event_partners_type == 2){?>
                                        <input id="optionsRadios2" type="radio" value="2" name="event_partners_type" checked>
										<?php }else{?>
                                        <input id="optionsRadios2" type="radio" value="2" name="event_partners_type">
                                        <?php }?>
                                        Sponsor
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                            			<?php if($event_partners_type == 3){?>
                                        <input id="optionsRadios3" type="radio" value="3" name="event_partners_type" checked>
										<?php }else{?>
                                        <input id="optionsRadios3" type="radio" value="3" name="event_partners_type">
                                        <?php }?>
                                        Speaker
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="gallery_name">Description</label>
                            <div class="col-lg-8">
                            	<textarea class="form-control" name="event_partners_description" placeholder="Description"><?php echo $row->event_partners_description;?></textarea>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="image">Partner Image</label>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%;">
                                    <img src="<?php echo $event_location;?>" class="img-responsive"/>
                                </div>
                                <div>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="event_partners_image"></span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                	</div>
                </div>
				
				<div class="form-group center-align">
					<input type="submit" value="Edit Partner" class="login_btn btn btn-success">
				</div>
				<?php
					echo form_close();
				?>
		</div>
        
    </div>
</section>

<?php }?>