<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Phone Number</th>
						<th>Email</th>
						<th>Contact Person</th>
						<th>Contact Person Email</th>
						<th>Contact Person Phone</th>
						<th>Username</th>
						<th>Status</th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				<tbody>
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$applicant_id = $row->applicant_id;
				$applicant_name = $row->applicant_name;
				$applicant_phone = $row->applicant_phone;
				$applicant_email = $row->applicant_email;
				$applicant_status = $row->applicant_status;
				$contact_person = $row->contact_person;
				$contact_person_email = $row->contact_person_email;
				$contact_person_phone = $row->contact_person_phone;
				$username = $row->username;
				$applicant_created = $row->created;
				//status
				if($applicant_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($applicant_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info btn-sm" href="'.site_url().'applicants/activate-conservancy/'.$applicant_id.'" onclick="return confirm(\'Do you want to activate '.$applicant_name.'?\');" title="Activate '.$applicant_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($applicant_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default btn-sm" href="'.site_url().'applicants/activate-conservancy/'.$applicant_id.'" onclick="return confirm(\'Do you want to deactivate '.$applicant_name.'?\');" title="Deactivate '.$applicant_name.'"><i class="fa fa-thumbs-down"></i></a>';
				}

				// $personnel_query = $this->applications_model->get_conservation_personnel($applicant_id);
				
				// $personnel_fname = '';
				// $personnel_onames = '';
				// $personnel_username = '';
				// if($personnel_query->num_rows() == 1)
				// {
				// 	foreach ($personnel_query->result() as $key => $value) {
				// 		# code...
				// 		$personnel_fname= $value->personnel_fname;
				// 		$personnel_onames= $value->personnel_onames;
				// 		$personnel_username= $value->personnel_username;

				// 	}
				// }
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$applicant_name.'</td>
						<td>'.$applicant_phone.'</td>
						<td>'.$applicant_email.'</td>
						<td>'.$contact_person.'</td>
						<td>'.$contact_person_email.'</td>
						<td>'.$contact_person_phone.'</td>
						<td>'.$username.'</td>
						<td>'.$status.'</td>
						<td>'.$button.'</td>
						<td>
						<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#book-appointment'.$applicant_id.'"><i class="fa fa-user"></i> User Account </button>
									<div class="modal fade " id="book-appointment'.$applicant_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									    <div class="modal-dialog modal-lg" role="document">
									        <div class="modal-content ">
									            <div class="modal-header">
									            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									            	<h4 class="modal-title" id="myModalLabel">User Account for '.$applicant_name.'</h4>
									            </div>
									            '.form_open("add-conservancy-user/".$applicant_id, array("class" => "form-horizontal")).'

									            <div class="modal-body">
									            	<div class="row">
									            		<input type="hidden" name="redirect_url'.$applicant_id.'" id="redirect_url'.$applicant_id.'" value="'.$this->uri->uri_string().'">
									            		<input type="hidden" name="applicant_id" id="applicant_id'.$applicant_id.'" value="'.$applicant_id.'">
									            		<div class="col-md-12">
									            			<div class="col-md-6">
									            				<div class="form-group">
																	<label class="col-lg-2 control-label">Name: </label>
																	<div class="col-lg-10">
								                                        <input type="text" class="form-control" name="name'.$applicant_id.'" id="name" value="'.$contact_person.'">
																	</div>
																</div>
															</div>
									            			<div class="col-md-6">
									            				<div class="form-group">
																	<label class="col-lg-2 control-label">Username: </label>
																	<div class="col-lg-10">
								                                        <input type="text" class="form-control" name="username'.$applicant_id.'" id="username" value="'.$username.'">
																	</div>
																</div>
															</div>
															
									            		</div>
									            	</div>

									            	
															
									              	
									            </div>
									            <div class="modal-footer">
									            	<button type="submit" class="btn btn-sm btn-success"> Update User</button>
									                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
									            </div>

									               '.form_close().'
									        </div>
									    </div>
									</div>
								</td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no applicant";
		}
?>

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
			<div class="panel-heading">
                
                <h2 class="panel-title">All <?php echo $title;?></h2>
                <div class="pull-right" style="margin-top: -25px;">
                	
                </div>
                
            </div>

			<div class="panel-body">
		    	<?php
				$search = $this->session->userdata('applicant_search_title2');
				
				if(!empty($search))
				{
					echo '<h6>Filtered by: '.$search.'</h6>';
					echo '<a href="'.site_url().'hr/applicant/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
				}
		        $success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				?>

				<div class="table-responsive">
		        	
					<?php echo $result;?>
			
		        </div>
			</div>
		    <div class="panel-footer">
		    	<?php if(isset($links)){echo $links;}?>
		    </div>
		 </div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="import_applicant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Import applicants</h4>
			</div>
			<div class="modal-body">
				<section class="panel">
                    <!-- Widget content -->
                    <div class="panel-body">
                    <div class="padd">
                        
                    <div class="row">
                    <div class="col-md-12">
                    
                        <?php echo form_open_multipart('applicants/import-applicants/0', array("class" => "form-horizontal", "role" => "form"));?>
                        <div class="row">
                            <div class="col-md-12">
                                <ul>
                                    <li>Download the import template <a href="<?php echo site_url().'applicants/applicants-template';?>">here.</a></li>
                                    
                                    <li>Save your file as a <strong>CSV (Comma Delimited)</strong> file before importing</li>
                                    <li>After adding your applicants to the import template please import them using the button below</li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="row">
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <div class="fileUpload btn btn-primary">
                                    <span>Import applicants</span>
                                    <input type="file" class="upload"  name="import_csv"/>
                                </div>
                            </div>
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <input type="submit" onChange="this.form.submit();" class="btn btn-warning" onclick="return confirm('Do you really want to upload the selected file?')" value="Import">
                            </div>
                        </div>
                               
                                
                    </div>
                    </div>
                        <?php echo form_close();?>
                    </div>
                    </div>
            
            </section>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Companies Modal -->
<div class="modal fade" id="import_company" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Import Companies</h4>
			</div>
			<div class="modal-body">
				<section class="panel">
                    <!-- Widget content -->
                    <div class="panel-body">
                    <div class="padd">
                        
                    <div class="row">
                    <div class="col-md-12">
                    
                        <?php echo form_open_multipart('applicants/import-applicants/1', array("class" => "form-horizontal", "role" => "form"));?>
                        <div class="row">
                            <div class="col-md-12">
                                <ul>
                                    <li>Download the import template <a href="<?php echo site_url().'applicants/companies-template';?>">here.</a></li>
                                    
                                    <li>Save your file as a <strong>CSV (Comma Delimited)</strong> file before importing</li>
                                    <li>After adding your applicants to the import template please import them using the button below</li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="row">
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <div class="fileUpload btn btn-primary">
                                    <span>Import Companies</span>
                                    <input type="file" class="upload"  name="import_csv"/>
                                </div>
                            </div>
                            
                            <div class="col-md-12" style="margin-top:10px">
                                <input type="submit" onChange="this.form.submit();" class="btn btn-warning" onclick="return confirm('Do you really want to upload the selected file?')" value="Import">
                            </div>
                        </div>
                               
                                
                    </div>
                    </div>
                        <?php echo form_close();?>
                    </div>
                    </div>
            
            </section>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>