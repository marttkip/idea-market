 <?php
$about_query = $this->site_model->get_active_items('About Us');
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
  }
}
 ?>
 <section class="inner-header divider layer-overlay overlay-dark" data-bg-img="<?php echo $image_about?>">
  <div class="container pt-0 pb-0">
    <!-- Section Content -->
    <div class="section-content text-center">
      <div class="row"> 
        <div class="col-md-8 col-md-offset-2 text-center">
          <h2 class="text-theme-colored font-36">ADD CAMPAIGN</h2>
        </div>
      </div>
    </div>
  </div>      
</section> 
<?php

$category_id = $this->site_model->get_category_id('Company Initiatives');
// service number two
$about_query = $this->site_model->get_active_child_items($category_id);
$about_sub_menu_services = '<select class="form-control" name="blog_category_id">';
if($about_query->num_rows() > 0)
{
  foreach($about_query->result() as $res)
  {
    $blog_category_name = $res->blog_category_name;
    $blog_category_id = $res->blog_category_id;
    $about_sub_menu_services .= '<option value="'.$blog_category_id.'">'.$blog_category_name.'</option>';
  }
}
$about_sub_menu_services .= '</select>';
?>

<section>
  <div class="container">
    <div class="row">
       <div class="col-md-10 col-md-push-1">
       		<a href="<?php echo site_url().'conservancy/profile'?>" class="btn btn-dark btn-flat btn-sm pull-right" data-loading-text="Please wait..."><i class="fa fa-arrow-left"></i> Back to profile</a>
       		<br>
        	<?php
                if(isset($error)){
                    echo '<div class="alert alert-danger">'.$error.'</div>';
                }
                
                $validation_errors = validation_errors();
                
                if(!empty($validation_errors))
                {
                    echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                }
                ?>
                
                <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
                <div class="row mt-20">
                    <div class="col-md-12">
                        <div class="col-md-6">
                             <!-- post category -->
                            <div class="form-group">
                                <label class="col-lg-2 col-md-2  control-label">Post Category</label>
                                <div class="col-lg-10 col-md-10" >
                                    <?php echo $about_sub_menu_services;?>
                                </div>
                            </div>
                            <!-- post Name -->
                            <div class="form-group">
                                <label class="col-lg-2 col-md-2  control-label">Post Title</label>
                                <div class="col-lg-10 col-md-10" >
                                    <input type="text" class="form-control" name="post_title" placeholder="Post Title" value="<?php echo set_value('post_title');?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-md-2  control-label">Post Video</label>
                                <div class="col-lg-10 col-md-10" >
                                    <input type="text" class="form-control" name="post_video" placeholder="Post Video" value="<?php echo set_value('post_video');?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-md-2  control-label">Target</label>
                                <div class="col-lg-10 col-md-10" >
                                    <input type="text" class="form-control" name="post_target" placeholder="Post Target" value="<?php echo set_value('post_target');?>" >
                                </div>
                            </div>
                                                    
                        </div>
                         <div class="col-md-6">
                           
                            <!-- Image -->
                            <div class="form-group">
                                <label class="col-lg-2 col-md-2  control-label">Post Image</label>
                                <div class="col-lg-10 col-md-10" >
                                    
                                    <div class="row">
                                    
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:200px; height:200px;">
                                                    <img src="http://placehold.it/200x200">
                                                </div>
                                                <div>
                                                    <span class="btn btn-file btn-info"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="post_image"></span>
                                                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- Activate checkbox -->
                        </div>
                    </div>
                </div>
              
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hpanel">
                            <div class="panel-heading">
                               Post Content
                            </div>
                            <div class="panel-body">
                            	<textarea  class="summernote" id="post_content" name="post_content"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                 <input name="uri_string" type="hidden" value="<?php echo $this->uri->uri_string()?>">
               	<br>
                <div class="row">
                    <div class="col-md-12 text-center">
                          <div class="form-actions ">
                            <button class="submit btn btn-primary" onclick="save_items()" id="save-button" type="submit">
                                Add post
                            </button>
                        </div>
                    </div>
                </div>
              
               
              
                <br />
                <?php echo form_close();?>
         
      </div>
    </div>
  </div>
</section>