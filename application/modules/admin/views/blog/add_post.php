  <section class="panel">
        <header class="panel-heading">
            <div class="pull-right">
                <a href="<?php echo site_url();?>web-content/<?php echo $title_url;?>" class="btn btn-sm btn-default pull-right"><i class="fa fa-arrow-left"></i> Back to posts</a>
            </div>
    
            <h2 class="panel-title">Categories</h2>
        </header>
        <div class="panel-body">
              <style type="text/css">
    		  	.add-on{cursor:pointer;}
    		  </style>
              <link href="<?php echo base_url()."assets/themes/jasny/css/jasny-bootstrap.css"?>" rel="stylesheet"/>
              <div class="padd">
                <!-- Adding Errors -->
                <?php
                if(isset($error)){
                    echo '<div class="alert alert-danger">'.$error.'</div>';
                }
                
                $validation_errors = validation_errors();
                
                if(!empty($validation_errors))
                {
                    echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                }
                ?>
                
                <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                             <!-- post category -->
                            <div class="form-group">
                                <label class="col-lg-6 col-md-6  control-label">Post Category</label>
                                <div class="col-lg-6 col-md-6" >
                                    <?php echo $categories;?>
                                </div>
                            </div>
                            <!-- post Name -->
                            <div class="form-group">
                                <label class="col-lg-6 col-md-6  control-label">Post Title</label>
                                <div class="col-lg-6 col-md-6" >
                                    <input type="text" class="form-control" name="post_title" placeholder="Post Title" value="<?php echo set_value('post_title');?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-6 col-md-6  control-label">Post Video</label>
                                <div class="col-lg-6 col-md-6" >
                                    <input type="text" class="form-control" name="post_video" placeholder="Post Video" value="<?php echo set_value('post_video');?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-6 col-md-6  control-label">Target</label>
                                <div class="col-lg-6 col-md-6" >
                                    <input type="text" class="form-control" name="post_target" placeholder="Post Target" value="<?php echo set_value('post_target');?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-6 col-md-6  control-label">Activate Post?</label>
                                <div class="col-lg-6 col-md-6" >
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios1" type="radio" checked value="1" name="post_status">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" value="0" name="post_status">
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="col-lg-6 col-md-6  control-label">Featured ?</label>
                                <div class="col-lg-6 col-md-6" >
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios1" type="radio" checked value="1" name="featured_status">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="optionsRadios2" type="radio" value="0" name="featured_status">
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Post Date: </label>
                                
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input id="datapicker2"  name="created" placeholder="Post Date" value="<?php echo set_value('created');?>" class="form-control" type="text">
                                    </div>
                                </div>
                            </div>
                            <!-- Image -->
                            <div class="form-group">
                                <label class="col-lg-6 col-md-6  control-label">Post Image</label>
                                <div class="col-lg-6 col-md-6" >
                                    
                                    <div class="row">
                                    
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:200px; height:200px;">
                                                    <img src="http://placehold.it/200x200">
                                                </div>
                                                <div>
                                                    <span class="btn btn-file btn-info"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="post_image"></span>
                                                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- Activate checkbox -->
                        </div>
                    </div>
                </div>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                             <label class="col-md-4 control-label" for="image">Gallery Image</label>
                            <div class="col-md-8">
                            <?php echo form_upload(array( 'name'=>'gallery[]', 'multiple'=>true, 'class'=>'btn btn-danger'));?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                             <label class="col-md-4 control-label" for="image">Header Image</label>
                            <div class="col-md-8">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:200px; height:200px;">
                                    	<img src="http://placehold.it/200x200">
                                    </div>
                                    <div>
                                        <span class="btn btn-file btn-info"><span class="fileinput-new">Select Image</span><span class="fileinput-exists">Change</span><input type="file" name="header_image"></span>
                                        <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                             <label class="col-md-4 control-label" for="image">Attachment</label>
                            <div class="col-md-8">
                            <?php echo form_upload(array( 'name'=>'attachment', 'class'=>'btn btn-warning'));?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hpanel">
                            <div class="panel-heading">
                               Post Content
                            </div>
                            <div class="panel-body">
                                <div class="summernote">
                                </div>
                            </div>
                            <textarea  class="cleditor" id="post_content" name="post_content"></textarea>
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-md-12">
                          <div class="form-actions center-align">
                            <button class="submit btn btn-primary" onclick="save_items()" id="save-button" type="submit">
                                Add post
                            </button>
                        </div>
                    </div>
                </div>
              
               
              
                <br />
                <?php echo form_close();?>
    		</div>
        </div>
    </section>
   <script >
          function save_items()
          {
            var mysave = $('div.note-editable').html();
            
            // $('textarea.note-editable').val(mysave);
            $('textarea#post_content').val(mysave);
          }
            $(function(){
                $('#save-button').click(function () {

                    var mysave = $('div.note-editable').html();
                    // alert(mysave);
                    // $('textarea.note-editable').val(mysave);
                    $('textarea#post_content').val(mysave);
                  });
                });
    </script>