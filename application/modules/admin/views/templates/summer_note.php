<?php 
	
	if(!isset($contacts))
	{
		$contacts = $this->site_model->get_contacts();
	}
	$data['contacts'] = $contacts; 

	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$logo = $contacts['logo'];
		$mission = $contacts['mission'];
		$vision = $contacts['vision'];
		$company_name = $contacts['company_name'];
		
		$phone = $contacts['phone'];
		
		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$mission = '';
		$vision = '';
		$google = '';
	}


?>
<!DOCTYPE html>
<html>
<head>
	 <?php echo $this->load->view('admin/includes/summer_note_header', $data, TRUE); ?>
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/jquery/dist/jquery.min.js"></script>
</head>
<body class="fixed-navbar fixed-sidebar">

<input type="hidden" id="base_url" value="<?php echo site_url();?>">
    	<input type="hidden" id="config_url" value="<?php echo site_url();?>">

 <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1><?php echo $company_name;?></h1><p>Mission: <?php echo $mission;?>. </p><p>Vision: <?php echo $vision;?>. </p><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>

<!-- Header -->
<?php echo $this->load->view('admin/includes/top_navigation', $data, TRUE); ?>

<!-- Navigation -->
<?php echo $this->load->view('admin/includes/sidebar', $data, TRUE); ?>

<!-- Main Wrapper -->
<div id="wrapper">

   
<div class="content">

	<?php echo $content;?>
</div>

    <!-- Right sidebar -->
    <div id="right-sidebar" class="animated fadeInRight">

        <div class="p-m">
            <button id="sidebar-close" class="right-sidebar-toggle sidebar-button btn btn-default m-b-md"><i class="pe pe-7s-close"></i>
            </button>
            <div>
                <span class="font-bold no-margins"> Analytics </span>
                <br>
                <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.</small>
            </div>
            <div class="row m-t-sm m-b-sm">
                <div class="col-lg-6">
                    <h3 class="no-margins font-extra-bold text-success">300,102</h3>

                    <div class="font-bold">98% <i class="fa fa-level-up text-success"></i></div>
                </div>
                <div class="col-lg-6">
                    <h3 class="no-margins font-extra-bold text-success">280,200</h3>

                    <div class="font-bold">98% <i class="fa fa-level-up text-success"></i></div>
                </div>
            </div>
            <div class="progress m-t-xs full progress-small">
                <div style="width: 25%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="25" role="progressbar"
                     class=" progress-bar progress-bar-success">
                    <span class="sr-only">35% Complete (success)</span>
                </div>
            </div>
        </div>
        <div class="p-m bg-light border-bottom border-top">
            <span class="font-bold no-margins"> Social talks </span>
            <br>
            <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.</small>
            <div class="m-t-md">
                <div class="social-talk">
                    <div class="media social-profile clearfix">
                        <a class="pull-left">
                            <img src="images/a1.jpg" alt="profile-picture">
                        </a>

                        <div class="media-body">
                            <span class="font-bold">John Novak</span>
                            <small class="text-muted">21.03.2015</small>
                            <div class="social-content small">
                                Injected humour, or randomised words which don't look even slightly believable.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="social-talk">
                    <div class="media social-profile clearfix">
                        <a class="pull-left">
                            <img src="images/a3.jpg" alt="profile-picture">
                        </a>

                        <div class="media-body">
                            <span class="font-bold">Mark Smith</span>
                            <small class="text-muted">14.04.2015</small>
                            <div class="social-content">
                                Many desktop publishing packages and web page editors.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="social-talk">
                    <div class="media social-profile clearfix">
                        <a class="pull-left">
                            <img src="images/a4.jpg" alt="profile-picture">
                        </a>

                        <div class="media-body">
                            <span class="font-bold">Marica Morgan</span>
                            <small class="text-muted">21.03.2015</small>

                            <div class="social-content">
                                There are many variations of passages of Lorem Ipsum available, but the majority have
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="p-m">
            <span class="font-bold no-margins"> Sales in last week </span>
            <div class="m-t-xs">
                <div class="row">
                    <div class="col-xs-6">
                        <small>Today</small>
                        <h4 class="m-t-xs">$170,20 <i class="fa fa-level-up text-success"></i></h4>
                    </div>
                    <div class="col-xs-6">
                        <small>Last week</small>
                        <h4 class="m-t-xs">$580,90 <i class="fa fa-level-up text-success"></i></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <small>Today</small>
                        <h4 class="m-t-xs">$620,20 <i class="fa fa-level-up text-success"></i></h4>
                    </div>
                    <div class="col-xs-6">
                        <small>Last week</small>
                        <h4 class="m-t-xs">$140,70 <i class="fa fa-level-up text-success"></i></h4>
                    </div>
                </div>
            </div>
            <small> Lorem Ipsum is simply dummy text of the printing simply all dummy text.
                Many desktop publishing packages and web page editors.
            </small>
        </div>

    </div>

    <!-- Footer-->
    <footer class="footer">
        <span class="pull-right">
            Example text
        </span>
        Company 2015-2020
    </footer>

</div>

<!-- Vendor scripts -->
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/sparkline/index.js"></script>
<script src="<?php echo base_url()."assets/themes/jasny/js/jasny-bootstrap.js";?>"></script>
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/summernote/dist/summernote.min.js"></script>
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
<script src="<?php echo base_url()."assets/themes/ubiker/";?>vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
		
<!-- App scripts -->
<script src="<?php echo base_url()."assets/themes/ubiker/";?>scripts/homer.js"></script>


<script>

	$(document).on("change",".parent_sections select",function()
	{
		var section_parent = $(this).val();
		var base_url = '<?php echo site_url();?>';
		
		$.ajax({
			type:'POST',
			url: base_url+'hr/personnel/get_section_children/'+section_parent,
			cache:false,
			contentType: false,
			processData: false,
			dataType: 'text',
			success:function(data)
			{	
				$(".child_sections select").html(data);
			},
			error: function(xhr, status, error) 
			{
				alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			}
		});
		
		return false;
	});
    $(function () {
		$('#datapicker2').datepicker({format: 'yyyy-mm-dd'});

        // Initialize summernote plugin
        $('.summernote').summernote();

        var sHTML = $('.summernote').code();
      
        console.log(sHTML);

        $('.summernote1').summernote({
            toolbar: [
                ['headline', ['style']],
                ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
                ['textsize', ['fontsize']],
                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
            ]
        });

        $('.summernote2').summernote({
            airMode: true,
        });

    });

</script>

</body>
</html>