<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "site";
$route['404_override'] = '';

/*
*	Site Routes
*/
$route['home'] = 'site/home_page';
$route['about-us'] = 'site/about';
$route['services'] = 'site/services';
$route['services/(:any)/(:any)'] = 'site/view_service/$2';
$route['services/(:any)'] = 'site/services/$1';
$route['loans'] = 'site/loans';
$route['blog'] = 'site/blog';
$route['contact-us'] = 'site/contact_us';

/*
*	Settings Routes
*/
$route['settings'] = 'admin/settings';
$route['dashboard'] = 'admin/index';

/*
*	Login Routes
*/
$route['admin'] = 'auth/login_user';
$route['login-admin'] = 'login/login_admin';
$route['logout-admin'] = 'login/logout_admin';


/*
*	Settings Routes
*/
$route['settings'] = 'admin/settings';
$route['dashboard'] = 'admin/dashboard';
$route['change-password'] = 'admin/users/change_password';




/*
*	Users Routes
*/
$route['admin/administrators'] = 'admin/users';
$route['admin/administrators/(:any)/(:any)/(:num)'] = 'admin/users/index/$1/$2/$3';
$route['add-user'] = 'admin/users/add_user';
$route['edit-user/(:num)'] = 'admin/users/edit_user/$1';
$route['delete-user/(:num)'] = 'admin/users/delete_user/$1';
$route['activate-user/(:num)'] = 'admin/users/activate_user/$1';
$route['deactivate-user/(:num)'] = 'admin/users/deactivate_user/$1';
$route['reset-user-password/(:num)'] = 'admin/users/reset_password/$1';
$route['admin-profile/(:num)'] = 'admin/users/admin_profile/$1';

/*
*	Customers Routes
*/
$route['view-invoice/(:num)'] = 'admin/customers/view_invoice/$1';
$route['all-members'] = 'admin/members';
$route['all-members/(:num)'] = 'admin/members/index/$1';
$route['delete-member/(:num)'] = 'admin/members/delete_member/$1';
$route['activate-member/(:num)'] = 'admin/members/activate_member/$1';
$route['deactivate-member/(:num)'] = 'admin/members/deactivate_member/$1';

/*
*	Member Routes
*/

// $route['login'] = 'member/auth/login_member';
// $route['logout'] = 'member/sign_out';
// $route['account'] = 'member/my_account';
// $route['uploads'] = 'member/uploads';

/*
*	Accounts Routes
*/
$route['admin/accounts-receivable'] = 'admin/accounts/accounts_receivable';
$route['admin/accounts-receivable/(:num)'] = 'admin/accounts/accounts_receivable/$1';
$route['admin/accounts-receivable/(:any)/(:any)/(:num)'] = 'admin/accounts/accounts_receivable/$1/$2/$3';
$route['admin/accounts-payable'] = 'admin/accounts/accounts_payable';
$route['admin/accounts-payable/(:num)'] = 'admin/accounts/accounts_payable/$1';
$route['admin/accounts-payable/(:any)/(:any)/(:num)'] = 'admin/accounts/accounts_payable/$1/$2/$3';
$route['admin/confirm-payment/(:num)/(:any)/(:any)/(:any)/(:any)'] = 'admin/accounts/confirm_payment/$1/$2/$3/$4/$5';
$route['admin/unconfirm-payment/(:num)/(:any)/(:any)/(:any)/(:any)'] = 'admin/accounts/unconfirm_payment/$1/$2/$3/$4/$5';
$route['admin/receipt-payment/(:num)/(:any)/(:any)/(:any)/(:any)'] = 'admin/accounts/receipt_payment/$1/$2/$3/$4/$5';
$route['admin/search-accounts-receivable'] = 'admin/accounts/search_accounts_receivable';
$route['admin/close-receivable-search'] = 'admin/accounts/close_accounts_receivable_search';
$route['admin/search-accounts-payable'] = 'admin/accounts/search_accounts_payable';
$route['admin/close-payable-search'] = 'admin/accounts/close_accounts_payable_search';
$route['admin/receipts'] = 'admin/accounts/receipts';
$route['admin/receipts/(:num)'] = 'admin/accounts/receipts/$1';
$route['admin/receipts/(:any)/(:any)/(:num)'] = 'admin/accounts/receipts/$1/$2/$3';
$route['admin/search-receipts'] = 'admin/accounts/search_receipts';
$route['admin/close-payable-search'] = 'admin/accounts/close_receipts_search';


//sections
$route['administration/sections'] = 'admin/sections/index';
$route['administration/sections/(:any)/(:any)/(:num)'] = 'admin/sections/index/$1/$2/$3';
$route['administration/add-section'] = 'admin/sections/add_section';
$route['administration/edit-section/(:num)'] = 'admin/sections/edit_section/$1';

$route['administration/edit-section/(:num)/(:num)'] = 'admin/sections/edit_section/$1/$2';
$route['administration/delete-section/(:num)'] = 'admin/sections/delete_section/$1';
$route['administration/delete-section/(:num)/(:num)'] = 'admin/sections/delete_section/$1/$2';
$route['administration/activate-section/(:num)'] = 'admin/sections/activate_section/$1';
$route['administration/activate-section/(:num)/(:num)'] = 'admin/sections/activate_section/$1/$2';
$route['administration/deactivate-section/(:num)'] = 'admin/sections/deactivate_section/$1';
$route['administration/deactivate-section/(:num)/(:num)'] = 'admin/sections/deactivate_section/$1/$2';

//add members
$route['members/add-member'] = 'admin/members/add_member';
//imort of members
$route['members'] = 'admin/members/index';
$route['members/validate-import'] = 'admin/members/do_members_import';
$route['import/members-template'] = 'admin/members/import_members_template';
$route['members/import-members'] = 'admin/members/import_members';

//contact 
$route['administration/contacts']='admin/contacts/show_contacts';
$route['admin/company-profile'] = 'admin/contacts/show_contacts';

//about us routes
$route['front-page-about'] = 'admin/blog/front_post/32';

//company services
$route['company-services'] = 'admin/services/index';
$route['administration/all-services'] = 'admin/services/index';
$route['administration/all-services/(:num)'] = 'admin/services/index/$1';//with a page number
$route['administration/add-service'] = 'admin/services/add_service';
$route['administration/edit-service/(:num)/(:num)'] = 'admin/services/edit_service/$1/$2';
$route['administration/activate-service/(:num)/(:num)'] = 'admin/services/activate_service/$1/$2';
$route['administration/deactivate-service/(:num)/(:num)'] = 'admin/services/deactivate_service/$1/$2';
$route['administration/delete-service/(:num)/(:num)'] = 'admin/services/delete_service/$1/$2';

//company routes
$route['company-gallery'] = 'admin/gallery';
$route['administration/all-gallery-images'] = 'admin/gallery/index';
$route['administration/all-gallery-images/(:num)'] = 'admin/gallery/index/$1';//with a page number
$route['administration/add-gallery'] = 'admin/gallery/add_gallery';
$route['administration/edit-gallery/(:num)/(:num)'] = 'admin/gallery/edit_gallery/$1/$2';
$route['administration/activate-gallery/(:num)/(:num)'] = 'admin/gallery/activate_gallery/$1/$2';
$route['administration/deactivate-gallery/(:num)/(:num)'] = 'admin/gallery/deactivate_gallery/$1/$2';
$route['administration/delete-gallery/(:num)/(:num)'] = 'admin/gallery/delete_gallery/$1/$2';

// categories item

$route['categories'] = 'admin/blog/categories';
$route['categories/(:num)'] = 'admin/blog/categories/$1';
$route['add-blog-category'] = 'admin/blog/add_blog_category';
$route['edit-blog-category/(:num)'] = 'admin/blog/edit_blog_category/$1';
$route['delete-blog-category/(:num)'] = 'admin/blog/delete_blog_category/$1';
$route['activate-blog-category/(:num)'] = 'admin/blog/activate_blog_category/$1';
$route['deactivate-blog-category/(:num)'] = 'admin/blog/deactivate_blog_category/$1';


//blog routes
$route['posts'] = 'admin/blog';
$route['blog/posts'] = 'admin/blog';
$route['blog/categories'] = 'admin/blog/categories';
$route['add-post'] = 'admin/blog/add_post';
$route['add-post/(:any)'] = 'admin/blog/add_post_content/$1';
$route['edit-post/(:num)/(:any)'] = 'admin/blog/edit_post/$1/$2';
$route['delete-post/(:num)/(:any)'] = 'admin/blog/delete_post/$1/$2';
$route['activate-post/(:num)/(:any)'] = 'admin/blog/activate_post/$1/$2';
$route['deactivate-post/(:num)/(:any)'] = 'admin/blog/deactivate_post/$1/$2';
$route['post-comments/(:num)'] = 'admin/blog/post_comments/$1';
$route['blog/comments/(:num)'] = 'admin/blog/comments/$1';
$route['blog/comments'] = 'admin/blog/comments';
$route['add-comment/(:num)'] = 'admin/blog/add_comment/$1';
$route['delete-comment/(:num)/(:num)'] = 'admin/blog/delete_comment/$1/$2';
$route['activate-comment/(:num)/(:num)'] = 'admin/blog/activate_comment/$1/$2';
$route['deactivate-comment/(:num)/(:num)'] = 'admin/blog/deactivate_comment/$1/$2';

$route['delete-comment/(:num)'] = 'admin/blog/delete_comment/$1';
$route['activate-comment/(:num)'] = 'admin/blog/activate_comment/$1';
$route['deactivate-comment/(:num)'] = 'admin/blog/deactivate_comment/$1';

//projects
$route['projects'] = 'site/projects';
/* End of file routes.php */
/* Location: ./system/application/config/routes.php */


//trainings
$route['trainings'] = 'admin/trainings/index';
$route['trainings/(:num)'] = 'admin/trainings/index/$1';//with a page number
$route['administration/add-training'] = 'admin/trainings/add_training';
$route['administration/edit-training/(:num)/(:num)'] = 'admin/trainings/edit_training/$1/$2';
$route['administration/activate-training/(:num)/(:num)'] = 'admin/trainings/activate_training/$1/$2';
$route['administration/deactivate-training/(:num)/(:num)'] = 'admin/trainings/deactivate_training/$1/$2';
$route['administration/delete-training/(:num)/(:num)'] = 'admin/trainings/delete_training/$1/$2';

$route['slideshow'] = 'admin/slideshow/index';
$route['slideshow/(:num)'] = 'admin/slideshow/index/$1';
$route['administration/all-slides/(:num)'] = 'admin/slideshow/index/$1';//with a page number
$route['administration/add-slide'] = 'admin/slideshow/add_slide';
$route['administration/edit-slide/(:num)/(:num)'] = 'admin/slideshow/edit_slide/$1/$2';
$route['administration/activate-slide/(:num)/(:num)'] = 'admin/slideshow/activate_slide/$1/$2';
$route['administration/deactivate-slide/(:num)/(:num)'] = 'admin/slideshow/deactivate_slide/$1/$2';
$route['administration/delete-slide/(:num)/(:num)'] = 'admin/slideshow/delete_slide/$1/$2';

// web content 

$route['web-content/(:any)'] = 'admin/blog/group_post/$1';
$route['news'] = 'admin/blog/group_post/news';
$route['company-courses'] = 'admin/blog/group_post/company-courses';
$route['company-courses/(:num)'] = 'admin/blog/group_post/company-courses/$1';
$route['company-blog'] = 'admin/blog/group_post/company-blog';
$route['company-blog/(:num)'] = 'admin/blog/group_post/company-blog/$1';



$route['company-events'] = 'admin/blog/group_post/company-events';
$route['company-events/(:num)'] = 'admin/blog/group_post/company-events/$1';

$route['company-initiatives'] = 'admin/blog/group_post/company-initiatives';
$route['company-initiatives/(:num)'] = 'admin/blog/group_post/company-initiatives/$1';





$route['company-projects'] = 'admin/blog/group_post/company-projects';
$route['company-projects/(:num)'] = 'admin/blog/group_post/company-projects/$1';

$route['company-programmes'] = 'admin/blog/group_post/company-programmes';
$route['company-programmes/(:num)'] = 'admin/blog/group_post/company-programmes/$1';

$route['company-membership/(:any)'] = 'admin/blog/group_post/$1';
$route['trainings/(:any)'] = 'admin/blog/group_post/$1';
$route['organization-detail/(:any)'] = 'admin/blog/group_post/$1';

$route['company-opportunities'] = 'admin/blog/group_post/company-opportunities';
$route['company-opportunities/(:num)'] = 'admin/blog/group_post/company-opportunities/$1';


$route['company-services/main-services'] = 'admin/blog/group_post/main-services';
$route['company-services/services-list'] = 'admin/blog/group_post/services-list';


$route['company-contacts/main-contact'] = 'admin/blog/group_post/main-contact';
$route['company-contacts/departmental-contacts'] = 'admin/blog/group_post/departmental-contacts';

$route['blog/admin-posts'] = 'admin/blog/group_post/admin-posts';


/*
*	Site contacts Routes
*/
$route['contact'] = 'site/contact';
// $route['gallery'] = 'site/gallery';
// $route['event'] = 'site/trainings';
// $route['blog'] = 'site/blog/index';
// $route['event/facilitators'] = 'site/facilitators';
// $route['about/board'] = 'site/board';
$route['about'] = 'site/about';
$route['about-us'] = 'site/about';
$route['membership'] = 'site/membership';
$route['events'] = 'site/events';
$route['events/(:num)'] = 'site/events/$1';
$route['blog'] = 'site/site_blog';
$route['blog/(:num)'] = 'site/blog/$1';
$route['initiatives'] = 'site/initiatives';
$route['initiatives/(:num)'] = 'site/initiatives/$1';


$route['about-us/(:any)'] = 'site/about_content/$1';
$route['our-casuses/(:any)'] = 'site/view_service/$1';
$route['our-casuses'] = 'site/view_service';
$route['our-services/(:any)/(:any)/(:any)/(:any)'] = 'site/view_service/$4';
$route['our-services/(:any)/(:any)/(:any)'] = 'site/view_service/$3';
$route['our-services/(:any)/(:any)'] = 'site/view_service/$2';
$route['our-services/(:any)'] = 'site/view_service/$1';
$route['valuation'] = 'site/valuation_content';
// $route['programmes'] = 'site/programme_view';
$route['programmes/(:any)/(:num)'] = 'site/programme_view/$1/$2';
$route['opportunities/(:any)/(:num)'] = 'site/opportunities_view/$1/$2';
$route['our-people'] = 'site/people_view';
$route['useful-info'] = 'site/useful_info';
$route['driver-training/(:any)'] = 'site/projects_content/$1';
// $route['services'] = 'site/services_brief';
// $route['services/(:any)'] = 'site/service_item/$1';


$route['tourism/(:any)'] = 'site/tourism_item/$1';


/*
*	Blog Routes
*/
$route['blog'] = 'site/blog';
$route['blog/(:num)'] = 'site/blog/index/__/__/$1';//going to different page without any filters
$route['view-post/(:any)/(:num)'] = 'site/blog/view_post/$1/$2';//going to single post page
$route['blog/category/(:any)'] = 'site/blog/index/$1';//category present
$route['blog/category/(:any)/(:num)'] = 'site/blog/index/$1/$2';//category present going to next page
$route['blog/search/(:any)'] = 'site/blog/index/__/$1';//search present
$route['blog/search/(:any)/(:num)'] = 'site/blog/index/__/$1/$2';//search present going to next page

/* End of file routes.php */
/* Location: ./application/config/routes.php */

/*
*	Gallery Routes
*/
$route['gallery'] = 'site/gallery';
$route['gallery/(:num)'] = 'site/gallery/$1';

//events
$route['content/events'] = 'admin/events/index';
$route['content/events/(:num)'] = 'admin/events/index/$1';//with a page number
$route['administration/all-events'] = 'admin/events/index';
$route['administration/all-events/(:num)'] = 'admin/events/index/$1';//with a page number
$route['administration/add-event'] = 'admin/events/add_event';
$route['administration/edit-event/(:num)/(:num)'] = 'admin/events/edit_event/$1/$2';
$route['administration/activate-event/(:num)/(:num)'] = 'admin/events/activate_event/$1/$2';
$route['administration/deactivate-event/(:num)/(:num)'] = 'admin/events/deactivate_event/$1/$2';
$route['administration/delete-event/(:num)/(:num)'] = 'admin/events/delete_event/$1/$2';


/*
*	Gallery Routes
*/
$route['departments'] = 'site/departments';
$route['departments/(:num)'] = 'site/departments/$1';
$route['departments/(:any)'] = 'site/view_department/$1';

$route['about/(:any)'] = 'site/about_item/$1';//going to single post page
$route['academic-courses/(:any)'] = 'site/academic_courses_item/$1';//going to single post page
$route['student-affairs/(:any)'] = 'site/student_affairs_item/$1';//going to single post page
$route['contact-us'] = 'site/contact_us';
$route['booking'] = 'site/booking';
$route['app'] = 'site/app';
$route['booking/(:any)'] = 'site/booking/$1';

$route['view-info/(:any)/(:num)'] = 'site/view_info/$1/$2';
$route['book-taxi'] = 'site/book_taxi';
$route['faqs/(:any)'] = 'site/faqs/$1';

$route['administration/branches'] = 'admin/branches';
$route['administration/edit-branch/(:num)'] = 'admin/branches/edit_branch/$1';
$route['administration/add-branch'] = 'admin/branches/add_branch';



//company services
$route['testimonials'] = 'admin/testimonials/index';
$route['administration/all-testimonials'] = 'admin/testimonials/index';
$route['administration/all-testimonials/(:num)'] = 'admin/testimonials/index/$1';//with a page number
$route['administration/add-testimonial'] = 'admin/testimonials/add_testimonial';
$route['administration/edit-testimonial/(:num)/(:num)'] = 'admin/testimonials/edit_testimonial/$1/$2';
$route['administration/activate-testimonial/(:num)/(:num)'] = 'admin/testimonials/activate_testimonial/$1/$2';
$route['administration/deactivate-testimonial/(:num)/(:num)'] = 'admin/testimonials/deactivate_testimonial/$1/$2';
$route['administration/delete-testimonial/(:num)/(:num)'] = 'admin/testimonials/delete_testimonial/$1/$2';

/*
*	HR Routes
*/
$route['human-resource/schedules'] = 'hr/schedules/index';
$route['human-resource/delete-schedule/(:num)'] = 'hr/schedules/delete_schedule/$1';
$route['human-resource/delete-schedule/(:num)/(:num)'] = 'hr/schedules/delete_schedule/$1/$2';
$route['human-resource/activate-schedule/(:num)'] = 'hr/schedules/activate_schedule/$1';
$route['human-resource/activate-schedule/(:num)/(:num)'] = 'hr/schedules/activate_schedule/$1/$2';
$route['human-resource/deactivate-schedule/(:num)'] = 'hr/schedules/deactivate_schedule/$1';
$route['human-resource/deactivate-schedule/(:num)/(:num)'] = 'hr/schedules/deactivate_schedule/$1/$2';
$route['human-resource/schedule-personnel/(:num)'] = 'hr/schedules/schedule_personnel/$1';
$route['human-resource/fill-timesheet/(:num)/(:num)'] = 'hr/schedules/fill_timesheet/$1/$2';
$route['human-resource/doctors-schedule'] = 'hr/schedules/doctors_schedule';
$route['human-resource/schedule-personnel/(:num)/(:any)/(:any)/(:num)'] = 'hr/schedules/schedule_personnel/$1/$2/$3/$4';
$route['human-resource/schedule-personnel/(:num)/(:any)/(:any)'] = 'hr/schedules/schedule_personnel/$1/$2/$3';
$route['human-resource/schedules/(:any)/(:any)/(:num)'] = 'hr/schedules/index/$1/$2/$3';
$route['human-resource/schedules/(:any)/(:any)'] = 'hr/schedules/index/$1/$2';

$route['users'] = 'hr/personnel/index';
$route['human-resource/my-account'] = 'admin/dashboard';
$route['human-resource/my-account/edit-about/(:num)'] = 'hr/personnel/my_account/update_personnel_about_details/$1';
$route['human-resource/edit-personnel-account/(:num)'] = 'hr/personnel/update_personnel_account_details/$1';
$route['human-resource/configuration'] = 'hr/configuration';
$route['human-resource/add-job-title'] = 'hr/add_job_title';
$route['human-resource/edit-job-title/(:num)'] = 'hr/edit_job_title/$1';
$route['human-resource/delete-job-title/(:num)'] = 'hr/delete_job_title/$1';
$route['human-resource/personnel'] = 'hr/personnel/index';
$route['human-resource/personnel/(:any)/(:any)/(:num)'] = 'hr/personnel/index/$1/$2/$3';
$route['human-resource/add-personnel'] = 'hr/personnel/add_personnel';
$route['human-resource/edit-personnel/(:num)'] = 'hr/personnel/edit_personnel/$1';
$route['human-resource/edit-store-authorize/(:num)'] = 'hr/personnel/edit_store_authorize/$1';
$route['human-resource/edit-order-authorize/(:num)'] = 'hr/personnel/edit_order_authorize/$1';

$route['human-resource/edit-personnel-about/(:num)'] = 'hr/personnel/update_personnel_about_details/$1';
$route['human-resource/edit-personnel-account/(:num)'] = 'hr/personnel/update_personnel_account_details/$1';
$route['human-resource/edit-personnel/(:num)/(:num)'] = 'hr/personnel/edit_personnel/$1/$2';
$route['human-resource/delete-personnel/(:num)'] = 'hr/personnel/delete_personnel/$1';
$route['human-resource/delete-personnel/(:num)/(:num)'] = 'hr/personnel/delete_personnel/$1/$2';
$route['human-resource/activate-personnel/(:num)'] = 'hr/personnel/activate_personnel/$1';
$route['human-resource/activate-personnel/(:num)/(:num)'] = 'hr/personnel/activate_personnel/$1/$2';
$route['human-resource/deactivate-personnel/(:num)'] = 'hr/personnel/deactivate_personnel/$1';
$route['human-resource/deactivate-personnel/(:num)/(:num)'] = 'hr/personnel/deactivate_personnel/$1/$2';
$route['human-resource/reset-password/(:num)'] = 'hr/personnel/reset_password/$1';
$route['human-resource/update-personnel-roles/(:num)'] = 'hr/personnel/update_personnel_roles/$1';
$route['human-resource/add-emergency-contact/(:num)'] = 'hr/personnel/add_emergency_contact/$1';
$route['human-resource/activate-emergency-contact/(:num)/(:num)'] = 'hr/personnel/activate_emergency_contact/$1/$2';
$route['human-resource/deactivate-emergency-contact/(:num)/(:num)'] = 'hr/personnel/deactivate_emergency_contact/$1/$2';
$route['human-resource/delete-emergency-contact/(:num)/(:num)'] = 'hr/personnel/delete_emergency_contact/$1/$2';

$route['human-resource/add-dependant-contact/(:num)'] = 'hr/personnel/add_dependant_contact/$1';
$route['human-resource/activate-dependant-contact/(:num)/(:num)'] = 'hr/personnel/activate_dependant_contact/$1/$2';
$route['human-resource/deactivate-dependant-contact/(:num)/(:num)'] = 'hr/personnel/deactivate_dependant_contact/$1/$2';
$route['human-resource/delete-dependant-contact/(:num)/(:num)'] = 'hr/personnel/delete_dependant_contact/$1/$2';

$route['human-resource/add-personnel-job/(:num)'] = 'hr/personnel/add_personnel_job/$1';
$route['human-resource/activate-personnel-job/(:num)/(:num)'] = 'hr/personnel/activate_personnel_job/$1/$2';
$route['human-resource/deactivate-personnel-job/(:num)/(:num)'] = 'hr/personnel/deactivate_personnel_job/$1/$2';
$route['human-resource/delete-personnel-job/(:num)/(:num)'] = 'hr/personnel/delete_personnel_job/$1/$2';

$route['human-resource/leave'] = 'hr/leave/calender';
$route['human-resource/leave/(:any)/(:any)'] = 'hr/leave/calender/$1/$2';
$route['human-resource/view-leave/(:any)'] = 'hr/leave/view_leave/$1';
$route['human-resource/add-personnel-leave/(:num)'] = 'hr/personnel/add_personnel_leave/$1';
$route['human-resource/add-leave/(:any)'] = 'hr/leave/add_leave/$1';
$route['human-resource/add-calender-leave'] = 'hr/leave/add_calender_leave';
$route['human-resource/activate-leave/(:num)/(:any)'] = 'hr/leave/activate_leave/$1/$2';
$route['human-resource/deactivate-leave/(:num)/(:any)'] = 'hr/leave/deactivate_leave/$1/$2';
$route['human-resource/delete-leave/(:num)/(:any)'] = 'hr/leave/delete_leave/$1/$2';
$route['human-resource/activate-personnel-leave/(:num)/(:num)'] = 'hr/personnel/activate_personnel_leave/$1/$2';
$route['human-resource/deactivate-personnel-leave/(:num)/(:num)'] = 'hr/personnel/deactivate_personnel_leave/$1/$2';
$route['human-resource/delete-personnel-leave/(:num)/(:num)'] = 'hr/personnel/delete_personnel_leave/$1/$2';

$route['human-resource/delete-personnel-role/(:num)/(:num)'] = 'hr/personnel/delete_personnel_role/$1/$2';
$route['human-resource/delete-personnel-approvals_assigned/(:num)/(:num)'] = 'hr/personnel/delete_personnel_approvals_assigned/$1/$2';

$route['search'] = 'site/search';
$route['search-result/(:any)'] = 'site/search_content/$1';



$route['view-project/(:any)/(:num)'] = 'site/blog/view_project/$1/$2';//going to single post page

$route['procced-to-payment/(:num)'] = 'site/subscription/start_donate/$1';
$route['donate-now'] = 'site/subscription/donate_now';


$route['payment'] = 'site/subscription/payment';
$route['process-payment/(:any)/(:any)'] = 'site/subscription/process_payment/$1/$2';

$route['my-donations'] = 'customers/account/my_donations';
$route['my-donations/(:num)'] = 'customers/account/my_donations/$1';
$route['my-transactions'] = 'customers/account/my_transactions';
$route['my-transactions/(:num)'] = 'customers/account/my_transactions/$1';

$route['my-trips'] = 'customers/account/my_trips';
$route['my-trips/(:num)'] = 'customers/account/my_trips/$1';
$route['add-member-trip'] = 'customers/account/add_member_trip';
$route['edit-member-trip/(:num)'] = 'customers/account/edit_member_trip/$1';

$route['member-contributions']= 'customers/account/my_donations';
$route['member-contributions/(:num)'] = 'customers/account/my_donations/$1';
$route['member-transactions'] = 'customers/account/my_transactions';
$route['member-transactions/(:num)'] = 'customers/account/my_transactions/$1';

$route['causes'] = 'customers/account/causes';
$route['causes/(:num)'] = 'customers/account/causes/$1';

$route['campaigns'] = 'customers/account/campaigns';
$route['campaigns/(:num)'] = 'customers/account/campaigns/$1';
$route['add-campaign'] = 'customers/account/add_campaign';






$route['members/(:any)'] = 'customers/customers/index/$1';
$route['members/(:any)/(:num)'] = 'customers/customers/index/$1/$2';
$route['donors/(:any)'] = 'customers/customers/index/$1';
$route['donors/(:any)/(:num)'] = 'customers/customers/index/$1/$2';



$route['customers/import-customers/(:num)'] = 'customers/do_companies_import/$1';
$route['customers/customers-template'] = 'customers/import_customers_template';
$route['add-member/(:any)'] = 'customers/customers/add_company/$1';
$route['customers/delete_document_scan/(:num)/(:num)'] = 'customers/delete_document_scan/$1/$2';
$route['customers/companies-template'] = 'customers/import_companies_template';
$route['customers/add_person/(:num)'] = 'customers/customers/add_company_contact/$1';
$route['edit-member/(:num)/(:any)'] = 'customers/customers/edit_company/$1/$2';
$route['customers/update-passwords'] = 'customers/customers/update_customers_password';
$route['customers/send-password/(:num)'] = 'customers/customers/send_password/$1';
$route['customers/deactivate-customer/(:num)'] = 'customers/customers/deactivate_customer/$1';
$route['customers/delete-customer/(:num)'] = 'customers/customers/delete_customer/$1';
$route['customers/customer-documents/(:num)'] = 'customers/customers/customer_documents/$1';
$route['customers/activate-customer/(:num)'] = 'customers/customers/activate_customer/$1';
$route['customers/edit_company/(:num)'] = 'customers/customers/edit_company/$1';
$route['customers/edit_company_contact/(:num)'] = 'customers/customers/edit_company_contact/$1';
$route['customers/deactivate_company_contact/(:num)'] = 'customers/customers/deactivate_customer_contact/$1';
$route['customers/activate_company_contact/(:num)'] = 'customers/customers/activate_customer_contact/$1';
$route['customers/(:any)/(:any)/(:num)'] = 'customers/customers/index/$1/$2/$3';
$route['customers/(:any)/(:any)'] = 'customers/customers/index/$1/$2';

$route['donations'] = 'customers/customer_donations';
$route['donations/(:num)'] = 'customers/customer_donations/$1';

$route['applications'] = 'customers/customer_applications';
$route['applications/(:num)'] = 'customers/customer_applications/$1';

$route['applications/opportunities'] = 'admin/applications/opportunities';
$route['applications/opportunities/(:num)'] = 'admin/applications/opportunities/$1';

$route['applications/conservancies'] = 'admin/applications/conservancies';
$route['applications/conservancies/(:num)'] = 'admin/applications/conservancies/$1';

$route['applications/clubs'] = 'admin/applications/applicants_view/clubs';
$route['applications/clubs/(:num)'] = 'admin/applications/applicants_view/clubs/$1';

$route['applications/schools'] = 'admin/applications/applicants_view/schools';
$route['applications/schools/(:num)'] = 'admin/applications/applicants_view/schools/$1';
$route['applications/corporates'] = 'admin/applications/applicants_view/corporates';
$route['applications/corporates/(:num)'] = 'admin/applications/applicants_view/corporates/$1';


$route['applicants/activate-applicant/(:num)'] = 'admin/applications/activate_applicant/$1';
$route['applicants/deactivate-applicant/(:num)'] = 'admin/applications/deactivate_applicant/$1';
$route['applicants/activate-conservancy/(:num)'] = 'admin/applications/activate_conservancy/$1';
$route['applicants/deactivate-conservancy/(:num)'] = 'admin/applications/deactivate_conservancy/$1';


$route['add-campaign'] = 'admin/blog/add_campaign';
$route['edit-campaign/(:any)/(:num)'] = 'admin/blog/edit_campaign/$1/$2';
$route['campaign-contributions'] = 'customers/account/campaign_contributions';


$route['trips'] = 'customers/account/trips';
$route['trips/(:num)'] = 'customers/account/trips/$1';

$route['conservancy-trips'] = 'customers/account/conservancy_trips';
$route['conservancy-trips/(:num)'] = 'customers/account/conservancy_trips/$1';

$route['approve-trip/(:num)'] = 'customers/account/approve_trip/$1';
$route['cancel-trip/(:num)'] = 'customers/account/cancel_trip/$1';

/*
*	Users Routes
*/
$route['all-users'] = 'admin/users';
$route['all-users/(:num)'] = 'admin/users/index/$1';
$route['add-user'] = 'admin/users/add_user';
$route['edit-user/(:num)'] = 'admin/users/edit_user/$1';
$route['delete-user/(:num)'] = 'admin/users/delete_user/$1';
$route['activate-user/(:num)'] = 'admin/users/activate_user/$1';
$route['deactivate-user/(:num)'] = 'admin/users/deactivate_user/$1';
$route['reset-user-password/(:num)'] = 'admin/users/reset_password/$1';
$route['admin-profile/(:num)'] = 'admin/users/admin_profile/$1';



$route['admin/administrators'] = 'admin/users';
$route['admin/administrators/(:any)/(:any)/(:num)'] = 'admin/users/index/$1/$2/$3';
$route['add-user'] = 'admin/users/add_user';
$route['edit-user/(:num)'] = 'admin/users/edit_user/$1';
$route['delete-user/(:num)'] = 'admin/users/delete_user/$1';
$route['activate-user/(:num)'] = 'admin/users/activate_user/$1';
$route['deactivate-user/(:num)'] = 'admin/users/deactivate_user/$1';
$route['reset-user-password/(:num)'] = 'admin/users/reset_password/$1';
$route['admin-profile/(:num)'] = 'admin/users/admin_profile/$1';


/*
*	Admin Routes
*/

//jobs
$route['administration/all-jobs'] = 'admin/jobs/index';
$route['administration/all-jobs/(:num)'] = 'admin/jobs/index/created/DESC/$1';//with a page number
$route['administration/all-jobs/(:any)/(:any)/(:num)'] = 'admin/jobs/index/$1/$2/$3';//with a page number
$route['administration/add-job'] = 'admin/jobs/add_job';
$route['administration/edit-job/(:num)/(:num)'] = 'admin/jobs/edit_job/$1/$2';
$route['administration/activate-job/(:num)/(:num)'] = 'admin/jobs/activate_job/$1/$2';
$route['administration/deactivate-job/(:num)/(:num)'] = 'admin/jobs/deactivate_job/$1/$2';
$route['administration/delete-job/(:num)/(:num)'] = 'admin/jobs/delete_job/$1/$2';

//slides
$route['administration/contacts'] = 'admin/contacts/show_contacts';
$route['administration/loans'] = 'admin/loans/show_loans';
$route['administration/all-slides'] = 'admin/slideshow/index';
$route['administration/all-slides/(:num)'] = 'admin/slideshow/index/$1';//with a page number
$route['administration/add-slide'] = 'admin/slideshow/add_slide';
$route['administration/edit-slide/(:num)/(:num)'] = 'admin/slideshow/edit_slide/$1/$2';
$route['administration/activate-slide/(:num)/(:num)'] = 'admin/slideshow/activate_slide/$1/$2';
$route['administration/deactivate-slide/(:num)/(:num)'] = 'admin/slideshow/deactivate_slide/$1/$2';
$route['administration/delete-slide/(:num)/(:num)'] = 'admin/slideshow/delete_slide/$1/$2';

//departments
$route['administration/all-departments'] = 'admin/departments/index';
$route['administration/all-departments/(:num)'] = 'admin/departments/index/$1';//with a page number
$route['administration/add-department'] = 'admin/departments/add_department';
$route['administration/edit-department/(:num)/(:num)'] = 'admin/departments/edit_department/$1/$2';
$route['administration/activate-department/(:num)/(:num)'] = 'admin/departments/activate_department/$1/$2';
$route['administration/deactivate-department/(:num)/(:num)'] = 'admin/departments/deactivate_department/$1/$2';
$route['administration/delete-department/(:num)/(:num)'] = 'admin/departments/delete_department/$1/$2';

//services
$route['administration/all-services'] = 'admin/services/index';
$route['administration/all-services/(:num)'] = 'admin/services/index/$1';//with a page number
$route['administration/add-service'] = 'admin/services/add_service';
$route['administration/edit-service/(:num)/(:num)'] = 'admin/services/edit_service/$1/$2';
$route['administration/activate-service/(:num)/(:num)'] = 'admin/services/activate_service/$1/$2';
$route['administration/deactivate-service/(:num)/(:num)'] = 'admin/services/deactivate_service/$1/$2';
$route['administration/delete-service/(:num)/(:num)'] = 'admin/services/delete_service/$1/$2';

//services
$route['administration/all-gallery-images'] = 'admin/gallery/index';
$route['administration/all-gallery-images/(:num)'] = 'admin/gallery/index/$1';//with a page number
$route['administration/add-gallery'] = 'admin/gallery/add_gallery';
$route['administration/edit-gallery/(:num)/(:num)'] = 'admin/gallery/edit_gallery/$1/$2';
$route['administration/activate-gallery/(:num)/(:num)'] = 'admin/gallery/activate_gallery/$1/$2';
$route['administration/deactivate-gallery/(:num)/(:num)'] = 'admin/gallery/deactivate_gallery/$1/$2';
$route['administration/delete-gallery/(:num)/(:num)'] = 'admin/gallery/delete_gallery/$1/$2';

/*
*	Admin Blog Routes
*/
$route['posts'] = 'admin/blog';
$route['all-posts'] = 'admin/blog';
$route['blog-categories'] = 'admin/blog/categories';
$route['add-post'] = 'admin/blog/add_post';
$route['edit-post/(:num)'] = 'admin/blog/edit_post/$1';
$route['delete-post/(:num)'] = 'admin/blog/delete_post/$1';
$route['activate-post/(:num)'] = 'admin/blog/activate_post/$1';
$route['deactivate-post/(:num)'] = 'admin/blog/deactivate_post/$1';
$route['post-comments/(:num)'] = 'admin/blog/post_comments/$1';
$route['comments/(:num)'] = 'admin/blog/comments/$1';
$route['comments'] = 'admin/blog/comments';
$route['add-comment/(:num)'] = 'admin/blog/add_comment/$1';
$route['delete-comment/(:num)/(:num)'] = 'admin/blog/delete_comment/$1/$2';
$route['activate-comment/(:num)/(:num)'] = 'admin/blog/activate_comment/$1/$2';
$route['deactivate-comment/(:num)/(:num)'] = 'admin/blog/deactivate_comment/$1/$2';
$route['add-blog-category'] = 'admin/blog/add_blog_category';
$route['edit-blog-category/(:num)'] = 'admin/blog/edit_blog_category/$1';
$route['delete-blog-category/(:num)'] = 'admin/blog/delete_blog_category/$1';
$route['activate-blog-category/(:num)'] = 'admin/blog/activate_blog_category/$1';
$route['deactivate-blog-category/(:num)'] = 'admin/blog/deactivate_blog_category/$1';
$route['delete-comment/(:num)'] = 'admin/blog/delete_comment/$1';
$route['activate-comment/(:num)'] = 'admin/blog/activate_comment/$1';
$route['deactivate-comment/(:num)'] = 'admin/blog/deactivate_comment/$1';

/*
*	Blog Routes
*/
$route['blog'] = 'site/blog';
$route['blog/(:num)'] = 'site/blog/index/__/__/$1';//going to different page without any filters
$route['blog/view-single/(:any)'] = 'site/blog/view_post/$1';//going to single post page
$route['blog/category/(:any)'] = 'site/blog/index/$1';//category present
$route['blog/category/(:any)/(:num)'] = 'site/blog/index/$1/$2';//category present going to next page
$route['blog/search/(:any)'] = 'site/blog/index/__/$1';//search present
$route['blog/search/(:any)/(:num)'] = 'site/blog/index/__/$1/$2';//search present going to next page

/* End of file routes.php */
/* Location: ./application/config/routes.php */

/*
*	Gallery Routes
*/
$route['gallery'] = 'site/gallery';
$route['gallery/(:num)'] = 'site/gallery/$1';


/*
*	Gallery Routes
*/
$route['departments'] = 'site/departments';
$route['departments/(:num)'] = 'site/departments/$1';
$route['departments/(:any)'] = 'site/view_department/$1';


$route['our-courses/(:any)'] = 'site/view_article/$1';
$route['our-courses'] = 'site/view_article';

$route['view-service-category/(:any)'] = 'site/view_service_category/$1';
$route['view-service/(:any)'] = 'site/view_service/$1';